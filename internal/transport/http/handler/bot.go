package handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/labstack/echo/v4"
	"net/http"
	"processor/internal/model"
	"strconv"
	"strings"
	"time"
)


type msg struct {
	UpdateId int `json:"update_id"`
	Message  struct {
		MessageId int `json:"message_id"`
		From      struct {
			Id           int64  `json:"id"`
			IsBot        bool   `json:"is_bot"`
			FirstName    string `json:"first_name"`
			Username     string `json:"username"`
			LanguageCode string `json:"language_code"`
		} `json:"from"`
		Chat struct {
			Id        int64  `json:"id"`
			FirstName string `json:"first_name"`
			Username  string `json:"username"`
			Type      string `json:"type"`
		} `json:"chat"`
		Date     int    `json:"date"`
		Text     string `json:"text"`
		Entities []struct {
			Offset int    `json:"offset"`
			Length int    `json:"length"`
			Type   string `json:"type"`
		} `json:"entities"`
	} `json:"message"`
}

type person struct {
	id     int
	chatId int64
}

// This handler is called everytime telegram sends us a webhook event
func (h Handler) BotHandler(c echo.Context) error {
	// First, decode the JSON response body
	msgg := &msg{}
	if err := json.NewDecoder(c.Request().Body).Decode(msgg); err != nil {
		fmt.Println("could not decode request body", err)
		return nil
	}
	danil := person{
		id:     17,
		chatId: 48909833,
	}
	rufat := person{
		id:     0,
		chatId: 0,
	}
	var managers []person
	managers = append(managers, rufat, danil)
	//reqBodyBytes := new(bytes.Buffer)
	//json.NewEncoder(reqBodyBytes).Encode(msgg)

	// Check if the message contains the word "marco"
	// if not, return without doing anything
	//if !strings.Contains(strings.ToLower(msgg.Message.Text), "marco") {
	//	return nil
	//}
	//fmt.Println(msgg.Message.Text)
	// If the text contains marco, call the `sayPolo` function, which
	// is defined below
	//if err := sayPolo(msgg.Message.Chat.Id, string(reqBodyBytes.Bytes())); err != nil {
	//	fmt.Println("error in sending reply:", err)
	//	return nil
	//}
	bot, err := tgbotapi.NewBotAPI("5792153430:AAG3tL9zumpHN0jPtssiiP8kS_gIycnvlBI")
	if err != nil {
		fmt.Println("error in sending reply:", err)
		return nil
	}
	if msgg.Message.Entities != nil && len(msgg.Message.Entities) > 0 {
		if msgg.Message.Entities[0].Type == "bot_command" {
			for _, u := range managers {
				if u.chatId == msgg.Message.Chat.Id {
					switch strings.Split(msgg.Message.Text, " ")[0] {
					case "/my_balance":
						{
							since := time.Time{}.String()
							to := time.Now().String()
							rs, _ := h.s.Report(c.Request().Context(), u.id, "", since, to)
							manager, _ := h.repo.GetUserInfoByID(u.id)
							reqBodyBytes := new(bytes.Buffer)
							json.NewEncoder(reqBodyBytes).Encode(rs.Balance)
							msgNew := tgbotapi.NewMessage(msgg.Message.Chat.Id, fmt.Sprintf("Сальдо менеджера %s по всем пользователям: %s", manager.Username, string(reqBodyBytes.Bytes())))
							//msgNew.ReplyToMessageID = update.Message.MessageID
							bot.Send(msgNew)
							//if err := sayPolo(msgg.Message.Chat.Id, fmt.Sprintf("Сальдо менеджера %s по всем пользователям: %s", manager.Username, string(reqBodyBytes.Bytes()))); err != nil {
							//	fmt.Println("error in sending reply:", err)
							//	return nil
							//}
						}
					case "/my_debtors":
						{
							var message string
							debtors, _ := h.repo.GetDebtors(u.id)
							for _, d := range *debtors {
								if d.Total < 0 {
									message += fmt.Sprintf("Пользователь: %s. Сальдо: %f.\n", d.Name, d.Total)
								}
							}
							if message != "" {
								if err := sayPolo(msgg.Message.Chat.Id, message); err != nil {
									fmt.Println("error in sending reply:", err)
									return nil
								}
							}

						}
					case "/debtor":
						{
							darr := strings.Split(msgg.Message.Text, " ")
							var name string
							if len(darr) == 2 {
								name = darr[1]
							}
							fmt.Println(name)
							id, _ := h.repo.IdByUser(name)
							fmt.Println(id)
							since := time.Time{}.String()
							to := time.Now().String()
							rs, _ := h.s.Report(c.Request().Context(), u.id, "", since, to)
							var message string
							for _, d := range rs.Finance {
								if d.User == fmt.Sprintf("%d", id) {
									if d.Income != 0 {
										message += fmt.Sprintf("%s Получено: %f.\n", d.Time, d.Income)
									}
									if d.Outcome != 0 {
										message += fmt.Sprintf("%s Пополнено/возвращено: %f.\n", d.Time, d.Outcome)
									}

								}
							}
							if message != "" {
								if err := sayPolo(msgg.Message.Chat.Id, message); err != nil {
									fmt.Println("error in sending reply:", err)
									return nil
								}
							}
						}
					case "/kassa":
						{
							darr := strings.Split(msgg.Message.Text, " ")
							var name float64
							var message string
							if len(darr) == 2 {
								name, _ = strconv.ParseFloat(darr[1], 64)
							}
							since := time.Time{}.String()
							to := time.Now().String()
							h.s.CreateTransaction(c.Request().Context(), model.Finance{
								Time:            time.Time{},
								Income:          0,
								Outcome:         name,
								User:            "",
								Comment:         fmt.Sprintf("Передал в кассу %f (%v)", name, time.Now().Format("01/02/2006")),
								TransactionType: "Передал в кассу",
							}, u.id, "", since, to)
							message += fmt.Sprintf("Передал в кассу %f (%v)", name, time.Now().Format("01/02/2006"))
							if message != "" {
								if err := sayPolo(msgg.Message.Chat.Id, message); err != nil {
									fmt.Println("error in sending reply:", err)
									return nil
								}
								if err := sayPolo(48909833, fmt.Sprintf("%d Передал в кассу %f (%v)", u.id, name, time.Now().Format("01/02/2006"))); err != nil {
									fmt.Println("error in sending reply:", err)
									return nil
								}
							}
						}
					case "/income":
						{

						}
					}
				}
			}

		}
	}
	//fmt.Println(msgg.Message.Chat.Id)
	// log a confirmation message if the message is sent successfully
	//fmt.Println("reply sent")
	return nil
}

type sendMessageReqBody struct {
	ChatID int64  `json:"chat_id"`
	Text   string `json:"text"`
}

// sayPolo takes a chatID and sends "polo" to them
func sayPolo(chatID int64, message string) error {
	// Create the request body struct
	reqBody := &sendMessageReqBody{
		ChatID: chatID,
		Text:   message,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/bot5792153430:AAG3tL9zumpHN0jPtssiiP8kS_gIycnvlBI/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}
