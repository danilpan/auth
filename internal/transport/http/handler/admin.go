package handler

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/shopspring/decimal"
	"github.com/xuri/excelize/v2"
	"net/http"
	"processor/internal/model"
	"processor/internal/service"
	"strconv"
	"time"
)

func (h Handler) UserRegistration(c echo.Context) error {
	var user model.User
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: err.Error()})
	}
	if err := user.HashPassword(user.Password); err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: err.Error()})
	}
	data, userErr := h.repo.CreateUser(user)
	if userErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: userErr.Error()})
	}
	return c.JSON(http.StatusCreated, *data)
}

func (h Handler) UserBalanceReplenishment(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	rq := model.UserBalanceRequest{}
	err := c.Bind(&rq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "bad request"})
	}
	balance := decimal.NewFromFloat(rq.Balance)
	user, err := h.s.ReplenishBalance(c.Request().Context(), rq.UserID, balance, claims.UserID, rq.IsRecieved)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: ""})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetReportExcel(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	from := c.QueryParam("from")
	to := c.QueryParam("to")
	username := c.QueryParam("user")
	if from == "" {
		from = time.Time{}.String()
	}
	if to == "" {
		to = time.Now().String()
	}
	user, err := h.s.Report(c.Request().Context(), claims.UserID, username, from, to)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: ""})
	}

	file := excelize.NewFile()
	for i, t := range user.Finance {

		file.SetCellValue("Sheet1", fmt.Sprintf("A%v", i+1), t.Time)
		file.SetCellValue("Sheet1", fmt.Sprintf("B%v", i+1), t.User)

		file.SetCellValue("Sheet1", fmt.Sprintf("C%v", i+1), t.Income)
		file.SetCellValue("Sheet1", fmt.Sprintf("D%v", i+1), t.Outcome)

	}
	fileByte, errFile := file.WriteToBuffer()
	if errFile != nil {
		return errFile
	}

	return c.Blob(http.StatusOK, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileByte.Bytes())

}

func (h Handler) GetReport(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	since := c.QueryParam("since")
	to := c.QueryParam("to")
	if since == "" {
		since = time.Time{}.String()
	}
	if to == "" {
		to = time.Now().String()
	}
	user, err := h.s.Report(c.Request().Context(), claims.UserID, "", since, to)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: ""})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetReportAdmin(c echo.Context) error {

	since := c.QueryParam("since")
	to := c.QueryParam("to")
	manager := c.QueryParam("managerId")
	var managerId int
	if since == "" {
		since = time.Time{}.String()
	}
	if to == "" {
		to = time.Now().String()
	}
	if manager != "" {
		managerId, _ = strconv.Atoi(manager)
	}
	user, err := h.s.Report(c.Request().Context(), managerId, "", since, to)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: ""})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) AddTransaction(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	rq := model.Finance{}
	since := time.Time{}.String()
	to := time.Now().String()
	err := c.Bind(&rq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "bad request"})
	}
	user, err := h.s.CreateTransaction(c.Request().Context(), rq, claims.UserID, "", since, to)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: ""})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) UserBalanceWriteOff(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	rq := model.UserBalanceRequest{}
	err := c.Bind(&rq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "bad request"})
	}
	balance := decimal.NewFromFloat(rq.Balance)
	user, err := h.s.WriteOffBalance(c.Request().Context(), rq.UserID, balance, claims.UserID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetCustomersList(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	users, err := h.s.GetCustomersList(int64(claims.UserID))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "internal error"})
	}
	return c.JSON(http.StatusOK, users)
}

func (h Handler) GetUsersList(c echo.Context) error {
	users, err := h.s.GetUsersList()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "internal error"})
	}
	return c.JSON(http.StatusOK, users)
}

func (h Handler) GetUserByID(c echo.Context) error {
	userID, parseErr := strconv.Atoi(c.Param("id"))
	if parseErr != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}
	user, err := h.s.GetUserByID(c.Request().Context(), userID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) UpdateUserByID(c echo.Context) error {
	userID, parseErr := strconv.Atoi(c.Param("id"))
	if parseErr != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}
	rsData := model.UpdateUserRequest{}
	bindErr := c.Bind(&rsData)
	if bindErr != nil {
		h.cLogger.WarnCtx(c.Request().Context(), "bind error: "+bindErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "request error"})
	}
	user, err := h.s.UpdateUserByID(c.Request().Context(), userID, rsData)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "update error"})
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetAllInvoices(c echo.Context) error {

	shipper := c.Request().URL.Query().Get("shipper")
	recipient := c.Request().URL.Query().Get("recipient")
	id := c.Request().URL.Query().Get("id")
	status := c.Request().URL.Query().Get("status")
	invoices, err := h.s.GetInvoices(shipper, recipient, id, status, c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "error"})
	}
	return c.JSON(http.StatusOK, invoices)
}

func (h Handler) GetAllInvoicesTable(c echo.Context) error {
	shipper := c.Request().URL.Query().Get("shipper")
	recipient := c.Request().URL.Query().Get("recipient")
	id := c.Request().URL.Query().Get("id")
	status := c.Request().URL.Query().Get("status")
	invoices, err := h.s.GetInvoices(shipper, recipient, id, status, c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "error"})
	}

	var table model.Table
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "inoviceNum",
		Label:          "Номер накладной",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "status",
		Label:          "Статус",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "date",
		Label:          "Дата",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "shipper",
		Label:          "Перевозчик",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "recipient",
		Label:          "Получатель",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "total",
		Label:          "Сумма",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "manager",
		Label:          "Менеджер",
		Numeric:        false,
		DisablePadding: false,
	}}
	table.Data = invoices
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetInvoiceDetails(c echo.Context) error {

	//shipper := c.Request().URL.Query().Get("shipper")
	//recipient := c.Request().URL.Query().Get("recipient")
	id := c.Param("ID")
	//status := c.Request().URL.Query().Get("status")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return err
	}
	invoices, err := h.s.GetInvoice(idInt, c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "error"})
	}
	return c.JSON(http.StatusOK, invoices)
}

func (h Handler) ApproveInvoiceDetails(c echo.Context) error {

	//shipper := c.Request().URL.Query().Get("shipper")
	//recipient := c.Request().URL.Query().Get("recipient")

	rq := model.ApproveInvoiceRq{}
	errB := c.Bind(&rq)
	if errB != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "bad request"})
	}
	errAI := h.s.ApproveInvoice(c.Request().Context(), rq)

	if errAI != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: fmt.Sprintf("%v", errAI)})
	}
	return c.JSON(http.StatusOK, model.Error{Message: "ok"})
}
