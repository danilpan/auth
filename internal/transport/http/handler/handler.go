package handler

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/shopspring/decimal"
	"github.com/xuri/excelize/v2"
	"gitlab.com/danilpan/logger"
	"io"
	"io/ioutil"
	"net/http"
	"processor/internal/config"
	"processor/internal/model"
	"processor/internal/repository"
	"processor/internal/service"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	SUCCESS = "success"
)

type Handler struct {
	cfg     *config.Config
	repo    *repository.Repository
	cLogger *logger.Logger
	s       *service.Service
}

func NewHandler(config *config.Config, r *repository.Repository, logger *logger.Logger, s *service.Service) *Handler {
	return &Handler{
		cfg:     config,
		repo:    r,
		cLogger: logger,
		s:       s,
	}
}

func removeDuplicates(strSlice []string) []string {
	allKeys := make(map[string]bool)
	var list []string
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func (h Handler) HealthCheck(c echo.Context) error {
	return c.String(200, "Test")
}

func (h Handler) RegisterUser(c echo.Context) error {
	var user model.User
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: err.Error()})
	}
	if err := user.HashPassword(user.Password); err != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: err.Error()})
	}
	data, userErr := h.repo.CreateCustomer(user)
	if userErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: userErr.Error()})
	}

	//verificationCode, verErr := h.s.GetVerificationCodeByUserID(c.Request().Context(), data.ID)
	//if verErr != nil {
	//	return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: verErr.Error()})
	//}
	//emailErr := h.s.SendMail(c.Request().Context(), data, verificationCode)
	//if emailErr != nil {
	//	return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: emailErr.Error()})
	//}

	return c.JSON(http.StatusCreated, *data)
}

func (h Handler) GenerateToken(c echo.Context) error {
	var request model.TokenRequest
	var User *model.User
	if err := c.Bind(&request); err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: err.Error()})
	}

	User, repErr := h.repo.GetUser(request.Username)
	if repErr != nil {
		h.cLogger.Warn(repErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
	}

	credentialError := User.CheckPassword(request.Password)
	if credentialError != nil {
		h.cLogger.Warn(credentialError.Error())
		if User.ID > 0 {
			return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "wrong password"})
		}
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "wrong credentials"})
	}

	if !*User.IsActive {
		verificationCode, verErr := h.s.GetVerificationCodeByUserID(c.Request().Context(), User.ID)
		if verErr != nil {
			return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: verErr.Error()})
		}
		emailErr := h.s.SendMail(c.Request().Context(), User, verificationCode)
		if emailErr != nil {
			return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: emailErr.Error()})
		}
		return c.JSON(http.StatusForbidden, model.ResponseMessage{Message: "пользователь не активирован, пройдите по ссылке, отправленной на почту, указанную при регистрации"})
	}

	tokenString, jwtErr := service.GenerateJWT(User)
	if jwtErr != nil {
		h.cLogger.Warn(jwtErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: jwtErr.Error()})
	}
	return c.JSON(http.StatusOK, model.Token{
		AccessToken:      tokenString,
		RefreshToken:     "",
		ExpiresIn:        86400,
		RefreshExpiresIn: 0,
		TokenType:        "Bearer",
	})
}

func (h Handler) AddCartItem(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data := model.AddCartItem{}
	bindErr := c.Bind(&data)
	if bindErr != nil {
		h.cLogger.Warn(bindErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: bindErr.Error()})
	}
	data.UserID = claims.UserID

	repErr := h.repo.AddCartItem(data)
	if repErr != nil {
		h.cLogger.Warn(repErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
	}

	return c.JSON(http.StatusOK, model.ResponseMessage{
		Message: SUCCESS,
		Success: true,
	})
}

func (h Handler) CartItemQuantity(c echo.Context) error {
	//claims := c.Get("claims").(*service.JWTClaim)
	data := model.CartItemQuantity{}
	bindErr := c.Bind(&data)
	if bindErr != nil {
		h.cLogger.Warn(bindErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: bindErr.Error()})
	}

	repErr := h.repo.CartItemUpdateQuantity(data)
	if repErr != nil {
		h.cLogger.Warn(repErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
	}

	return c.JSON(http.StatusOK, model.ResponseMessage{
		Message: SUCCESS,
		Success: true,
	})
}

func (h Handler) ConfirmCartItems(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data := []model.ConfirmCartRequest{}
	bindErr := c.Bind(&data)
	if bindErr != nil {
		h.cLogger.Warn(bindErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: bindErr.Error()})
	}

	for _, v := range data {
		if v.Confirmed == false {
			continue
		}
		repErr := h.repo.ConfirmCartItem(claims.UserID, v.ID)
		if repErr != nil {
			h.cLogger.Warn(repErr.Error())
			return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
		}
	}

	return c.JSON(http.StatusOK, model.ResponseMessage{
		Message: SUCCESS,
		Success: true,
	})
}

func (h Handler) DeleteCartItems(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data := []model.DeleteCartRequest{}
	bindErr := c.Bind(&data)
	if bindErr != nil {
		h.cLogger.Warn(bindErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: bindErr.Error()})
	}

	for _, v := range data {
		repErr := h.repo.DeleteCartItem(claims.UserID, v.ID)
		if repErr != nil {
			h.cLogger.Warn(repErr.Error())
			return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
		}
	}

	return c.JSON(http.StatusOK, model.ResponseMessage{
		Message: SUCCESS,
		Success: true,
	})
}

func (h Handler) ConfirmToOrder(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	ctx := c.Request().Context()

	data := []model.ConfirmOrderRequest{}
	bindErr := c.Bind(&data)
	if bindErr != nil {
		h.cLogger.Warn(bindErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: bindErr.Error()})
	}
	info, errGUB := h.s.GetUserByID(ctx, claims.UserID)
	if errGUB != nil {
		h.cLogger.Warn(errGUB.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: errGUB.Error()})
	}
	repErr := h.s.ConfirmToOrder(ctx, claims.UserID, *info.ManagerID, &data)
	if repErr != nil {
		h.cLogger.Warn(repErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
	}

	return c.JSON(http.StatusOK, model.ResponseMessage{
		Message: SUCCESS,
		Success: true,
	})
}

func (h Handler) GetUserInvoices(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	ctx := c.Request().Context()
	var table model.Table
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер накладной",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "invoiceNum",
		Label:          "Название",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "Date",
		Label:          "Дата",
		Numeric:        false,
		DisablePadding: false}}
	rs, repErr := h.s.GetUserInvoices(ctx, claims.Username)
	if repErr != nil {
		h.cLogger.Warn(repErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
	}
	table.Data = rs
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetUserInvoice(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	ctx := c.Request().Context()
	id := c.Param("id")
	//status := c.Request().URL.Query().Get("status")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return err
	}
	rs, repErr := h.s.GetUserInvoice(ctx, claims.Username, int64(idInt))
	if repErr != nil {
		h.cLogger.Warn(repErr.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: repErr.Error()})
	}
	file := excelize.NewFile()
	file.SetCellValue("Sheet1", fmt.Sprintf("A%v", 1), "Номер накладной")
	file.SetCellValue("Sheet1", fmt.Sprintf("B%v", 1), "Номер коробки")
	file.SetCellValue("Sheet1", fmt.Sprintf("C%v", 1), "Производитель")
	file.SetCellValue("Sheet1", fmt.Sprintf("D%v", 1), "Номер запчасти")
	file.SetCellValue("Sheet1", fmt.Sprintf("E%v", 1), "Название запчасти")
	file.SetCellValue("Sheet1", fmt.Sprintf("F%v", 1), "Количество")
	file.SetCellValue("Sheet1", fmt.Sprintf("G%v", 1), "Цена")
	file.SetCellValue("Sheet1", fmt.Sprintf("H%v", 1), "Страна производитель")
	file.SetCellValue("Sheet1", fmt.Sprintf("I%v", 1), "Бренд")
	file.SetCellValue("Sheet1", fmt.Sprintf("J%v", 1), "Штрихкод")
	file.SetCellValue("Sheet1", fmt.Sprintf("K%v", 1), "HS код")
	file.SetCellValue("Sheet1", fmt.Sprintf("L%v", 1), "Производитель 2")
	file.SetCellValue("Sheet1", fmt.Sprintf("M%v", 1), "Номер запчасти 2")
	file.SetCellValue("Sheet1", fmt.Sprintf("N%v", 1), "Логин")
	file.SetCellValue("Sheet1", fmt.Sprintf("O%v", 1), "Референс")
	for i, t := range rs {

		file.SetCellValue("Sheet1", fmt.Sprintf("A%v", i+2), t.InvoiceId)
		file.SetCellValue("Sheet1", fmt.Sprintf("B%v", i+2), t.Box)
		file.SetCellValue("Sheet1", fmt.Sprintf("C%v", i+2), t.Brand)
		file.SetCellValue("Sheet1", fmt.Sprintf("D%v", i+2), t.PartNumber)
		file.SetCellValue("Sheet1", fmt.Sprintf("E%v", i+2), t.PartName)
		file.SetCellValue("Sheet1", fmt.Sprintf("F%v", i+2), t.Quantity)
		file.SetCellValue("Sheet1", fmt.Sprintf("G%v", i+2), t.Price)
		file.SetCellValue("Sheet1", fmt.Sprintf("H%v", i+2), t.OriginCountry)
		file.SetCellValue("Sheet1", fmt.Sprintf("I%v", i+2), t.BrandName)
		file.SetCellValue("Sheet1", fmt.Sprintf("J%v", i+2), t.Barcode)
		file.SetCellValue("Sheet1", fmt.Sprintf("K%v", i+2), t.HsCode)
		file.SetCellValue("Sheet1", fmt.Sprintf("L%v", i+2), t.CrossBrand)
		file.SetCellValue("Sheet1", fmt.Sprintf("M%v", i+2), t.CrossNumber)
		file.SetCellValue("Sheet1", fmt.Sprintf("N%v", i+2), t.Login)
		file.SetCellValue("Sheet1", fmt.Sprintf("O%v", i+2), t.ReferenceNum)
	}
	fileByte, errFile := file.WriteToBuffer()
	if errFile != nil {
		return errFile
	}

	return c.Blob(http.StatusOK, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileByte.Bytes())

}

func (h Handler) Search(c echo.Context) error {
	var res model.SearchRs
	var userId int
	if c.Request().Header.Get("Authorization") != "" {
		authHeader := c.Request().Header.Get("Authorization")
		token := strings.TrimPrefix(authHeader, "Bearer ")

		claims, err := service.ValidateToken(token)
		if err != nil {
			return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: err.Error()})
		}

		c.Set("claims", claims)
		claim := c.Get("claims").(*service.JWTClaim)
		userId = claim.UserID
	} else {
		userId = 0
	}
	partNum := c.Param("NUM")
	rate, errGC := h.s.GetCourse()
	if errGC != nil {
		return errGC
	}
	rs, err := h.s.SearchEmex(c.Request().Context(), partNum, userId, rate)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err)
	}
	rsRu, errRu := h.s.SearchEmexRu(partNum, userId, decimal.NewFromFloat(2.7))
	if errRu != nil {
		h.cLogger.WarnCtx(c.Request().Context(), errRu)
	}
	rsAt, errAt := h.s.SearchArmtek(c.Request().Context(), partNum, userId, decimal.NewFromInt(1))
	if errAt != nil {
		h.cLogger.WarnCtx(c.Request().Context(), errAt)
	}
	rsS, errR := h.s.SearchRossko(c.Request().Context(), partNum, userId, decimal.NewFromInt(1))
	if errR != nil {
		h.cLogger.WarnCtx(c.Request().Context(), errR)
	}
	var makesRu []string
	for _, i := range rsRu {
		makesRu = append(makesRu, i.MakeName)
	}
	var makes []string
	for _, i := range rs {
		makes = append(makes, i.MakeName)
	}
	var makesAt []string
	for _, i := range rsAt {
		makesAt = append(makesAt, i.MakeName)
	}
	var makesS []string
	for _, i := range rsS {
		makesS = append(makesS, i.MakeName)
	}
	makes = append(makes, makesRu...)
	makes = append(makes, makesAt...)
	makes = append(makes, makesS...)
	makes = removeDuplicates(makes)
	res.Makes = makes
	res.Parts = rs
	res.Parts = append(res.Parts, rsRu...)
	res.Parts = append(res.Parts, rsAt...)
	res.Parts = append(res.Parts, rsS...)

	res.Parts, _ = sortNumbers(res.Parts)
	return c.JSON(http.StatusOK, res)
}

func (h Handler) GetCartItems(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, err := h.repo.GetCartItems(claims.UserID)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetCartItemsTable(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, err := h.repo.GetCartItems(claims.UserID)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}

	var table model.Table
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "part_number",
		Label:          "Номер детали",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "brand",
		Label:          "Изготовитель",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:    "booking",
		Label: "Резерв",
	}, {
		Id:    "delivery",
		Label: "Доставка",
	}, {
		Id:    "destination",
		Label: "Пункт назначения",
	}, {
		Id:    "transport",
		Label: "Транспорт",
	}, {
		Id:             "quantity",
		Label:          "Количество",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "price",
		Label:          "Цена",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:    "currency",
		Label: "Валюта",
	}, {
		Id:             "reference",
		Label:          "Референс",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:      "order_id",
		Label:   "Номер заказа",
		Numeric: true,
	}, {
		Id:    "confirmed",
		Label: "Подтвержден",
	}, {
		Id:             "user_id",
		Label:          "Номер клиента",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:    "makeLogo",
		Label: "Make Logo",
	}, {
		Id:    "priceLogo",
		Label: "Склад",
	}, {
		Id:      "weight",
		Label:   "Масса, кг",
		Numeric: true,
	}, {
		Id:    "origin",
		Label: "Поставщик",
	}, {
		Id:    "part_id",
		Label: "Код запчасти",
	}, {
		Id:    "availability",
		Label: "Доступное количество",
	}, {
		Id:    "make_num",
		Label: "Наименование бренда",
	}, {
		Id:    "part_name_rus",
		Label: "Наименование запчасти",
	}, {
		Id:    "created_at",
		Label: "Время создания",
	}}
	table.Data = data
	return c.JSON(http.StatusOK, table)
}

func (h Handler) AddToEmexCart(c echo.Context) error {
	orderItemId, parseErr := strconv.Atoi(c.Param("id"))
	if parseErr != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}
	orderItem, err := h.s.GetOrderItem(orderItemId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}

	if orderItem.Booking == "emex" {
		var req []model.Partstobasket

		req = append(req, model.Partstobasket{
			CoeffMaxAgree:       "1",
			UploadedPrice:       fmt.Sprintf("%v", orderItem.OriginalPrice),
			BitAgree:            "true",
			OnlyThisBrand:       "true",
			Confirm:             "true",
			Delete:              "true",
			BasketId:            "101",
			MakeLogo:            orderItem.Brand,
			DetailNum:           orderItem.PartNumber,
			DestinationLogo:     "0001",
			PriceLogo:           *orderItem.StockName,
			Quantity:            fmt.Sprintf("%v", orderItem.Quantity),
			BitOnly:             "true",
			BitQuantity:         "true",
			BitWait:             "true",
			Reference:           fmt.Sprintf("%v", orderItem.Reference),
			CustomerSubId:       "100",
			TransportPack:       "String",
			DetailWeight:        fmt.Sprintf("%v", orderItem.WeightKg),
			EmExWeight:          fmt.Sprintf("%v", orderItem.WeightKg),
			CustomerStickerData: fmt.Sprintf("%v", orderItem.Reference),
		})

		errAdd := h.s.AddEmexBasket(req)
		if errAdd != nil {
			return errAdd
		}
		errU := h.repo.UpdateOrderItemStatus("в работе", orderItemId)
		if errU != nil {
			return errU
		}
	} else if orderItem.Booking == "emexru" {
		var req []model.PartRu

		req = append(req, model.PartRu{
			Num:                orderItem.PartNumber,
			DLogo:              "REG",
			MName:              "",
			MLogo:              orderItem.Brand,
			DNum:               orderItem.PartNumber,
			Ref:                orderItem.Reference.(string),
			Name:               "",
			Com:                "",
			Quan:               orderItem.Quantity.(string),
			Price:              fmt.Sprintf("%v", orderItem.Price),
			PLogo:              *orderItem.StockName,
			Notc:               "",
			Error:              "",
			DeliveryRegionType: "PRI",
		})

		errAdd := h.s.AddEmexRuBasket(req)
		if errAdd != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("add emex ru %v", errAdd)})
		}
		errU := h.repo.UpdateOrderItemStatus("в работе", orderItemId)
		if errU != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("update order irems %v", errU)})
		}
	} else if orderItem.Booking == "arm" {
		var req []model.ArmtekItem
		req = append(req, model.ArmtekItem{
			PIN:      orderItem.PartNumber,
			BRAND:    orderItem.Brand,
			KWMENG:   orderItem.Quantity.(string),
			KEYZAK:   *orderItem.StockName,
			COMPLDLV: "1",
		})
		ordes, errAdd := h.s.AddArmtekOrder(req)
		if errAdd != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("add emex ru %v", errAdd)})
		}
		errUO := h.repo.UpdateOrderItem(ordes.RESP.ITEMS[0].RESULT[0].VBELN, orderItemId)
		if errUO != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("update order irems %v", errUO)})
		}
		errU := h.repo.UpdateOrderItemStatus("в работе", orderItemId)
		if errU != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("update order irems %v", errU)})
		}
	} else if orderItem.Booking == "rossko" {
		var req []model.RosskoPart

		req = append(req, model.RosskoPart{
			Partnumber: orderItem.PartNumber,
			Brand:      orderItem.Brand,
			Stock:      *orderItem.StockName,
			Count:      orderItem.Quantity.(string),
		})
		errAdd := h.s.AddRossko(req)
		if errAdd != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("add emex ru %v", errAdd)})
		}
		errU := h.repo.UpdateOrderItemStatus("в работе", orderItemId)
		if errU != nil {
			return c.JSON(http.StatusInternalServerError, model.Error{Message: fmt.Sprintf("update order irems %v", errU)})
		}
	}
	return c.JSON(http.StatusOK, model.Error{Message: "ok"})
}
func (h Handler) CancelOrder(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	orderItemId, parseErr := strconv.Atoi(c.Param("id"))
	if parseErr != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}
	orderItem, err := h.s.GetOrderItem(orderItemId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid id"})
	}
	var refs []string
	refs = append(refs, fmt.Sprintf("%v", orderItem.Reference))
	dataUOIB := h.repo.UpdateOrderItemsBatch(c.Request().Context(), refs, "Отменен менеджером")
	if dataUOIB != nil {
		h.cLogger.Warn("approve invoices error")
		return dataUOIB
	}
	quantity, parseErrQ := strconv.Atoi(fmt.Sprintf("%v", orderItem.Quantity))
	if parseErrQ != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "invalid quantity"})
	}
	oPrice, _ := decimal.NewFromString(orderItem.Price)
	price := decimal.NewFromInt(int64(quantity)).Mul(oPrice)
	order, errGEO := h.s.GetOrderByID(orderItem.OrderID, 0, 1)
	if errGEO != nil {
		h.cLogger.Warn(errGEO.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: errGEO.Error()})
	}
	_, errB := h.repo.BalanceReplenishment(c.Request().Context(), order.UserID, price, claims.UserID)
	if errB != nil {
		h.cLogger.Warn(errB.Error())
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: errB.Error()})
	}
	return c.JSON(http.StatusOK, nil)
}

func (h Handler) ChangeStatus(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.OrderItemsStatus
	err := c.Bind(&rq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.Error{Message: "Bad Email"})
	}
	var refs []int64
	if len(rq.Id) > 0 {
		for _, i := range rq.Id {
			refs = append(refs, i.Id)
		}
	}
	dataUOIB := h.repo.UpdateOrderItemsBatchById(c.Request().Context(), refs, rq.Status)
	if dataUOIB != nil {
		h.cLogger.Warn("approve invoices error")
		return dataUOIB
	}

	return c.JSON(http.StatusOK, nil)
}

func (h Handler) CheckPdf(c echo.Context) error {
	var msg model.Error
	buffer := &bytes.Buffer{}

	name := c.Param("name")
	err := c.Request().ParseMultipartForm(32 << 20) // 32MB max upload size
	if err != nil {
		msg.Message = "Error: Bad File"
		h.cLogger.ErrorCtx(c.Request().Context(), msg)
		return c.JSON(http.StatusBadRequest, msg)
	}
	file, _, err := c.Request().FormFile("document")
	if err != nil {
		msg.Message = "Error: Document Read"
		h.cLogger.ErrorCtx(c.Request().Context(), msg)
		return c.JSON(http.StatusBadRequest, msg)
	}
	defer file.Close()
	if _, err := io.Copy(buffer, file); err != nil {
		msg.Message = "Error: Copy File"
		h.cLogger.ErrorCtx(c.Request().Context(), msg)
		return c.JSON(http.StatusBadRequest, msg)
	}

	imageBytes, err := ioutil.ReadAll(buffer)
	if err != nil {
		msg.Message = "Error: create byte array"
		h.cLogger.ErrorCtx(c.Request().Context(), msg)
		return c.JSON(http.StatusBadRequest, msg)
	}
	fileName := uuid.NewString()
	err = ioutil.WriteFile(fmt.Sprintf("%v.xlsx", fileName), imageBytes, 0777)
	if err != nil {
		msg.Message = "Error: create file"
		h.cLogger.ErrorCtx(c.Request().Context(), msg)
		return c.JSON(http.StatusBadRequest, msg)
	}
	f, err := excelize.OpenFile(fmt.Sprintf("%v.xlsx", fileName))
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			h.cLogger.Warn(err.Error())
		}
	}()
	// Get value from cell by given worksheet name and axis.

	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("Sheet1")
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	id, err := h.repo.CreateStock(name, c.Request().Context())
	if err != nil {
		h.cLogger.Warn(fmt.Sprintf("create invoice error, %v", err))
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	var parts []model.ExcelPart
	for _, row := range rows {
		var part model.ExcelPart
		stock, parseErr := strconv.Atoi(row[3])
		if parseErr != nil {
			h.cLogger.WarnCtx(c.Request().Context(), "parse err: "+parseErr.Error())
			return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "wrong building_id"})
		}

		price, errD := decimal.NewFromString(row[4])
		if errD != nil {
			h.cLogger.WarnCtx(c.Request().Context(), "parse err: "+errD.Error())
			return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "wrong building_id"})
		}
		part.Brand = row[0]
		part.Name = row[1]
		part.DetailNum = row[2]
		part.Stock = stock
		part.Price = price
		part.StockId = id
		parts = append(parts, part)
	}
	errAdd := h.repo.CreatePart(parts, c.Request().Context())
	if errAdd != nil {
		return c.JSON(http.StatusInternalServerError, errAdd)
	}
	return c.JSON(http.StatusOK, model.Error{Message: "ok"})
}

func (h Handler) CreateInvoice(c echo.Context) error {
	var msg model.Error
	var rq model.InvoiceMail
	err := c.Bind(&rq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.Error{Message: "Bad Email"})
	}
	if rq.Attachments == nil || len(rq.Attachments) == 0 {
		return c.JSON(http.StatusBadRequest, rq)
	} else {
		var name string
		var imageBytes []byte
		for _, i := range rq.Attachments {
			if strings.Contains(i.FileName, "InvoiceFull") {
				name = fmt.Sprintf("%v %v", i.FileName, time.Now())

				imageBytes, err = base64.StdEncoding.DecodeString(i.Content)
				if err != nil {
					return c.JSON(http.StatusBadRequest, model.Error{Message: "decode err"})
				}

			}
		}
		err = ioutil.WriteFile(fmt.Sprintf("%v.xlsx", name), imageBytes, 0777)
		if err != nil {
			msg.Message = "Error: create file"
			h.cLogger.ErrorCtx(c.Request().Context(), msg)
			return c.JSON(http.StatusBadRequest, msg)
		}
		f, err := excelize.OpenFile(fmt.Sprintf("%v.xlsx", name))
		if err != nil {
			h.cLogger.Warn(err.Error())
			return err
		}
		defer func() {
			// Close the spreadsheet.
			if err := f.Close(); err != nil {
				h.cLogger.Warn(err.Error())
			}
		}()
		// Get value from cell by given worksheet name and axis.

		// Get all the rows in the Sheet1.
		rows, err := f.GetRows("INVOICE")
		if err != nil {
			h.cLogger.Warn(err.Error())
			return err
		}
		invoice := model.Invoice{
			InvoiceNum: name,
			Status:     "added",
			Date:       time.Now(),
			Shipper:    "test",
			Recipient:  "xamilion",
			Total:      0,
			Manager:    "mail",
		}
		errAdd := h.s.CreateInvoice(rows, invoice, c.Request().Context())
		if errAdd != nil {
			return c.JSON(http.StatusInternalServerError, errAdd)
		}
	}

	return c.JSON(http.StatusOK, model.Error{Message: "ok"})
}

func (h Handler) CreateInvoiceFile(c echo.Context) error {
	var rq model.Error
	var msg model.Error
	err := c.Bind(&rq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.Error{Message: "Bad Email"})
	}
	name := uuid.NewString()
	imageBytes, err := base64.StdEncoding.DecodeString(rq.Message)
	if err != nil {
		return c.JSON(http.StatusBadRequest, model.Error{Message: "decode err"})
	}

	err = ioutil.WriteFile(fmt.Sprintf("%v.xlsx", name), imageBytes, 0777)
	if err != nil {
		msg.Message = "Error: create file"
		h.cLogger.ErrorCtx(c.Request().Context(), msg)
		return c.JSON(http.StatusBadRequest, msg)
	}
	f, err := excelize.OpenFile(fmt.Sprintf("%v.xlsx", name))
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			h.cLogger.Warn(err.Error())
		}
	}()
	// Get value from cell by given worksheet name and axis.

	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("INVOICE")
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	invoice := model.Invoice{
		InvoiceNum: name,
		Status:     "added",
		Date:       time.Now(),
		Shipper:    "test",
		Recipient:  "xamilion",
		Total:      0,
		Manager:    "mail",
	}
	errAdd := h.s.CreateInvoice(rows, invoice, c.Request().Context())
	if errAdd != nil {
		return c.JSON(http.StatusInternalServerError, errAdd)
	}

	return c.JSON(http.StatusOK, model.Error{Message: "ok"})
}

func (h Handler) GetUserInfo(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	user, err := h.s.GetProfileByID(c.Request().Context(), claims.UserID)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetOrderItems(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	partNumber := c.QueryParam("part_number") //артикул поиск
	reference := c.QueryParam("reference")    //референс
	partName := c.QueryParam("part_name")     //наименование
	brand := c.QueryParam("brand")            //производитель
	manager := c.QueryParam("manager")        //менеджер (дропдаун)
	client := c.QueryParam("client")          //клиент(дропдаун)
	supplier := c.QueryParam("supplier")      //поставщик(дропдаун)
	status := c.QueryParam("status")
	user, err := h.s.GetOrderItems(claims.UserID, partNumber, reference, partName, brand, manager, client, supplier, status, c.Request().Context())
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	his := model.History{
		Date:    time.Now().String(),
		Status:  "создан",
		Comment: "заказ создан",
	}
	for i, _ := range user {
		user[i].History = append(user[i].History, his)
		user[i].Link = fmt.Sprintf("https://www.google.com/search?q=%v", user[i].PartNumber)
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetOrderItemsAll(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	//user, err := h.s.GetOrderItemsAll(claims.UserID, c.Request().Context())
	//if err != nil {
	//	h.cLogger.Warn(err.Error())
	//	return err
	//}
	partNumber := c.QueryParam("part_number") //артикул поиск
	reference := c.QueryParam("reference")    //референс
	partName := c.QueryParam("part_name")     //наименование
	brand := c.QueryParam("brand")            //производитель
	manager := c.QueryParam("manager")        //менеджер (дропдаун)
	client := c.QueryParam("client")          //клиент(дропдаун)
	supplier := c.QueryParam("supplier")      //поставщик(дропдаун)
	status := c.QueryParam("status")

	user, err := h.s.GetOrderItemsAllV2(claims.UserID, partNumber, reference, partName, brand, manager, client, supplier, status, c.Request().Context())
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	his := model.History{
		Date:    time.Now().String(),
		Status:  "создан",
		Comment: "заказ создан",
	}
	for i, _ := range user {
		user[i].History = append(user[i].History, his)
		user[i].Link = fmt.Sprintf("https://www.google.com/search?q=%v", user[i].PartNumber)
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) TestTable(c echo.Context) error {
	var table model.Table
	claims := c.Get("claims").(*service.JWTClaim)
	partNumber := c.QueryParam("part_number") //артикул поиск
	reference := c.QueryParam("reference")    //референс
	partName := c.QueryParam("part_name")     //наименование
	brand := c.QueryParam("brand")            //производитель
	status := c.QueryParam("status")
	user, err := h.s.GetOrderItemsAllTable(claims.UserID, c.Request().Context(), partNumber, reference, partName, brand, status)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "orderId",
		Label:          "Номер заказа",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "reference",
		Label:          "Референс",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "partNumber",
		Label:          "Номер детали",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "brand",
		Label:          "Изготовитель",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "price",
		Label:          "Цена",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "quantity",
		Label:          "Количество",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "rejected",
		Label:          "Отмененные",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "amount",
		Label:          "Сумма",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "status",
		Label:          "Статус",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "comment",
		Label:          "Комментарий",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "stock",
		Label:          "Склад",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "invoice",
		Label:          "Накладная",
		Numeric:        false,
		DisablePadding: false,
	}}
	table.Data = user
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetOrderItemsManager(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	partNumber := c.QueryParam("part_number") //артикул поиск
	reference := c.QueryParam("reference")    //референс
	partName := c.QueryParam("part_name")     //наименование
	brand := c.QueryParam("brand")            //производитель
	manager := c.QueryParam("manager")        //менеджер (дропдаун)
	client := c.QueryParam("client")          //клиент(дропдаун)
	supplier := c.QueryParam("supplier")      //поставщик(дропдаун)
	status := c.QueryParam("status")
	user, err := h.s.GetOrderItems(claims.UserID, partNumber, reference, partName, brand, manager, client, supplier, status, c.Request().Context())
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) UpdateUserInfo(c echo.Context) error {
	var rs model.UpdateUserProfileRequest
	claims := c.Get("claims").(*service.JWTClaim)
	err := c.Bind(&rs)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	user, err := h.s.UpdateUserInfoByID(c.Request().Context(), claims.UserID, rs)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	rs.Email = user.Email
	rs.Username = user.Username
	rs.PhoneNumber = user.PhoneNumber
	return c.JSON(http.StatusOK, rs)
}

func (h Handler) UserOrders(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	user, err := h.s.GetProfileByID(c.Request().Context(), claims.UserID)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) GetAllLocal(c echo.Context) error {

	rs, err := h.repo.GetStock(c.Request().Context())
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, rs)
}
func (h Handler) GetLocal(c echo.Context) error {
	id := c.Param("id")
	rs, err := h.repo.GetStockById(c.Request().Context(), id)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, rs)
}

func (h Handler) Rossko(c echo.Context) error {

	user, err := h.s.SearchRossko(c.Request().Context(), "333114", 0, decimal.Decimal{})
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) Armtek(c echo.Context) error {

	user, err := h.s.SearchArmtek(c.Request().Context(), "333114", 0, decimal.Decimal{})
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func sortNumbers(data []model.Part) ([]model.Part, error) {
	var lastErr error
	sort.Slice(data, func(i, j int) bool {
		a, err := strconv.ParseFloat(data[i].Price, 64)
		if err != nil {
			lastErr = err
			return false
		}
		b, err := strconv.ParseFloat(data[j].Price, 64)
		if err != nil {
			lastErr = err
			return false
		}
		return a < b
	})
	return data, lastErr
}
