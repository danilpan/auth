package handler

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/xuri/excelize/v2"
	"net/http"
	"processor/internal/model"
	"processor/internal/service"
)

func (h Handler) GetCustomerTransactions(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.s.GetUserTransactions(claims.UserID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	var table model.Table
	var t []model.TransactionRs
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер операции",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "operationType",
		Label:          "Тип операции ",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "managerNumber",
		Label:          "Номер Менеджера",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "manager",
		Label:          "Менеджер",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "date",
		Label:          "Дата",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "amount",
		Label:          "Сумма",
		Numeric:        true,
		DisablePadding: false,
	}}
	for _, i := range *data {
		var transType, phone, login string
		if i.WriteOff {
			transType = "Списание"
		} else {
			transType = "Пополнение"
		}
		if i.Manager != nil {
			phone = i.Manager.PhoneNumber
			login = i.Manager.Username
		}
		t = append(t, model.TransactionRs{
			ID:              i.ID,
			Amount:          i.Amount,
			CreatedAt:       i.CreatedAt,
			TransactionType: transType,
			ManagerNumber:   phone,
			Manager:         login,
		})
	}
	table.Data = t
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetCustomerTransactionsExcel(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	from := c.QueryParam("from")
	to := c.QueryParam("to")
	data, dataErr := h.s.GetUserTransactionsExcel(claims.UserID, from, to)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	file := excelize.NewFile()
	for i, t := range *data {
		var transType string
		if t.WriteOff {
			transType = "Списание"
		} else {
			transType = "Пополнение"
		}
		file.SetCellValue("Sheet1", fmt.Sprintf("A%v", i+1), t.ID)
		file.SetCellValue("Sheet1", fmt.Sprintf("B%v", i+1), transType)
		if t.User != nil {
			file.SetCellValue("Sheet1", fmt.Sprintf("C%v", i+1), t.User.Username)
		} else {
			file.SetCellValue("Sheet1", fmt.Sprintf("C%v", i+1), claims.Username)
		}

		file.SetCellValue("Sheet1", fmt.Sprintf("D%v", i+1), t.Amount)
		file.SetCellValue("Sheet1", fmt.Sprintf("E%v", i+1), t.CreatedAt)

		if t.Manager != nil {
			file.SetCellValue("Sheet1", fmt.Sprintf("F%v", i+1), t.Manager.Username)
		} else {
			file.SetCellValue("Sheet1", fmt.Sprintf("F%v", i+1), " ")
		}

	}
	fileByte, errFile := file.WriteToBuffer()
	if errFile != nil {
		return errFile
	}

	return c.Blob(http.StatusOK, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileByte.Bytes())
}

func (h Handler) GetManagerTransactions(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.s.GetManagerTransactions(claims.UserID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	var table model.Table
	var t []model.TransactionRs
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер операции",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "operationType",
		Label:          "Тип операции ",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "managerNumber",
		Label:          "Номер Менеджера",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "manager",
		Label:          "Менеджер",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "date",
		Label:          "Дата",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "amount",
		Label:          "Сумма",
		Numeric:        true,
		DisablePadding: false,
	}}
	for _, i := range *data {
		var transType, phone, login string
		if i.WriteOff {
			transType = "Списание"
		} else {
			transType = "Пополнение"
		}
		if i.Manager != nil {
			phone = i.Manager.PhoneNumber
			login = i.Manager.Username
		}
		t = append(t, model.TransactionRs{
			ID:              i.ID,
			Amount:          i.Amount,
			CreatedAt:       i.CreatedAt,
			TransactionType: transType,
			ManagerNumber:   phone,
			Manager:         login,
		})
	}
	table.Data = t
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetAllTransactions(c echo.Context) error {
	data, dataErr := h.s.GetAllTransactions()
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	var table model.Table
	var t []model.TransactionRs
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер операции",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "operationType",
		Label:          "Тип операции ",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "managerNumber",
		Label:          "Номер Менеджера",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "manager",
		Label:          "Менеджер",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "date",
		Label:          "Дата",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "amount",
		Label:          "Сумма",
		Numeric:        true,
		DisablePadding: false,
	}}
	for _, i := range *data {
		var transType, phone, login string
		if i.WriteOff {
			transType = "Списание"
		} else {
			transType = "Пополнение"
		}
		if i.Manager != nil {
			phone = i.Manager.PhoneNumber
			login = i.Manager.Username
		}
		t = append(t, model.TransactionRs{
			ID:              i.ID,
			Amount:          i.Amount,
			CreatedAt:       i.CreatedAt,
			TransactionType: transType,
			ManagerNumber:   phone,
			Manager:         login,
		})
	}
	table.Data = t
	return c.JSON(http.StatusOK, table)
}
