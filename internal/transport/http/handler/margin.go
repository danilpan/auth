package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"processor/internal/model"
	"processor/internal/service"
)

func (h Handler) GetVolume(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	data, err := h.s.GetVolumeMargins(c.Request().Context())
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) AddVolume(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq []model.Volume
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.AddVolumeMargins(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) PatchVolume(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.Volume
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.PatchVolumeMargin(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) DeleteVolume(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.Volume
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.DeleteVolumeMargin(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetKeyWord(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	data, err := h.s.GetKeyWordMargins(c.Request().Context())
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) AddKeyWord(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq []model.KeyWord
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.AddKeyWordMargins(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) PatchKeyWord(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.KeyWord
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.PatchKeyWordMargin(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) DeleteKeyWord(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.KeyWord
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.DeleteKeyWordMargin(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

//group

func (h Handler) GetGroup(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	data, err := h.repo.GetGroupMargins(c.Request().Context())
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) AddGroup(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq []model.MarginTable
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.AddGroupMargins(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) PatchGroup(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.MarginTable
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.PatchGroupMargin(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) DeleteGroup(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.MarginTable
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.DeleteGroupMargin(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetCurrencies(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	data, err := h.repo.GetCurrencies(c.Request().Context())
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) AddCurrency(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq []model.Currency
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.AddCurrency(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) PatchCurrency(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.Currency
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.PatchCurrency(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) DeleteCurrency(c echo.Context) error {
	_ = c.Get("claims").(*service.JWTClaim)
	var rq model.Currency
	err := c.Bind(&rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	data, err := h.s.DeleteCurrency(c.Request().Context(), rq)
	if err != nil {
		h.cLogger.WarnCtx(c.Request().Context(), err.Error())
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "query error"})
	}
	return c.JSON(http.StatusOK, data)
}
