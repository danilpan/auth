package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"processor/internal/model"
	"processor/internal/service"
	"strconv"
)

func (h Handler) UpdateUserPassword(c echo.Context) error {
	var rs model.UpdatePasswordRequest
	claims := c.Get("claims").(*service.JWTClaim)
	err := c.Bind(&rs)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	user, err := h.s.UpdateUserPasswordByID(c.Request().Context(), claims.UserID, rs)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) UpdateUserPasswordByID(c echo.Context) error {
	var rs model.UpdatePasswordRequest
	id, idParseErr := strconv.Atoi(c.Param("id"))
	if idParseErr != nil {
		h.cLogger.Warn(idParseErr.Error())
		return idParseErr
	}
	err := c.Bind(&rs)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	user, err := h.s.UpdateUserPasswordByID(c.Request().Context(), id, rs)
	if err != nil {
		h.cLogger.Warn(err.Error())
		return err
	}
	return c.JSON(http.StatusOK, user)
}

func (h Handler) name() {

}

func (h Handler) VerifyActivationCode(c echo.Context) error {
	ctx := c.Request().Context()
	verificationCode := c.QueryParam("code")
	userId, idParseErr := strconv.Atoi(c.QueryParam("uid"))
	if idParseErr != nil {
		h.cLogger.WarnCtx(ctx, "parse error: "+idParseErr.Error())
		return idParseErr
	}
	verifyErr := h.s.VerifyActivationCode(ctx, verificationCode, userId)
	if verifyErr != nil {
		h.cLogger.WarnCtx(ctx, "verify error: "+verifyErr.Error())
		return verifyErr
	}
	//return c.JSON(http.StatusOK, model.ResponseMessage{Message: SUCCESS, Success: true})
	return c.Redirect(http.StatusSeeOther, "https://xamilion.kz")
}
