package handler

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"processor/internal/model"
	"processor/internal/service"
	"strconv"
)

func (h Handler) GetCustomerOrders(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.s.GetOrdersByUserID(claims.UserID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: fmt.Sprintf("can't get orders %v", dataErr)})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetOrders(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.repo.GetOrdersByManagerID(claims.UserID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetOrdersList(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.s.GetOrders(claims.UserID, claims.RoleID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetOrder(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	orderID, parseErr := strconv.Atoi(c.Param("order_id"))
	if parseErr != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "id not found"})
	}
	data, dataErr := h.s.GetOrderByID(orderID, claims.UserID, claims.RoleID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetOrderItem(c echo.Context) error {
	claims := c.Get("claims").(*service.JWTClaim)
	orderID, parseErr := strconv.Atoi(c.Param("order_id"))
	if parseErr != nil {
		return c.JSON(http.StatusBadRequest, model.ResponseMessage{Message: "id not found"})
	}
	data, dataErr := h.s.GetOrderItemsByID(orderID, claims.UserID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}

	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetUsersCarts(c echo.Context) error {
	//claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.s.GetUsersActiveCarts()
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetUsersCartsTable(c echo.Context) error {
	//claims := c.Get("claims").(*service.JWTClaim)
	data, dataErr := h.s.GetUsersActiveCartsTable()
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}

	var table model.Table
	table.Headers = []model.Header{{
		Id:             "user_id",
		Label:          "Номер клиента",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "user_email",
		Label:          "Почта клиента",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "user",
		Label:          "Пользователь",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "manager_id",
		Label:          "Номер менеджера",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "created_at",
		Label:          "Время создания",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "total_sum",
		Label:          "Сумма",
		Numeric:        true,
		DisablePadding: false,
	}}
	table.Data = data
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetUsersCartItemsTable(c echo.Context) error {
	userID, idErr := strconv.Atoi(c.Param("user_id"))
	if idErr != nil {
		return idErr
	}
	data, dataErr := h.s.GetUserActiveCartItems(userID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}

	var table model.Table
	table.Headers = model.CartItemsTable
	table.Data = data
	return c.JSON(http.StatusOK, table)
}

func (h Handler) GetUserCarts(c echo.Context) error {
	//claims := c.Get("claims").(*service.JWTClaim)
	userID, idErr := strconv.Atoi(c.Param("user_id"))
	if idErr != nil {
		return idErr
	}
	data, dataErr := h.s.GetUserActiveCarts(userID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}
	return c.JSON(http.StatusOK, data)
}

func (h Handler) GetUserCartsTable(c echo.Context) error {
	//claims := c.Get("claims").(*service.JWTClaim)
	userID, idErr := strconv.Atoi(c.Param("user_id"))
	if idErr != nil {
		return idErr
	}
	data, dataErr := h.s.GetUserActiveCarts(userID)
	if dataErr != nil {
		return c.JSON(http.StatusInternalServerError, model.ResponseMessage{Message: "can't get orders"})
	}

	var table model.Table
	table.Headers = []model.Header{{
		Id:             "id",
		Label:          "Номер",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "part_number",
		Label:          "Номер детали",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:             "brand",
		Label:          "Изготовитель",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:    "booking",
		Label: "Резерв",
	}, {
		Id:    "delivery",
		Label: "Доставка",
	}, {
		Id:    "destination",
		Label: "Пункт назначения",
	}, {
		Id:    "transport",
		Label: "Транспорт",
	}, {
		Id:             "quantity",
		Label:          "Количество",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:             "price",
		Label:          "Цена",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:    "currency",
		Label: "Валюта",
	}, {
		Id:             "reference",
		Label:          "Референс",
		Numeric:        false,
		DisablePadding: false,
	}, {
		Id:      "order_id",
		Label:   "Номер заказа",
		Numeric: true,
	}, {
		Id:    "confirmed",
		Label: "Подтвержден",
	}, {
		Id:             "user_id",
		Label:          "Номер клиента",
		Numeric:        true,
		DisablePadding: false,
	}, {
		Id:    "makeLogo",
		Label: "Make Logo",
	}, {
		Id:    "priceLogo",
		Label: "Склад",
	}, {
		Id:      "weight",
		Label:   "Масса, кг",
		Numeric: true,
	}, {
		Id:    "origin",
		Label: "Поставщик",
	}, {
		Id:    "part_id",
		Label: "Код запчасти",
	}, {
		Id:    "availability",
		Label: "Доступное количество",
	}, {
		Id:    "make_num",
		Label: "Наименование бренда",
	}, {
		Id:    "part_name_rus",
		Label: "Наименование запчасти",
	}, {
		Id:    "created_at",
		Label: "Время создания",
	}}
	table.Data = data
	return c.JSON(http.StatusOK, table)
}
