package http

import (
	"context"
	"github.com/labstack/echo/v4"
	echoMw "github.com/labstack/echo/v4/middleware"
	"gitlab.com/danilpan/logger"
	"net"
	"net/http"
	"processor/internal/config"
	"processor/internal/middleware"
	"processor/internal/repository"
	"processor/internal/service"
	"processor/internal/transport/http/handler"
	"time"
)

func Init(cfg *config.Config, ctx context.Context, repo *repository.Repository, logger *logger.Logger) *echo.Echo {
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:    100,
		IdleConnTimeout: 90 * time.Second,
	}
	client := &http.Client{
		Transport: transport,
	}
	defer client.CloseIdleConnections()

	ps := service.NewService(cfg, client, repo, logger)
	h := handler.NewHandler(cfg, repo, logger, ps)

	e := echo.New()
	e.Debug = true
	// Set Bundle MiddleWare
	e.Use(echoMw.Gzip())
	e.Use(echoMw.CORSWithConfig(echoMw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
	}))
	if errStartJobs := ps.StartScheduler(context.Background()); errStartJobs != nil {
		logger.Fatal(errStartJobs.Error())
	}
	// Routes
	e.GET("/live", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})
	e.GET("/ready", func(c echo.Context) error {
		return c.NoContent(http.StatusOK)
	})

	excludeUrls := make(map[string]interface{})
	excludeUrls["/ready"] = nil
	excludeUrls["/live"] = nil
	excludeUrls["/metrics"] = nil
	amw := middleware.NewAuthMiddleware(logger, cfg, repo, excludeUrls)
	e.Any("/api/v2/", h.BotHandler)
	e.POST("/api/v2/login", h.GenerateToken)                   //
	e.POST("/api/v2/registration", h.RegisterUser)             //
	e.GET("/api/v2/user/verification", h.VerifyActivationCode) //

	public := e.Group("/api/public")

	v2 := e.Group("/api/v2")

	v2.GET("/parts/:NUM", h.Search)
	v2.GET("/rossko", h.Rossko)
	v2.GET("/armtek", h.Armtek)
	v2.POST("/invoice", h.CreateInvoice)
	v2.POST("/invoice/file", h.CreateInvoiceFile)
	v2.Use(amw.ValidateToken)
	//user
	{
		v2.GET("/tracking", h.TestTable)
		v2.GET("/user/info", h.GetUserInfo)       //
		v2.POST("/user/update", h.UpdateUserInfo) //
		v2.POST("/user/password", h.UpdateUserPassword)
		v2.GET("/invoices", h.GetUserInvoices)
		v2.GET("/invoices/:id", h.GetUserInvoice)

		v2.GET("/cart/items", h.GetCartItems)            //
		v2.GET("/cart/items/table", h.GetCartItemsTable) //
		v2.POST("/cart/add", h.AddCartItem)              //
		v2.POST("/cart/quantity", h.CartItemQuantity)
		v2.POST("/cart/confirm", h.ConfirmCartItems) //
		v2.POST("/cart/delete", h.DeleteCartItems)   //
		v2.POST("/cart/order", h.ConfirmToOrder)     //
		v2.GET("/invoices", h.GetUserInvoices)
		v2.GET("/orders", h.GetCustomerOrders)  //
		v2.GET("/orders/:order_id", h.GetOrder) //
		v2.GET("/orders/:order_id/items", h.GetOrderItem)

		v2.GET("/transactions", h.GetCustomerTransactions)
		v2.GET("/transactions/excel", h.GetCustomerTransactionsExcel)
	}

	//admin
	adm := v2.Group("/admin")
	adm.Use(amw.AdminRole)
	{
		adm.POST("/balance_replenishment", h.UserBalanceReplenishment) //
		adm.POST("/balance_write_off", h.UserBalanceWriteOff)          //
		adm.GET("/customers/list", h.GetCustomersList)                 //
		adm.GET("/users/list", h.GetUsersList)                         //
		adm.GET("/users/:id", h.GetUserByID)                           //
		adm.POST("/users/:id", h.UpdateUserByID)                       //
		adm.POST("/users/password/:id", h.UpdateUserPasswordByID)      //
		adm.POST("/registration", h.UserRegistration)                  //

		adm.GET("/orders", h.GetOrdersList)      //
		adm.GET("/orders/:order_id", h.GetOrder) //

		adm.GET("/transactions", h.GetAllTransactions) //
		adm.GET("/report", h.GetReportAdmin)
		adm.GET("/report/excel", h.GetReportExcel)
		adm.POST("/report/transaction", h.AddTransaction)
		adm.GET("/invoices", h.GetAllInvoices)
		adm.GET("/invoices/table", h.GetAllInvoicesTable)
		adm.GET("/invoices/:ID/details", h.GetInvoiceDetails)
		adm.POST("/invoices/approve", h.ApproveInvoiceDetails)
		adm.POST("/orders/items/status", h.ChangeStatus)
		adm.GET("/orders/items", h.GetOrderItemsAll)
		adm.GET("/carts", h.GetUsersCarts)
		adm.GET("/carts/table", h.GetUsersCartsTable)
		adm.GET("/carts/table/:user_id", h.GetUsersCartItemsTable)
		adm.GET("/carts/user/:user_id", h.GetUserCarts)
		adm.GET("/cart/emex/add/:id", h.AddToEmexCart)

		adm.POST("/excel/:name", h.CheckPdf)
		adm.GET("/local", h.GetAllLocal)
		adm.GET("/local/:id", h.GetLocal)

		//volume margin
		adm.GET("/margin/volume", h.GetVolume)
		adm.POST("/margin/volume", h.AddVolume)
		adm.PATCH("/margin/volume", h.PatchVolume)
		adm.DELETE("/margin/volume", h.DeleteVolume)
		//keyword margin
		adm.GET("/margin/keyword", h.GetKeyWord)
		adm.POST("/margin/keyword", h.AddKeyWord)
		adm.PATCH("/margin/keyword", h.PatchKeyWord)
		adm.DELETE("/margin/keyword", h.DeleteKeyWord)
		//group margin
		adm.GET("/margin/group", h.GetGroup)
		adm.POST("/margin/group", h.AddGroup)
		adm.PATCH("/margin/group", h.PatchGroup)
		adm.DELETE("/margin/group", h.DeleteGroup)
		//currency
		adm.GET("/currency", h.GetCurrencies)
		adm.POST("/currency", h.AddCurrency)
		adm.PATCH("/currency", h.PatchCurrency)
		adm.DELETE("/currency", h.DeleteCurrency)
	}

	kass := v2.Group("/kassa")
	kass.Use(amw.KassaRole)
	{
		kass.POST("/balance_replenishment", h.UserBalanceReplenishment) //
		kass.POST("/balance_write_off", h.UserBalanceWriteOff)

		kass.GET("/transactions", h.GetAllTransactions) //
		kass.GET("/report", h.GetReport)
		kass.POST("/report/transaction", h.AddTransaction)
		kass.GET("/orders/items", h.GetOrderItemsAll)

	}

	mngr := v2.Group("/manager")
	mngr.Use(amw.ManagerRole)
	{
		mngr.POST("/balance_replenishment", h.UserBalanceReplenishment)
		mngr.POST("/balance_write_off", h.UserBalanceWriteOff)
		mngr.GET("/report", h.GetReport)
		mngr.GET("/report/excel", h.GetReportExcel)
		mngr.POST("/report/transaction", h.AddTransaction)
		mngr.GET("/customers/list", h.GetCustomersList)
		mngr.GET("/users/:id", h.GetUserByID)
		mngr.POST("/users/:id", h.UpdateUserByID)
		mngr.GET("/orders/list", h.GetOrders)
		mngr.GET("/orders/items", h.GetOrderItems)
		mngr.POST("/orders/items/status", h.ChangeStatus)
		mngr.GET("/transactions", h.GetManagerTransactions)
		mngr.POST("/excel/:name", h.CheckPdf)
		mngr.GET("/local", h.GetAllLocal)
		mngr.GET("/local/:id", h.GetLocal)
		mngr.GET("/cart/emex/add/:id", h.AddToEmexCart)
		mngr.POST("/orders/:id/cancel", h.CancelOrder)

		//volume margin
		mngr.GET("/margin/volume", h.GetVolume)
		mngr.POST("/margin/volume", h.AddVolume)
		mngr.PATCH("/margin/volume", h.PatchVolume)
		mngr.DELETE("/margin/volume", h.DeleteVolume)
		//keyword margin
		mngr.GET("/margin/keyword", h.GetKeyWord)
		mngr.POST("/margin/keyword", h.AddKeyWord)
		mngr.PATCH("/margin/keyword", h.PatchKeyWord)
		mngr.DELETE("/margin/keyword", h.DeleteKeyWord)
		//group margin
		mngr.GET("/margin/group", h.GetGroup)
		mngr.POST("/margin/group", h.AddGroup)
		mngr.PATCH("/margin/group", h.PatchGroup)
		mngr.DELETE("/margin/group", h.DeleteGroup)
		//Группы
		//mngr.GET("groups", h.GetGroups)
	}

	ctlg := public.Group("/catalogue")
	{
		ctlg.GET("/parts/top", h.GetTopParts)
		ctlg.GET("/list/city", h.GetCityList)
		ctlg.GET("/list/roles", h.GetRolesList)
		ctlg.GET("/list/managers", h.GetManagersList)
	}
	return e
}
