package http

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"processor/internal/config"
	"processor/internal/repository"

	"github.com/labstack/echo-contrib/jaegertracing"
	"github.com/labstack/echo-contrib/prometheus"
	"gitlab.com/danilpan/logger"
	"gitlab.com/danilpan/logger/middleware"
	"golang.yandex/hasql/checkers"
	hasql "golang.yandex/hasql/sqlx"
)

var (
	botToken = "5792153430:AAG3tL9zumpHN0jPtssiiP8kS_gIycnvlBI"
	baseURL  = "https://xamilion.kz/"
)

//func initTelegram() {
//	bot, err := tgbotapi.NewBotAPI("MyAwesomeBotToken")
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	bot.Debug = true
//
//	log.Printf("Authorized on account %s", bot.Self.UserName)
//	file, err := os.Open("/etc/letsencrypt/live/xamilion.kz")
//	var fileByte []byte
//	file.Read(fileByte)
//	wh, _ := tgbotapi.NewWebhookWithCert("https://xamilion.kz/api/v2/"+bot.Token, tgbotapi.FileBytes{
//		Name:  "cert.pem",
//		Bytes: fileByte,
//	})
//
//	_, err = bot.Request(wh)
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	info, err := bot.GetWebhookInfo()
//	if err != nil {
//		log.Fatal(err)
//	}
//	if info.LastErrorDate != 0 {
//		log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
//	}
//	updates := bot.ListenForWebhook("/" + bot.Token)
//	for update := range updates {
//		log.Printf("%+v\n", update)
//	}
//	bot
//}

func readConfigFile(directory, filename string) ([]byte, error) {
	cfgFile, fileErr := os.Open(directory + "/" + filename)

	if fileErr != nil {
		return nil, fileErr
	}
	return io.ReadAll(cfgFile)
}

func prepareConfigStruct(common []byte) (*config.Config, error) {
	cfg := config.NewConfig("processor", os.Getenv("SERVER_VERSION"))

	if unmFileErr := json.Unmarshal(common, cfg); unmFileErr != nil {
		return nil, unmFileErr
	}
	return cfg, nil
}

func StartServer(ctx context.Context, errCh chan error) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	//prefixPath := "conf"
	prefixPath := "/usr/local/go/auth/conf"
	commonCfg, commonCfgErr := readConfigFile(prefixPath, "config.json")

	if commonCfgErr != nil {
		errCh <- commonCfgErr
		return
	}
	cfg, cfgErr := prepareConfigStruct(commonCfg)

	if cfgErr != nil {
		errCh <- cfgErr
		return
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cLogger := logger.NewConsoleLogger(logger.INFO, logger.JSON)

	primaryDB, primaryErr := repository.InitDB("postgres", cfg.Database.PrimaryAddr)
	standbyDB, standbyErr := repository.InitDB("postgres", cfg.Database.StandbyAddr)

	if primaryErr != nil || standbyErr != nil {
		errCh <- fmt.Errorf("error database connection")
		return
	}
	cLogger.Info("PostgreSQL initialized")

	primaryNode := hasql.NewNode(cfg.Database.PrimaryAddr, primaryDB)
	standbyNode := hasql.NewNode(cfg.Database.StandbyAddr, standbyDB)

	cl, clErr := hasql.NewCluster(
		[]hasql.Node{primaryNode, standbyNode},
		checkers.PostgreSQL,
	)
	if clErr != nil {
		errCh <- clErr
		return
	}
	repo := repository.NewRepository(cl)

	defer func() {
		err := primaryDB.Close()
		err = standbyDB.Close()
		if err != nil {
			cLogger.WarnCtx(ctx, "Cannot close the resources "+err.Error())
		}
	}()

	excludeUrls := make(map[string]interface{})
	excludeUrls["/ready"] = nil
	excludeUrls["/live"] = nil
	excludeUrls["/metrics"] = nil

	lm := middleware.NewLoggerMiddleware(cLogger, middleware.WithExcludeUrls(excludeUrls))

	app := Init(cfg, ctx, repo, cLogger)
	app.Use(lm.HandleEchoLogger)

	p := prometheus.NewPrometheus("processor", nil)
	p.Use(app)
	c := jaegertracing.New(app, nil)

	cLogger.InfoCtx(ctx, "Houston, flight is normal")

	defer func(closer io.Closer) {
		if err := c.Close(); err != nil {
			cLogger.Warn("Cannot close resources")
		}
	}(c)
	errCh <- app.Start(cfg.Server.Addr)
}
