package service

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"processor/internal/model"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/robfig/cron/v3"
	"github.com/shopspring/decimal"
	"gitlab.com/danilpan/logger"

	//nolint:gosec
	"net"
	"net/http"
	"net/url"
	"processor/internal/config"
	"processor/internal/repository"
	"time"
)

type Service struct {
	cfg         *config.Config
	client      *http.Client
	repo        *repository.Repository
	logger      *logger.Logger
	proxyClient *http.Client
	crn         *cron.Cron
}

func NewService(cfg *config.Config, client *http.Client, repo *repository.Repository, logger *logger.Logger) *Service {
	proxyUrl, _ := url.Parse(cfg.Server.Proxy)
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:    100,
		IdleConnTimeout: 90 * time.Second,
		Proxy:           http.ProxyURL(proxyUrl),
	}
	proxyClient := &http.Client{
		Transport: transport,
	}
	return &Service{cfg: cfg, client: client, repo: repo, logger: logger, proxyClient: proxyClient, crn: cron.New()}
}

func (s Service) StartScheduler(ctx context.Context) error {
	if errUpdate := s.Scheduler(ctx); errUpdate != nil {
		return errUpdate
	}

	s.logger.Info("starting cron")
	go func() {
		s.crn.Start()
		s.logger.Info("cron started")
	}()

	return nil
}

func (s Service) StopScheduler() {
	s.logger.Info("stopping the scheduler")
	stopCtx := s.crn.Stop()
	<-stopCtx.Done()
	s.logger.Info("the scheduler stopped")
}

func (s Service) Scheduler(ctx context.Context) error {
	spec := fmt.Sprintf("* * * * *")
	bot, _ := tgbotapi.NewBotAPI("6260240803:AAFkOKtinjZT5IbBw7QYOaGLEu6Qks0ZMn0")
	_, errAddFunc := s.crn.AddFunc(spec, func() {
		_, cancel := context.WithTimeout(context.Background(), time.Duration(30)*time.Second)
		defer cancel()
		var orders []model.ReferenceUpdate
		movementInWork, errMovementInWork := s.MovementInWork()
		if errMovementInWork != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementInWork.Error()))
		}
		movementPurchased, errMovementPurchased := s.MovementPurchased()
		if errMovementPurchased != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementPurchased.Error()))
		}
		movementNotAvailable, errMovementNotAvailable := s.MovementNotAvailable()
		if errMovementNotAvailable != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementNotAvailable.Error()))
		}
		movementReadyToSend, errMovementReadyToSend := s.MovementReadyToSend()
		if errMovementReadyToSend != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementReadyToSend.Error()))
		}
		movementRecievedOnStock, errMovementRecievedOnStock := s.MovementRecievedOnStock()
		if errMovementRecievedOnStock != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementRecievedOnStock.Error()))
		}
		movementInSent, errMovementInSent := s.MovementInSent()
		if errMovementInSent != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementInSent.Error()))
		}
		movementInAgree, errMovementInAgree := s.MovementInAgree()
		if errMovementInAgree != nil {
			s.logger.Error(fmt.Sprintf("failed to update: %v", errMovementInAgree.Error()))
		}
		if len(movementInWork.Body.MovementInWorkResponse.MovementInWorkResult.Movement) > 0 {
			for _, m := range movementInWork.Body.MovementInWorkResponse.MovementInWorkResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		if len(movementPurchased.Body.MovementPurchasedResponse.MovementPurchasedResult.Movement) > 0 {
			for _, m := range movementPurchased.Body.MovementPurchasedResponse.MovementPurchasedResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		if len(movementNotAvailable.Body.MovementNotAvailableResponse.MovementNotAvailableResult.Movement) > 0 {
			for _, m := range movementNotAvailable.Body.MovementNotAvailableResponse.MovementNotAvailableResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		if len(movementReadyToSend.Body.MovementReadyToSendResponse.MovementReadyToSendResult.Movement) > 0 {
			for _, m := range movementReadyToSend.Body.MovementReadyToSendResponse.MovementReadyToSendResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		if len(movementRecievedOnStock.Body.MovementReceivedOnStockResponse.MovementReceivedOnStockResult.Movement) > 0 {
			for _, m := range movementRecievedOnStock.Body.MovementReceivedOnStockResponse.MovementReceivedOnStockResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		if len(movementInAgree.Body.MovementAGREEResponse.MovementAGREEResult.Movement) > 0 {
			for _, m := range movementInAgree.Body.MovementAGREEResponse.MovementAGREEResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		if len(movementInSent.Body.MovementSentResponse.MovementSentResult.Movement) > 0 {
			for _, m := range movementInSent.Body.MovementSentResponse.MovementSentResult.Movement {
				orders = append(orders, model.ReferenceUpdate{
					ReferenceEmex: m.Reference,
					Reference:     strings.Split(m.Reference, " ")[0],
					StatusEmex:    m.StatusId,
				})
			}
		}
		//TODO стянуть все заказы эмекс и смотреть изменение статуса
		fmt.Println(orders)
		allItems, errAI := s.repo.GetOrderItemsEmex(ctx)
		if errAI != nil {
			s.logger.Error(fmt.Sprintf("adding func update: %v", errAI))
		}
		for _, i := range allItems {
			for _, j := range orders {
				if i.Reference == j.Reference {
					if i.OldStatus == j.StatusEmex {
						j.IsChanged = false
					} else {
						j.IsChanged = true
						if j.StatusEmex == "6" {
						}
						bot.Send(tgbotapi.NewMessage(-1001705501387, fmt.Sprintf("Референс: %v. %v -> %v", j.Reference, i.OldStatus, j.StatusEmex)))
						fmt.Println(j.Reference)
					}
				}
			}
		}
		errU := s.repo.UpdateOrderItemsBatchStatus(ctx, orders)
		if errU != nil {
			s.logger.Error(fmt.Sprintf("adding func update: %v", errU))
		}

		armtekItems, errArm := s.repo.GetOrderItemsArmtek(ctx)
		if errArm != nil {
			s.logger.Error(fmt.Sprintf("getting armtek items: %v", errArm))
		}

		if len(armtekItems) > 0 {
			var armtekOrders []model.ReferenceUpdate
			var newOrder model.ReferenceUpdate
			for _, elem := range armtekItems {
				respArm, errArm := s.GetArmtekOrder(elem.Reference)
				if errArm != nil {
					s.logger.Error(fmt.Sprintf("getting armtek order: %s", errArm))
				}

				newOrder.Reference = respArm.RESP.HEADER.ORDER
				for _, item := range respArm.RESP.ITEMS {
					newOrder.Status = item.STATUS
				}

				armtekOrders = append(armtekOrders, newOrder)
			}

			var newStatuses []model.ReferenceUpdate
			for _, i := range armtekItems {
				for _, j := range armtekOrders {
					if i.Reference == j.Reference && i.OldStatus != j.Status {
						bot.Send(tgbotapi.NewMessage(-1001705501387, fmt.Sprintf("Референс: %v, %v -> %v", i.Reference, i.OldStatus, j.Status)))
						newStatuses = append(newStatuses, j)
					}
				}
			}

			errU := s.repo.UpdateArmtekOrderItemsBatchStatus(ctx, newStatuses)
			if errU != nil {
				s.logger.Error(fmt.Sprintf("adding func update: %v", errU))
			}
		}
	})
	if errAddFunc != nil {
		return fmt.Errorf("adding func update: %v", errAddFunc)
	}

	return nil
}

func (s Service) SearchEmex(ctx context.Context, partNum string, userId int, course decimal.Decimal) ([]model.Part, error) {
	//TODO переделать
	margins := model.Margin{}

	var rq model.EmexRq
	var rsSearch []model.Part
	var rs model.Envelope
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.SearchPartEx.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.SearchPartEx.Customer.CustomerId = "35968"
	rq.Body.SearchPartEx.Customer.SubCustomerId = "0"
	rq.Body.SearchPartEx.Customer.UserName = "QBNI"
	rq.Body.SearchPartEx.Customer.Password = "Azimyasaragil123"
	rq.Body.SearchPartEx.DetailNum = partNum
	rq.Body.SearchPartEx.ShowSubsets = "true"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rsSearch, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rsSearch, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rsSearch, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rsSearch, err
	}

	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rsSearch, err
	}
	if userId == 0 {
		margins = model.Margin{
			Emex:        decimal.NewFromInt(10),
			UserMargin:  decimal.NewFromInt(0),
			GroupMargin: decimal.NewFromInt(5),
		}
	} else {
		margins, _ = s.repo.GetUserMargins(userId)
	}
	arr, errGKWM := s.GetKeyWordMargins(ctx)
	if errGKWM != nil {
		return rsSearch, errGKWM
	}
	margin := decimal.Sum(margins.Emex, margins.UserMargin, margins.GroupMargin)
	if len(rs.Body.SearchPartExResponse.SearchPartExResult.FindByNumber) > 0 {
		for _, i := range rs.Body.SearchPartExResponse.SearchPartExResult.FindByNumber {
			var marginOveral decimal.Decimal
			//цена
			price, errNFS := decimal.NewFromString(i.Price)
			if errNFS != nil {
				continue
			}
			priceTg := price.Mul(course)
			del := decimal.NewFromFloat(6)
			fmt.Println(del)
			if len(arr) > 0 {
				for _, a := range arr {
					if strings.Contains(strings.ToLower(i.PartNameRus), strings.ToLower(a.Word)) {
						del = a.Margin
					}
					if strings.Contains(strings.ToLower(i.PartNameEng), strings.ToLower(a.Word)) {
						del = a.Margin
					}
				}
			}
			//Вес
			weight, errPF := strconv.ParseFloat(i.WeightGr, 64)
			if errPF != nil {
				return rsSearch, errPF
			}
			weightDec := decimal.NewFromFloat(weight)

			marginOveral = marginOveral.Add(margin)

			delivery, err := strconv.ParseInt(i.Delivery, 10, 64)
			if err != nil {
				return rsSearch, err
			}
			gDelivery, err := strconv.ParseInt(i.GuaranteedDay, 10, 64)
			if err != nil {
				return rsSearch, err
			}
			//todo пересчетать цену
			priceAvia := priceTg.Mul(marginOveral.Div(decimal.NewFromInt(100)).Add(decimal.NewFromInt(1)))
			priceAvia = priceAvia.Add(weightDec.Mul(del))
			if i.Available != "-1" && i.Available != "0" {
				rsSearch = append(rsSearch, model.Part{
					Id:                fmt.Sprintf("%v/%v/%v", i.DetailNum, i.PriceLogo, i.MakeName),
					Available:         i.Available,
					BitOldNum:         i.BitOldNum,
					PercentSupped:     i.PercentSupped,
					PriceID:           i.PriceID,
					Region:            "ae",
					Delivery:          fmt.Sprintf("%d", delivery+8),
					Delivery2:         fmt.Sprintf("%d", delivery+45),
					Make:              i.Make,
					DetailNum:         i.DetailNum,
					PriceLogo:         i.PriceLogo,
					Price:             priceAvia.Sub(weightDec.Mul(del)).RoundUp(2).StringFixed(0),
					Price2:            priceAvia.RoundUp(2).StringFixed(0),
					PartNameRus:       i.PartNameRus,
					PartNameEng:       i.PartNameEng,
					WeightGr:          weight,
					MakeName:          i.MakeName,
					Packing:           i.Packing,
					BitECO:            i.BitECO,
					BitWeightMeasured: i.BitWeightMeasured,
					VolumeAdd:         i.VolumeAdd,
					GuaranteedDay:     fmt.Sprintf("%v-%v", delivery+8, gDelivery+45),
					Origin:            "emex",
					Original:          fmt.Sprintf("%v|%v", price, course),
				})
			}

		}
	}
	return rsSearch, nil

}

func (s Service) SearchEmexRu(partNum string, userId int, course decimal.Decimal) ([]model.Part, error) {
	margins := model.Margin{}
	if userId == 0 {
		margins = model.Margin{
			EmexRu:      decimal.NewFromInt(15),
			UserMargin:  decimal.NewFromInt(15),
			GroupMargin: decimal.NewFromInt(15),
		}
	} else {
		margins, _ = s.repo.GetUserMargins(userId)
	}

	var rq model.EmexRuSearch
	var rsSearch []model.Part
	var rs model.SoapSearchResult
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Xsi = "http://www.w3.org/2001/XMLSchema-instance"
	rq.Xsd = "http://www.w3.org/2001/XMLSchema"
	rq.Body.FindDetailAdv4Test.Xmlns = "http://tempuri.org/"
	rq.Body.FindDetailAdv4Test.Login = "3286848"
	rq.Body.FindDetailAdv4Test.Password = "123456"
	rq.Body.FindDetailAdv4Test.DetailNum = partNum
	rq.Body.FindDetailAdv4Test.SubstLevel = "All"
	rq.Body.FindDetailAdv4Test.SubstFilter = "None"
	rq.Body.FindDetailAdv4Test.DeliveryRegionType = "PRI"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rsSearch, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, "http://ws.emex.ru/EmExService.asmx", bytes.NewReader(requestBody))
	if errReq != nil {
		return rsSearch, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	fmt.Println("request started")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rsSearch, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	//d := xml.NewDecoder(resp.Body)
	//d.Strict = false
	//d.Decode(&rs)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rsSearch, err
	}
	fmt.Println(string(body))
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rsSearch, err
	}

	margin := decimal.Sum(margins.UserMargin, margins.GroupMargin, margins.EmexRu)
	if len(rs.Body.FindDetailAdv4TestResponse.FindDetailAdv4TestResult.Details.SoapDetailItem) > 0 {
		for _, i := range rs.Body.FindDetailAdv4TestResponse.FindDetailAdv4TestResult.Details.SoapDetailItem {

			price, err := decimal.NewFromString(i.ResultPrice)
			if err != nil {
				return rsSearch, err
			}
			priceTg := price.Mul(course)
			marginDec := margin.Div(decimal.NewFromInt(100)).Add(decimal.NewFromInt(1))
			if i.Quantity != "-1" && i.Quantity != "0" {
				delivery, err := strconv.ParseInt(i.DeliverTimeGuaranteed, 10, 64)
				if err != nil {
					return rsSearch, err
				}
				rsSearch = append(rsSearch, model.Part{
					Id:                fmt.Sprintf("%v/%v/%v", i.DetailNum, i.PriceLogo, i.MakeName),
					Available:         i.Quantity,
					BitOldNum:         "",
					PercentSupped:     i.DDPercent,
					PriceID:           "",
					Region:            "ru",
					Delivery:          fmt.Sprintf("%d", delivery+14),
					Delivery2:         fmt.Sprintf("%d", delivery+14),
					Make:              i.MakeName,
					DetailNum:         i.DetailNum,
					PriceLogo:         i.PriceLogo,
					Price:             priceTg.Mul(marginDec).RoundUp(2).StringFixed(0),
					Price2:            priceTg.Mul(marginDec).RoundUp(2).StringFixed(0),
					PartNameRus:       i.DetailNameRus,
					PartNameEng:       "",
					WeightGr:          0,
					MakeName:          i.MakeName,
					Packing:           "",
					BitECO:            "",
					BitWeightMeasured: "",
					VolumeAdd:         "",
					GuaranteedDay:     fmt.Sprintf("%v-%v", delivery, delivery+14),
					Origin:            "emexru",
					Original:          fmt.Sprintf("%v|%v", price, course),
				})
			}

		}
	}
	return rsSearch, nil

}

func (s Service) AddEmexBasket(parts []model.Partstobasket) error {
	var rq model.EmexCartRq
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.SearchPartEx.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.SearchPartEx.Customer.CustomerId = "35968"
	rq.Body.SearchPartEx.Customer.SubCustomerId = "0"
	rq.Body.SearchPartEx.Customer.UserName = "QBNI"
	rq.Body.SearchPartEx.Customer.Password = "Azimyasaragil123"
	for _, part := range parts {
		rq.Body.SearchPartEx.Array.Partstobasket = append(rq.Body.SearchPartEx.Array.Partstobasket, part)
	}
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	//d := xml.NewDecoder(resp.Body)
	//d.Strict = false
	//d.Decode(&rs)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(string(body))

	if resp.StatusCode != 200 {
		return fmt.Errorf("Bad request")
	}
	return nil

}

func (s Service) AddEmexRuBasket(parts []model.PartRu) error {
	var rq model.PartstobasketRu
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Xsi = "http://www.w3.org/2001/XMLSchema-instance"
	rq.Xsd = "http://www.w3.org/2001/XMLSchema"
	rq.Body.InsertToBasket3.Xmlns = "http://tempuri.org/"
	rq.Body.InsertToBasket3.Login = "3286848"
	rq.Body.InsertToBasket3.Password = "123456"

	for _, part := range parts {
		rq.Body.InsertToBasket3.EPrices.EPrice = append(rq.Body.InsertToBasket3.EPrices.EPrice, part)
	}
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, "http://ws.emex.ru/EmEx_Basket.asmx", bytes.NewReader(requestBody))
	if errReq != nil {
		return errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	//d := xml.NewDecoder(resp.Body)
	//d.Strict = false
	//d.Decode(&rs)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(string(body))

	if resp.StatusCode != 200 {
		return fmt.Errorf("Bad request %v", resp.StatusCode)
	}
	return nil

}

func (s Service) AddRossko(parts []model.RosskoPart) error {
	var rq model.RosskoAddRq
	//<api:KEY1>d65eb025dcdaacdeb9e94c9b2535f866</api:KEY1>
	//<api:KEY2>1a16c82e7820dfe5a3b33d004f052b4c</api:KEY2>
	//<api:delivery>
	//<api:delivery_id>000000001</api:delivery_id>
	//</api:delivery>
	//<api:payment>
	//<api:payment_id>1</api:payment_id>
	//</api:payment>
	//<api:contact>
	//<api:name>Your name</api:name>
	//<api:phone>Your phone</api:phone>
	//</api:contact>
	//<api:delivery_parts>false</api:delivery_parts>
	rq.Soapenv = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Api = "http://api.rossko.ru/"
	rq.Body.GetCheckout.KEY1 = "d65eb025dcdaacdeb9e94c9b2535f866"
	rq.Body.GetCheckout.KEY2 = "1a16c82e7820dfe5a3b33d004f052b4c"
	rq.Body.GetCheckout.Delivery.DeliveryID = "000000001"
	rq.Body.GetCheckout.Payment.PaymentID = "1"
	rq.Body.GetCheckout.Contact.Phone = "Your name"
	rq.Body.GetCheckout.Contact.Name = "Your phone"
	rq.Body.GetCheckout.DeliveryParts = "false"
	for _, part := range parts {
		rq.Body.GetCheckout.PARTS.Part = append(rq.Body.GetCheckout.PARTS.Part, part)
	}
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, "http://api.rossko.ru/service/v2.1/GetCheckout", bytes.NewReader(requestBody))
	if errReq != nil {
		return errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	//d := xml.NewDecoder(resp.Body)
	//d.Strict = false
	//d.Decode(&rs)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(string(body))

	if resp.StatusCode != 200 {
		return fmt.Errorf("Bad request %v", resp.StatusCode)
	}
	return nil

}

func (s Service) AddArmtekOrder(parts []model.ArmtekItem) (model.ArmtekOrder, error) {
	var rq model.ArmtekOrderRq
	var rs model.ArmtekOrder
	rq.VKORG = "8000"
	rq.INCOTERMS = "1"
	rq.KUNRG = "43288073"
	rq.KUNNRZA = "48255348"
	rq.DBTYP = "3"
	rq.ITEMS = parts
	requestBody, errMar := json.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, "http://ws.armtek.kz/api/ws_order/createOrder?format=json", bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Authorization", "Basic YmVrYmF1b3ZfcnVmYXRAbWFpbC5ydTpBYTEyMzQ1Ng==")
	request.Header.Set("Content-Type", "application/json")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = json.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}

	if resp.StatusCode != 200 {
		return rs, fmt.Errorf("Bad request %v", resp.StatusCode)
	}
	return rs, nil

}

func (s Service) MovementInWork() (model.MovementInWorkRs, error) {
	var rq model.MovementInWork
	var rs model.MovementInWorkRs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementInWork.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementInWork.Customer.CustomerId = "35968"
	rq.Body.MovementInWork.Customer.SubCustomerId = "0"
	rq.Body.MovementInWork.Customer.UserName = "QBNI"
	rq.Body.MovementInWork.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}
func (s Service) MovementPurchased() (model.MovementPurchasedRs, error) {
	var rq model.MovementPurchased
	var rs model.MovementPurchasedRs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementPurchased.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementPurchased.Customer.CustomerId = "35968"
	rq.Body.MovementPurchased.Customer.SubCustomerId = "0"
	rq.Body.MovementPurchased.Customer.UserName = "QBNI"
	rq.Body.MovementPurchased.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}
func (s Service) MovementNotAvailable() (model.MovementNotAvailableRs, error) {
	var rq model.MovementNotAvailable
	var rs model.MovementNotAvailableRs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementNotAvailable.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementNotAvailable.Customer.CustomerId = "35968"
	rq.Body.MovementNotAvailable.Customer.SubCustomerId = "0"
	rq.Body.MovementNotAvailable.Customer.UserName = "QBNI"
	rq.Body.MovementNotAvailable.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}
func (s Service) MovementReadyToSend() (model.MovementReadyToSendRs, error) {
	var rq model.MovementReadyToSend
	var rs model.MovementReadyToSendRs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementReadyToSend.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementReadyToSend.Customer.CustomerId = "35968"
	rq.Body.MovementReadyToSend.Customer.SubCustomerId = "0"
	rq.Body.MovementReadyToSend.Customer.UserName = "QBNI"
	rq.Body.MovementReadyToSend.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}
func (s Service) MovementRecievedOnStock() (model.MovementReceivedOnStockRs, error) {
	var rq model.MovementReceivedOnStock
	var rs model.MovementReceivedOnStockRs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementReceivedOnStock.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementReceivedOnStock.Customer.CustomerId = "35968"
	rq.Body.MovementReceivedOnStock.Customer.SubCustomerId = "0"
	rq.Body.MovementReceivedOnStock.Customer.UserName = "QBNI"
	rq.Body.MovementReceivedOnStock.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}
func (s Service) MovementInSent() (model.MovementSentRs, error) {
	var rq model.MovementSent
	var rs model.MovementSentRs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementSent.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementSent.Customer.CustomerId = "35968"
	rq.Body.MovementSent.Customer.SubCustomerId = "0"
	rq.Body.MovementSent.Customer.UserName = "QBNI"
	rq.Body.MovementSent.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}
func (s Service) MovementInAgree() (model.MovementAGREERs, error) {
	var rq model.MovementAGREE
	var rs model.MovementAGREERs
	rq.Soap = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Body.MovementAGREE.Xmlns = "http://emexonline.com:3000/MaximaWS/"
	rq.Body.MovementAGREE.Customer.CustomerId = "35968"
	rq.Body.MovementAGREE.Customer.SubCustomerId = "0"
	rq.Body.MovementAGREE.Customer.UserName = "QBNI"
	rq.Body.MovementAGREE.Customer.Password = "Azimyasaragil123"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rs, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, s.cfg.Search.Emex, bytes.NewReader(requestBody))
	if errReq != nil {
		return rs, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}
	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}
	return rs, nil
}

func (s Service) SearchRossko(ctx context.Context, partNum string, userId int, course decimal.Decimal) ([]model.Part, error) {
	//TODO переделать
	margins := model.Margin{}

	var rq model.RosskoRq
	var rsSearch []model.Part
	var rs model.RosskoRs
	rq.Soapenv = "http://schemas.xmlsoap.org/soap/envelope/"
	rq.Api = "http://api.rossko.ru/"
	rq.Body.GetSearch.KEY1 = "d65eb025dcdaacdeb9e94c9b2535f866"
	rq.Body.GetSearch.KEY2 = "1a16c82e7820dfe5a3b33d004f052b4c"
	rq.Body.GetSearch.Text = partNum
	rq.Body.GetSearch.DeliveryID = "000000001"
	requestBody, errMar := xml.Marshal(rq)
	if errMar != nil {
		return rsSearch, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, "http://api.rossko.ru/service/v2.1/GetSearch", bytes.NewReader(requestBody))
	if errReq != nil {
		return rsSearch, errReq
	}
	request.Header.Set("Content-Type", "text/xml")
	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rsSearch, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rsSearch, err
	}

	err = xml.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rsSearch, err
	}

	if userId == 0 {
		margins = model.Margin{
			Emex:        decimal.NewFromInt(10),
			UserMargin:  decimal.NewFromInt(0),
			GroupMargin: decimal.NewFromInt(5),
		}
	} else {
		margins, _ = s.repo.GetUserMargins(userId)
	}

	margin := decimal.Sum(margins.Local, margins.UserMargin, margins.GroupMargin)
	if len(rs.Body.GetSearchResponse.SearchResult.PartsList.Part) > 0 {
		for ii, i := range rs.Body.GetSearchResponse.SearchResult.PartsList.Part {
			if len(i.Crosses.Part) > 0 {
				for ji, j := range i.Crosses.Part {
					if len(j.Stocks.Stock) > 0 {
						for _, k := range j.Stocks.Stock {
							delivery, err := strconv.ParseInt(k.Delivery, 10, 64)
							if err != nil {
								return rsSearch, err
							}
							priceTg, _ := decimal.NewFromString(k.Price)
							rsSearch = append(rsSearch, model.Part{
								Id:                fmt.Sprintf("%v/%v/%v", i.Crosses.Part[ji].Partnumber, k.ID, i.Crosses.Part[ji].Brand),
								Available:         k.Count,
								BitOldNum:         "",
								PercentSupped:     "",
								PriceID:           "",
								Region:            "kz",
								Delivery:          fmt.Sprintf("%v", k.Delivery),
								Delivery2:         fmt.Sprintf("%v", k.Delivery),
								Make:              i.Crosses.Part[ji].Brand,
								DetailNum:         i.Crosses.Part[ji].Partnumber,
								PriceLogo:         fmt.Sprintf("%v", k.ID),
								Price:             priceTg.Mul(margin).RoundUp(2).StringFixed(0),
								Price2:            priceTg.Mul(margin).RoundUp(2).StringFixed(0),
								PartNameRus:       i.Name,
								PartNameEng:       i.Name,
								WeightGr:          1,
								MakeName:          i.Crosses.Part[ji].Brand,
								Packing:           k.Multiplicity,
								BitECO:            "",
								BitWeightMeasured: "",
								VolumeAdd:         "",
								GuaranteedDay:     fmt.Sprintf("%v-%v", delivery, delivery+7),
								Origin:            "rossko",
								Original:          fmt.Sprintf("%v|%v", k.Price, course),
							})
						}
					}
				}
			}
			if len(i.Stocks.Stock) > 0 {
				for _, j := range i.Stocks.Stock {
					priceTg, _ := decimal.NewFromString(j.Price)
					delivery, err := strconv.ParseInt(j.Delivery, 10, 64)
					if err != nil {
						return rsSearch, err
					}
					rsSearch = append(rsSearch, model.Part{
						Id:                fmt.Sprintf("%v/%v/%v", rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Partnumber, j.ID, rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Brand),
						Available:         j.Count,
						BitOldNum:         "",
						PercentSupped:     "",
						PriceID:           "",
						Region:            "kz",
						Delivery:          fmt.Sprintf("%v", j.Delivery),
						Delivery2:         fmt.Sprintf("%v", j.Delivery),
						Make:              rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Brand,
						DetailNum:         rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Partnumber,
						PriceLogo:         fmt.Sprintf("%v", j.ID),
						Price:             priceTg.Mul(margin).String(),
						Price2:            priceTg.Mul(margin).String(),
						PartNameRus:       i.Name,
						PartNameEng:       i.Name,
						WeightGr:          1,
						MakeName:          rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Brand,
						Packing:           j.Multiplicity,
						BitECO:            "",
						BitWeightMeasured: "",
						VolumeAdd:         "",
						GuaranteedDay:     fmt.Sprintf("%v-%v", delivery, delivery+7),
						Origin:            "rossko",
						Original:          fmt.Sprintf("%v|%v", j.Price, course),
					})
					fmt.Println(fmt.Sprintf("%v %v %v", rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Partnumber, rs.Body.GetSearchResponse.SearchResult.PartsList.Part[ii].Name, j))
				}
			}
		}
	}
	return rsSearch, nil

}
func (s Service) SearchArmtek(ctx context.Context, partNum string, userId int, course decimal.Decimal) ([]model.Part, error) {
	//TODO переделать
	//margins := model.Margin{}

	var rq model.ArmtekRq
	var rsSearch []model.Part
	var rs model.ArmtekRs
	rq.PIN = partNum
	rq.VKORG = "8000"
	rq.KUNNRRG = "43288073"
	rq.KUNNRZA = "48255348"
	requestBody, errMar := json.Marshal(rq)
	if errMar != nil {
		return rsSearch, errMar
	}
	request, errReq := http.NewRequest(http.MethodPost, "http://ws.armtek.kz/api/ws_search/search?format=json", bytes.NewReader(requestBody))
	if errReq != nil {
		return rsSearch, errReq
	}
	request.Header.Set("Authorization", "Basic YmVrYmF1b3ZfcnVmYXRAbWFpbC5ydTpBYTEyMzQ1Ng==")
	request.Header.Set("Content-Type", "application/json")

	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rsSearch, respErr
	}
	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rsSearch, err
	}

	err = json.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rsSearch, err
	}
	if len(rs.Resp) > 0 {
		for _, i := range rs.Resp {
			price, errNFS := decimal.NewFromString(i.Price)
			if errNFS != nil {
				continue
			}
			var marginOveral decimal.Decimal
			//цена

			priceTg := price.Mul(course)

			//Вес
			//weight, errPF := strconv.ParseFloat(i., 64)
			//if errPF != nil {
			//	return rsSearch, errPF
			//}
			//weightDec := decimal.NewFromFloat(weight)

			//Объемный вес
			//volumeAdd, errPF := strconv.ParseFloat(i.VolumeAdd, 64)
			//if errPF != nil {
			//	return rsSearch, errPF
			//}

			//volumeAddDec := decimal.NewFromFloat(volumeAdd)
			//if volumeAddDec.GreaterThan(decimal.Zero) {
			//	volumeMargin, errGVMBV := s.GetVolumeMarginByVolume(ctx, volumeAddDec)
			//	if errGVMBV != nil {
			//		return rsSearch, errGVMBV
			//	}
			//	marginOveral.Add(volumeMargin)
			//}
			//marginOveral.Add(margin)
			//YYYYMMDDHHIISS
			myDate, err := time.Parse("20060102150405", i.Dlvdt)
			if err != nil {
				panic(err)
			}

			delivery := math.Round(myDate.Sub(time.Now()).Hours() / 24)

			//todo пересчетать
			priceAvia := priceTg.Mul(marginOveral.Div(decimal.NewFromInt(100)).Add(decimal.NewFromInt(1)))
			//priceAvia.Add(weightDec.Mul(decimal.NewFromInt(6)))
			if i.Rvalue != "-1" && i.Rvalue != "0" {
				rsSearch = append(rsSearch, model.Part{
					Id:                fmt.Sprintf("%v/%v/%v", i.Pin, i.Keyzak, i.Brand),
					Available:         i.Rvalue,
					BitOldNum:         "",
					PercentSupped:     i.Vensl,
					PriceID:           "",
					Region:            "kz",
					Delivery:          fmt.Sprintf("%v", delivery),
					Delivery2:         fmt.Sprintf("%v", delivery),
					Make:              i.Brand,
					DetailNum:         i.Pin,
					PriceLogo:         fmt.Sprintf("%v", i.Keyzak),
					Price:             priceAvia.RoundUp(2).StringFixed(0),
					Price2:            priceAvia.RoundUp(2).StringFixed(0),
					PartNameRus:       i.Name,
					PartNameEng:       i.Name,
					WeightGr:          1,
					MakeName:          i.Brand,
					Packing:           "",
					BitECO:            "",
					BitWeightMeasured: "",
					VolumeAdd:         "",
					GuaranteedDay:     fmt.Sprintf("%v-%v", delivery, delivery+7),
					Origin:            "arm",
					Original:          fmt.Sprintf("%v|%v", price, course),
				})
			}

		}
	}

	return rsSearch, nil

}

func (s Service) GetArmtekOrder(order string) (model.ArmtekGetOrderRs, error) {
	var rs model.ArmtekGetOrderRs

	request, errReq := http.NewRequest(http.MethodGet, fmt.Sprintf("http://ws.armtek.kz/api/ws_order/getOrder?VKORG=8000&KUNRG=43288073&ORDER=%s&format=json", order), nil)
	if errReq != nil {
		return rs, errReq
	}

	request.Header.Set("Authorization", "Basic YmVrYmF1b3ZfcnVmYXRAbWFpbC5ydTpBYTEyMzQ1Ng==")
	request.Header.Set("Content-Type", "application/json")

	resp, respErr := s.client.Do(request)
	if respErr != nil {
		return rs, respErr
	}

	defer func(Body io.ReadCloser) error {
		errClosing := Body.Close()
		if errClosing != nil {
			return errClosing
		}
		return nil
	}(resp.Body)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return rs, err
	}

	err = json.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println(err.Error())
		return rs, err
	}

	if resp.StatusCode != http.StatusOK {
		return rs, fmt.Errorf("Bad request: %v", resp.StatusCode)
	}

	return rs, nil
}
