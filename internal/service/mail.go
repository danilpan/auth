package service

import (
	"context"
	"fmt"
	"net/smtp"
	"processor/internal/model"
	"strings"
)

func (s Service) SendMail(ctx context.Context, user *model.User, verificationCode string) error {
	sender := "mailer@xamilion.kz"
	to := []string{
		user.Email,
	}

	login := "sendbox@xamilion.kz"
	password := "tp2bXHBPjZeD"
	link := fmt.Sprintf("https://xamilion.kz/api/v2/user/verification?code=%s&uid=%d", verificationCode, user.ID)

	subject := "Подтверждение регистрации на xamilion.kz"
	body := fmt.Sprintf(`Уважаемый(-ая) %s,<br>
				Пожалуйста, подтвердите свой адрес электронной почты для активации вашей регистрации на xamilion.kz<br>
				Для этого нажмите на следующую ссылку:<br>
				%s <br><br>
				С уважением, xamilion.kz`, fmt.Sprintf("%s %s", user.FirstName, user.LastName), link)

	request := model.Mail{
		Sender:  sender,
		To:      to,
		Subject: subject,
		Body:    body,
	}

	addr := "smtp.send-box.ru:2525"
	msg := BuildMessage(request)
	auth := smtp.PlainAuth("", login, password, "smtp.send-box.ru")
	err := smtp.SendMail(addr, auth, sender, to, []byte(msg))
	if err != nil {
		s.logger.WarnCtx(ctx, "email error: "+err.Error())
		return err
	}

	s.logger.InfoCtx(ctx, "Email sent successfully")
	return nil
}

func BuildMessage(mail model.Mail) string {
	msg := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\r\n"
	msg += fmt.Sprintf("From: %s\r\n", mail.Sender)
	msg += fmt.Sprintf("To: %s\r\n", strings.Join(mail.To, ";"))
	msg += fmt.Sprintf("Subject: %s\r\n", mail.Subject)
	msg += fmt.Sprintf("\r\n%s\r\n", mail.Body)

	return msg
}
