package service

import "processor/internal/model"

func (s Service) GetUserTransactions(userID int) (*[]model.Transaction, error) {
	data, dataErr := s.repo.GetUserTransactions(userID)
	if dataErr != nil {
		s.logger.Warn(dataErr.Error())
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetUserTransactionsExcel(userID int, from, to string) (*[]model.Transaction, error) {
	data, dataErr := s.repo.GetUserTransactionsExcel(userID, from, to)
	if dataErr != nil {
		s.logger.Warn(dataErr.Error())
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetManagerTransactions(userID int) (*[]model.Transaction, error) {
	data, dataErr := s.repo.GetManagerTransactions(userID)
	if dataErr != nil {
		s.logger.Warn(dataErr.Error())
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetAllTransactions() (*[]model.Transaction, error) {
	data, dataErr := s.repo.GetAllTransactions()
	if dataErr != nil {
		s.logger.Warn(dataErr.Error())
		return nil, dataErr
	}
	return data, nil
}
