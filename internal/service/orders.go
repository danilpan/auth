package service

import (
	"context"
	"github.com/shopspring/decimal"
	"processor/internal/model"
	"strconv"
	"strings"
)

func (ps Service) ConfirmToOrder(ctx context.Context, userID, managerID int, data *[]model.ConfirmOrderRequest) (err error) {
	var amount decimal.Decimal
	var ois []model.OrderItem

	//user, userErr := ps.GetUserByID(ctx, userID)
	//if userErr != nil {
	//	ps.logger.WarnCtx(ctx, "user error: "+userErr.Error())
	//	return userErr
	//}

	counter := 1
	for _, v := range *data {
		c, errGCI := ps.repo.GetCartItem(v.ItemID)
		if errGCI != nil {
			ps.logger.Warn(errGCI.Error())
			return errGCI
		}
		if c == nil {
			continue
		}
		stock, parseErr := strconv.Atoi(c.Availability)
		if parseErr != nil {
			ps.logger.WarnCtx(ctx, "parse error: "+parseErr.Error())
		}
		arr := strings.Split(c.Original, "|")
		rate, _ := decimal.NewFromString(arr[1])
		originalPrice, _ := decimal.NewFromString(arr[0])
		oi := &model.OrderItem{
			PartNumber:       c.PartNumber,
			Brand:            c.Brand,
			WeightKg:         c.Weight,
			VolumeKg:         c.VolumeAdd,
			Description:      "",
			Booking:          c.Booking,
			Available:        c.Availability,
			Quantity:         c.Quantity,
			Price:            c.Price,
			Currency:         c.Currency,
			Days:             0,
			DeliveryCost:     0,
			Delivery:         c.Delivery,
			Destination:      c.Destination,
			Transport:        c.Transport,
			Tariff:           "",
			Reference:        strconv.Itoa(c.ID),
			Excess:           "принят",
			Catalog:          "",
			Stock:            stock,
			Comment:          c.Comment,
			StockName:        &c.PriceLogo,
			RejectedItemsNum: 0,
			PartName:         c.PartNameRus,
			MakeName:         c.MakeName,
			Rate:             rate,
			OriginalPrice:    originalPrice,
		}
		price, _ := decimal.NewFromString(c.Price)
		amount = price.Mul(decimal.NewFromInt(int64(c.Quantity))).Add(amount)
		ois = append(ois, *oi)
		counter++
	}

	order := model.Order{
		UserID:    userID,
		StatusID:  0,
		IsPaid:    true,
		ManagerID: managerID,
		Amount:    amount,
	}
	id, err := ps.repo.CreateOrder(order)
	if err != nil {
		return err
	}

	for _, v := range ois {
		v.OrderID = id
		err = ps.repo.CreateOrderItem(&v)
		if err != nil {
			return err
		}
	}

	_, woErr := ps.WriteOffBalance(ctx, userID, amount, 0)
	if woErr != nil {
		ps.logger.WarnCtx(ctx, "write off error: "+woErr.Error())
		return err
	}

	order.IsPaid = true
	order.StatusID = 2
	uoErr := ps.repo.UpdateOrder(order)
	if uoErr != nil {
		ps.logger.WarnCtx(ctx, "order update error: "+uoErr.Error())
		return err
	}

	return nil
}

func (s Service) GetOrdersByUserID(userID int) (*[]model.Order, error) {
	data, dataErr := s.repo.GetOrdersByUserID(userID)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrders(userID, roleID int) (*[]model.Order, error) {
	data, dataErr := s.repo.GetOrders(userID, roleID)
	if dataErr != nil {
		s.logger.Warn("get orders error" + dataErr.Error())
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrderItem(order int) (*model.OrderItem, error) {
	data, dataErr := s.repo.GetOrderItemByID(order)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrderItemsAll(userID int, ctx context.Context) ([]model.OrderItems, error) {
	data, dataErr := s.repo.GetOrderItemsAll(userID, ctx)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrderItemsAllV2(userID int, partNumber, reference, partName, brand, manager, client, supplier, status string, ctx context.Context) ([]model.OrderItemsV2, error) {
	data, dataErr := s.repo.GetOrderItemsAllAdmin(userID, partNumber, reference, partName, brand, manager, client, supplier, status, ctx)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrderItemsAllTable(userID int, ctx context.Context, partNumber, reference, partName, brand, status string) ([]model.OrderItemsTable, error) {
	data, dataErr := s.repo.GetOrderItemsAllTable(userID, ctx, partNumber, reference, partName, brand, status)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	for i, _ := range data {
		data[i].Invoice = "59"
	}
	return data, nil
}

func (s Service) GetOrderItems(userID int, partNumber, reference, partName, brand, manager, client, supplier, status string, ctx context.Context) ([]model.OrderItemsV2, error) {
	data, dataErr := s.repo.GetOrderItemsByManagerId(userID, partNumber, reference, partName, brand, manager, client, supplier, status, ctx)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrderByID(orderID, userID, roleID int) (*model.Order, error) {
	data, dataErr := s.repo.GetOrderByID(orderID, userID, roleID)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetOrderItemsByID(orderID, userID int) ([]model.OrderItems, error) {
	data, dataErr := s.repo.GetOrderItemsByID(orderID, userID)
	if dataErr != nil {
		s.logger.Warn("get orders error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetUsersActiveCarts() (*[]model.UserCart, error) {
	users, usersErr := s.repo.GetUsersWithCarts()
	if usersErr != nil {
		s.logger.Warn(usersErr.Error())
		return nil, usersErr
	}
	carts, cartsErr := s.repo.GetUsersCarts(users)
	if cartsErr != nil {
		s.logger.Warn(cartsErr.Error())
		return nil, usersErr
	}

	return carts, nil
}

func (s Service) GetUsersActiveCartsTable() (*[]model.UserCart, error) {
	users, usersErr := s.repo.GetUsersWithCarts()
	if usersErr != nil {
		s.logger.Warn(usersErr.Error())
		return nil, usersErr
	}
	carts, cartsErr := s.repo.GetUsersCartsWithoutItems(users)
	if cartsErr != nil {
		s.logger.Warn(cartsErr.Error())
		return nil, usersErr
	}

	return carts, nil
}

func (s Service) GetUserActiveCarts(userID int) (*model.UserCart, error) {
	user, usersErr := s.repo.GetUserWithCarts(userID)
	if usersErr != nil {
		s.logger.Warn(usersErr.Error())
		return nil, usersErr
	}
	cart, cartsErr := s.repo.GetUserCarts(user)
	if cartsErr != nil {
		s.logger.Warn(cartsErr.Error())
		return nil, usersErr
	}

	return cart, nil
}

func (s Service) GetUserActiveCartItems(userID int) (*[]model.CartItem, error) {
	user, usersErr := s.repo.GetUserWithCarts(userID)
	if usersErr != nil {
		s.logger.Warn(usersErr.Error())
		return nil, usersErr
	}
	cart, cartsErr := s.repo.GetUserCartItemsTable(user)
	if cartsErr != nil {
		s.logger.Warn(cartsErr.Error())
		return nil, usersErr
	}

	return cart, nil
}
