package service

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/shopspring/decimal"
	"net/http"
	"strings"
)

func (s Service) GetCourse() (decimal.Decimal, error) {

	res, err := http.Get("https://www.mig.kz")
	if err != nil {
		s.logger.Error(err)
		return decimal.Decimal{}, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		s.logger.Error(fmt.Sprintf("status code error: %d %s", res.StatusCode, res.Status))
		return decimal.Decimal{}, fmt.Errorf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		s.logger.Error(err)
		return decimal.Decimal{}, err
	}
	var rate decimal.Decimal
	// Find the review items
	doc.Find("tr").Each(func(i int, s *goquery.Selection) {

		title := s.Find("td").Text()
		if strings.Contains(title, "USD") {
			splited := strings.Split(title, "USD")
			if len(splited) == 2 {
				fmt.Println(splited[1])
				rate, _ = decimal.NewFromString(splited[1])
			}
		}

	})
	return rate, nil
}
