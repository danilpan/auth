package service

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"processor/internal/model"
	"time"
)

var jwtKey = []byte("Jv#/TWw3")

type JWTClaim struct {
	UserID   int    `json:"user_id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	RoleID   int    `json:"role_id"`
	GroupID  *int   `json:"group_id"`
	jwt.StandardClaims
}

func GenerateJWT(user *model.User) (tokenString string, err error) {
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &JWTClaim{
		UserID:   user.ID,
		Email:    user.Email,
		Username: user.Username,
		RoleID:   user.RoleID,
		GroupID:  user.GroupID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err = token.SignedString(jwtKey)
	return
}

func ValidateToken(signedToken string) (claims *JWTClaim, err error) {
	token, err := jwt.ParseWithClaims(
		signedToken,
		&JWTClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(jwtKey), nil
		},
	)
	if err != nil {
		return claims, err
	}
	claims, ok := token.Claims.(*JWTClaim)
	if !ok {
		return claims, errors.New("couldn't parse claims")
	}
	if claims.ExpiresAt < time.Now().Local().Unix() {
		fmt.Println(fmt.Sprintf("%s validated", claims.Username))
		return claims, errors.New("token expired")
	}

	return claims, nil
}
