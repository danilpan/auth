package service

import (
	"context"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"processor/internal/model"
)

func (s Service) GetVolumeMargins(ctx context.Context) ([]model.Volume, error) {
	rs, dataErr := s.repo.GetVolumeMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	return *rs, nil
}

func (s Service) AddVolumeMargins(ctx context.Context, rq []model.Volume) ([]model.Volume, error) {
	if len(rq) > 0 {
		for i, _ := range rq {
			rq[i].Id = uuid.NewString()
		}
	}
	dataErr := s.repo.AddVolumeMargins(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	rs, dataErr := s.repo.GetVolumeMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	return *rs, nil
}

func (s Service) PatchVolumeMargin(ctx context.Context, rq model.Volume) ([]model.Volume, error) {
	dataErr := s.repo.PatchVolumeMargin(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	rs, dataErr := s.repo.GetVolumeMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	return *rs, nil
}

func (s Service) DeleteVolumeMargin(ctx context.Context, rq model.Volume) ([]model.Volume, error) {
	dataErr := s.repo.DeleteVolumeMargins(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	rs, dataErr := s.repo.GetVolumeMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Volume{}, dataErr
	}
	return *rs, nil
}

func (s Service) GetKeyWordMargins(ctx context.Context) ([]model.KeyWord, error) {
	rs, dataErr := s.repo.GetKeyWordMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	return *rs, nil
}

func (s Service) AddKeyWordMargins(ctx context.Context, rq []model.KeyWord) ([]model.KeyWord, error) {
	if len(rq) > 0 {
		for i, _ := range rq {
			rq[i].Id = uuid.NewString()
		}
	}
	dataErr := s.repo.AddKeyWordMargins(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	rs, dataErr := s.repo.GetKeyWordMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	return *rs, nil
}

func (s Service) PatchKeyWordMargin(ctx context.Context, rq model.KeyWord) ([]model.KeyWord, error) {
	dataErr := s.repo.PatchKeyWordMargin(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	rs, dataErr := s.repo.GetKeyWordMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	return *rs, nil
}

func (s Service) DeleteKeyWordMargin(ctx context.Context, rq model.KeyWord) ([]model.KeyWord, error) {
	dataErr := s.repo.DeleteKeyWordMargins(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	rs, dataErr := s.repo.GetKeyWordMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.KeyWord{}, dataErr
	}
	return *rs, nil
}

//group

func (s Service) GetGroupMargins(ctx context.Context) ([]model.MarginTable, error) {
	rs, dataErr := s.repo.GetGroupMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	return *rs, nil
}

func (s Service) AddGroupMargins(ctx context.Context, rq []model.MarginTable) ([]model.MarginTable, error) {

	dataErr := s.repo.AddGroupMargins(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	rs, dataErr := s.repo.GetGroupMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	return *rs, nil
}

func (s Service) PatchGroupMargin(ctx context.Context, rq model.MarginTable) ([]model.MarginTable, error) {
	dataErr := s.repo.PatchGroupMargin(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	rs, dataErr := s.repo.GetGroupMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	return *rs, nil
}

func (s Service) DeleteGroupMargin(ctx context.Context, rq model.MarginTable) ([]model.MarginTable, error) {
	dataErr := s.repo.DeleteGroupMargins(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	rs, dataErr := s.repo.GetGroupMargins(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.MarginTable{}, dataErr
	}
	return *rs, nil
}

func (s Service) GetVolumeMarginByVolume(ctx context.Context, volume decimal.Decimal) (decimal.Decimal, error) {
	rs, dataErr := s.repo.GetVolumeMarginByVolume(ctx, volume)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return decimal.Decimal{}, dataErr
	}
	return rs, nil
}

func (s Service) GetCurrencies(ctx context.Context) ([]model.Currency, error) {
	rs, dataErr := s.repo.GetCurrencies(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	return *rs, nil
}

func (s Service) AddCurrency(ctx context.Context, rq []model.Currency) ([]model.Currency, error) {

	dataErr := s.repo.AddCurrency(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	rs, dataErr := s.repo.GetCurrencies(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	return *rs, nil
}

func (s Service) PatchCurrency(ctx context.Context, rq model.Currency) ([]model.Currency, error) {
	dataErr := s.repo.PatchCurrency(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	rs, dataErr := s.repo.GetCurrencies(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	return *rs, nil
}

func (s Service) DeleteCurrency(ctx context.Context, rq model.Currency) ([]model.Currency, error) {
	dataErr := s.repo.DeleteCurrency(ctx, rq)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	rs, dataErr := s.repo.GetCurrencies(ctx)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.Currency{}, dataErr
	}
	return *rs, nil
}
