package service

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"golang.org/x/crypto/bcrypt"
	"processor/internal/model"
)

func (s Service) GetUsersList() (*[]model.User, error) {
	data, err := s.repo.GetUsersList()
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) GetCustomersList(managerId int64) (*[]model.User, error) {
	data, err := s.repo.GetCustomersList(managerId)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) GetManagerCustomersList(managerId int) (*[]model.User, error) {
	data, err := s.repo.GetManagerCustomersList(managerId)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) GetUserByID(ctx context.Context, userID int) (*model.User, error) {
	data, err := s.repo.GetUserByID(ctx, userID)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) GetProfileByID(ctx context.Context, userID int) (*model.UserProfile, error) {
	data, err := s.repo.GetProfileByID(ctx, userID)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) UpdateUserInfoByID(ctx context.Context, userID int, userInfo model.UpdateUserProfileRequest) (*model.User, error) {
	err := s.repo.UpdateUserInfoByID(ctx, userID, userInfo)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	data, err := s.repo.GetUserByID(ctx, userID)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) UpdateUserByID(ctx context.Context, userID int, userRq model.UpdateUserRequest) (*model.User, error) {
	err := s.repo.UpdateUserByID(ctx, userID, userRq)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	data, err := s.repo.GetUserByID(ctx, userID)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) UpdateUserPasswordByID(ctx context.Context, userID int, userRq model.UpdatePasswordRequest) (*model.User, error) {
	u, repErr := s.repo.GetUserByID(ctx, userID)
	if repErr != nil {
		s.logger.Warn(repErr.Error())
		return u, repErr
	}

	hashedPassword, hashErr := bcrypt.GenerateFromPassword([]byte(userRq.NewPassword), 10)
	if hashErr != nil {
		s.logger.Warn("hash error: " + hashErr.Error())
		return nil, hashErr
	}

	err := s.repo.UpdateUserPassword(ctx, userID, string(hashedPassword))
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	data, err := s.repo.GetUserByID(ctx, userID)
	if err != nil {
		s.logger.Warn("service error: " + err.Error())
		return nil, err
	}
	return data, err
}

func (s Service) ReplenishBalance(ctx context.Context, userID int, sum decimal.Decimal, managerID int, recieved bool) (user *model.User, err error) {
	transId, err := s.repo.BalanceReplenishment(ctx, userID, sum, managerID)
	if err != nil {
		s.logger.Warn(err.Error())
		return nil, err
	}
	if recieved {
		s.repo.ManagerWriteOff(ctx, userID, sum, managerID, transId, "Пополнение пользователю", "Пополнение пользователю")
		s.repo.ManagerReplenishment(ctx, userID, sum, managerID, transId, "Получено от пользователя", "Получено от пользователя")
	} else {
		s.repo.ManagerWriteOff(ctx, userID, sum, managerID, transId, "Пополнение пользователю", "Пополнение пользователю")
	}
	user, err = s.repo.GetUserByID(ctx, userID)
	if err != nil {
		s.logger.Warn(err.Error())
		return nil, err
	}
	return user, nil
}

func (s Service) Report(ctx context.Context, managerID int, username, since, to string) (report model.FinancialReport, err error) {
	finance, err := s.repo.Report(ctx, managerID, username, since, to)
	if err != nil {
		s.logger.Warn(err.Error())
		return report, err
	}
	var balance float64
	for _, f := range finance {
		balance += f.Income
		balance -= f.Outcome
	}
	report.Finance = finance
	report.Balance = balance
	return report, nil
}

func (s Service) CreateTransaction(ctx context.Context, rq model.Finance, managerID int, username, since, to string) (report model.FinancialReport, err error) {
	outcome := decimal.NewFromFloat(rq.Outcome)
	if rq.TransactionType == "Передал в кассу" {
		err := s.repo.ManagerWriteOff(ctx, 19, outcome, managerID, 0, rq.Comment, rq.TransactionType)
		if err != nil {
			s.logger.Warn(err.Error())
			return report, err
		}
		s.repo.ManagerReplenishment(ctx, 0, outcome, 19, 0, "Передал в кассу", "Передал в кассу")
	} else {
		err := s.repo.ManagerWriteOff(ctx, 0, outcome, managerID, 0, rq.Comment, rq.TransactionType)
		if err != nil {
			s.logger.Warn(err.Error())
			return report, err
		}
	}
	finance, err := s.repo.Report(ctx, managerID, username, since, to)
	if err != nil {
		s.logger.Warn(err.Error())
		return report, err
	}
	var balance float64
	for _, f := range finance {
		balance += f.Income
		balance -= f.Outcome
	}
	report.Finance = finance
	report.Balance = balance

	return report, nil
}

func (s Service) WriteOffBalance(ctx context.Context, userID int, sum decimal.Decimal, managerID int) (user *model.User, err error) {
	user, err = s.repo.GetUserByID(ctx, userID)
	if err != nil {
		return nil, err
	}
	userBalance := decimal.NewFromFloat(*user.Balance)
	if sum.GreaterThan(userBalance) {
		return user, errors.New("no money")
	}

	_, err = s.repo.BalanceWriteOff(ctx, userID, sum, managerID)
	if err != nil {
		return nil, err
	}
	return s.repo.GetUserByID(ctx, userID)
}

func (s Service) GetVerificationCodeByUserID(ctx context.Context, userID int) (string, error) {
	verificationCode := uuid.NewString()
	err := s.repo.SetVerificationCodeByUserID(ctx, userID, verificationCode)
	if err != nil {
		s.logger.Warn("get verification code error: " + err.Error())
		return "", err
	}
	return verificationCode, err
}

func (s Service) VerifyActivationCode(ctx context.Context, verificationCode string, userId int) error {
	userVerificationCode, codeErr := s.repo.GetUserVerifyActivationCode(ctx, userId)
	if codeErr != nil {
		s.logger.Warn("get verification code error: " + codeErr.Error())
		return codeErr
	}
	if verificationCode != userVerificationCode {
		return errors.New("указан неверный код")
	} else {
		activeErr := s.repo.ActivateUser(ctx, userId, true)
		if activeErr != nil {
			s.logger.Warn("set active error: " + activeErr.Error())
			return activeErr
		}
	}
	return nil
}
