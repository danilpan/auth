package service

import (
	"fmt"
	"golang.org/x/net/context"
	"processor/internal/model"
	"strconv"
	"strings"
)

func (s Service) GetInvoices(shipper, recipient, id, status string, ctx context.Context) (*[]model.Invoice, error) {
	data, dataErr := s.repo.GetInvoices(shipper, recipient, id, status, ctx)
	if dataErr != nil {
		s.logger.Warn("get invoices error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) GetInvoice(id int, ctx context.Context) (*[]model.InvoiceItem, error) {
	data, dataErr := s.repo.GetInvoice(id, ctx)
	if dataErr != nil {
		s.logger.Warn("get invoices error")
		return nil, dataErr
	}
	return data, nil
}

func (s Service) ApproveInvoice(ctx context.Context, ids model.ApproveInvoiceRq) error {
	var id []int64
	for _, i := range ids.Id {
		id = append(id, i.Id)
	}
	dataErr := s.repo.ApproveInvoice(ctx, ids)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return dataErr
	}
	references, errGIR := s.repo.GetInvoiceReferencesByID(ctx, id)
	if errGIR != nil {
		s.logger.Warn("get references")
		return errGIR
	}
	var refs []string
	var orders []model.OrderItemReturn
	for _, r := range references {
		for _, i := range ids.Id {
			if int64(r.Id) == i.Id {
				orders = append(orders, model.OrderItemReturn{
					Reference:        r.ReferenceNum,
					RejectedItemsNum: i.RejectedItems,
				})
			}
		}
		refs = append(refs, r.ReferenceNum)
	}
	dataUOIB := s.repo.UpdateOrderItemsBatch(ctx, refs, "Готово к выдаче")
	if dataUOIB != nil {
		s.logger.Warn("approve invoices error")
		return dataUOIB
	}
	dataUOIBO := s.repo.UpdateOrderItemsBatchOrders(ctx, orders)
	if dataUOIBO != nil {
		s.logger.Warn("approve invoices error")
		return dataUOIBO
	}
	return nil
}

func (s Service) CreateInvoice(rows [][]string, invoice model.Invoice, ctx context.Context) error {
	var parts []model.InvoiceItem
	var references []string
	for _, row := range rows[9:] {
		if row[0] == "" {
			break
		}
		qt, err := strconv.Atoi(row[7])
		if err != nil {
			s.logger.Warn("parse quantity " + row[7])
			s.logger.Warn(row)
			return err
		}
		price, errPrice := strconv.ParseFloat(row[11], 32)
		if errPrice != nil {
			s.logger.Warn("parse price " + row[11])
			s.logger.Warn(row)
			return errPrice
		}
		amount, errAmount := strconv.ParseFloat(row[12], 32)
		if errAmount != nil {
			s.logger.Warn("parse amount " + row[12])
			s.logger.Warn(row)
			return errAmount
		}
		weight, errWeight := strconv.ParseFloat(row[13], 32)
		if errWeight != nil {
			s.logger.Warn("parse weight " + row[13])
			s.logger.Warn(row)
			return errWeight
		}
		subId, errSubId := strconv.Atoi(row[17])
		if errSubId != nil {
			s.logger.Warn("parse sub id " + row[17])
			s.logger.Warn(row)
			return errSubId
		}
		portion, errPort := strconv.Atoi(row[18])
		if errPort != nil {
			s.logger.Warn("parse portion " + row[18])
			s.logger.Warn(row)
			return errPort
		}
		custPrice, errCustPrice := strconv.ParseFloat(row[21], 32)
		if errCustPrice != nil {
			s.logger.Warn("parse cust price " + row[21])
			s.logger.Warn(row)
			return errCustPrice
		}
		referenceTmp := strings.Split(row[4], " ")
		var reference []string
		for _, r := range referenceTmp {
			if r != " " {
				reference = append(reference, r)
			}
		}

		parts = append(parts, model.InvoiceItem{
			Box:              row[1],
			Brand:            row[2],
			PartNumber:       row[3],
			Reference:        row[4],
			PartName:         row[5],
			PartTotal:        row[6],
			Quantity:         qt,
			Price:            price,
			Amount:           amount,
			Weight:           weight,
			PartNameEn:       row[14],
			OriginCountry:    row[15],
			BrandName:        row[16],
			SubId:            subId,
			Portion:          portion,
			Barcode:          row[19],
			SubCustomerPrice: custPrice,
			HsCode:           row[22],
			CrossBrand:       row[23],
			CrossNumber:      row[24],
			ReferenceNum:     reference[0],
			Login:            reference[3],
			RejectedItems:    0,
		})
		references = append(references, reference[0])

	}
	s.logger.Warn("rows " + string(len(parts)))
	dataErr := s.repo.CreateInvoice(invoice, parts, ctx)
	if dataErr != nil {
		s.logger.Warn(fmt.Sprintf("create invoice error, %v", dataErr))
		return dataErr
	}

	errUOIB := s.repo.UpdateOrderItemsBatch(ctx, references, "В пути на склад Xamilion")
	if errUOIB != nil {
		s.logger.Warn(fmt.Sprintf("update order items error, %v", errUOIB))
		return errUOIB
	}
	return nil
}

func (s Service) GetUserInvoices(ctx context.Context, login string) ([]model.InvoiceUser, error) {
	rs, dataErr := s.repo.GetUserInvoices(ctx, login)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.InvoiceUser{}, dataErr
	}
	return rs, nil
}

func (s Service) GetUserInvoice(ctx context.Context, login string, id int64) ([]model.InvoiceItemUser, error) {
	rs, dataErr := s.repo.GetUserInvoice(ctx, login, id)
	if dataErr != nil {
		s.logger.Warn("approve invoices error")
		return []model.InvoiceItemUser{}, dataErr
	}
	return rs, nil
}
