package model

import "encoding/xml"

type Envelope struct {
	Body     Body     `xml:"Body"`
	XMLName  xml.Name `xml:"Envelope"`
	XMLNSs   string   `xml:"xmlns:soap,attr"`
	XMLNSxsi string   `xml:"xmlns:xsi,attr"`
	XMLNSxsd string   `xml:"xmlns:xsd,attr"`
}

type Body struct {
	SearchPartExResponse SearchPartExResponse `xml:"SearchPartResponse"`
	Prefix               string               `xml:"__prefix"`
}

type SearchPartExResponse struct {
	SearchPartExResult SearchPartExResult `xml:"SearchPartResult"`
	Xmlns              string             `xml:"_xmlns"`
}

type SearchPartExResult struct {
	FindByNumber []FindByNumber `xml:"FindByNumber"`
}

type FindByNumber struct {
	Available         string `xml:"Available" json:"available"`
	BitOldNum         string `xml:"bitOldNum" json:"bitOldNum"`
	PercentSupped     string `xml:"PercentSupped" json:"percentSupped"`
	PriceID           string `xml:"PriceId" json:"priceID"`
	Region            string `xml:"Region" json:"region"`
	Delivery          string `xml:"Delivery" json:"delivery"`
	Make              string `xml:"Make" json:"make"`
	DetailNum         string `xml:"DetailNum" json:"detailNum"`
	PriceLogo         string `xml:"PriceLogo" json:"priceLogo"`
	Price             string `xml:"Price" json:"price"`
	PartNameRus       string `xml:"PartNameRus" json:"partNameRus"`
	PartNameEng       string `xml:"PartNameEng" json:"partNameEng"`
	WeightGr          string `xml:"WeightGr" json:"weightGr"`
	MakeName          string `xml:"MakeName" json:"makeName"`
	Packing           string `xml:"Packing" json:"packing"`
	BitECO            string `xml:"bitECO" json:"bitECO"`
	BitWeightMeasured string `xml:"bitWeightMeasured" json:"bitWeightMeasured"`
	VolumeAdd         string `xml:"VolumeAdd" json:"volumeAdd"`
	GuaranteedDay     string `xml:"GuaranteedDay" json:"guaranteedDay"`
}

type Part struct {
	Id                string  `json:"id"`
	Available         string  `xml:"Available" json:"available"`
	BitOldNum         string  `xml:"bitOldNum" json:"bitOldNum"`
	PercentSupped     string  `xml:"PercentSupped" json:"percentSupped"`
	PriceID           string  `xml:"PriceId" json:"priceID"`
	Region            string  `xml:"Region" json:"region"`
	Delivery          string  `xml:"Delivery" json:"delivery"`
	Delivery2         string  `xml:"Delivery2" json:"delivery2"`
	Make              string  `xml:"Make" json:"make"`
	DetailNum         string  `xml:"DetailNum" json:"detailNum"`
	PriceLogo         string  `xml:"PriceLogo" json:"priceLogo"`
	Price             string  `xml:"Price" json:"price"`
	Price2            string  `json:"price2"`
	PartNameRus       string  `xml:"PartNameRus" json:"partNameRus"`
	PartNameEng       string  `xml:"PartNameEng" json:"partNameEng"`
	WeightGr          float64 `xml:"WeightGr" json:"weight"`
	MakeName          string  `xml:"MakeName" json:"makeName"`
	Packing           string  `xml:"Packing" json:"packing"`
	BitECO            string  `xml:"bitECO" json:"bitECO"`
	BitWeightMeasured string  `xml:"bitWeightMeasured" json:"bitWeightMeasured"`
	VolumeAdd         string  `xml:"VolumeAdd" json:"volumeAdd"`
	GuaranteedDay     string  `xml:"GuaranteedDay" json:"guaranteedDay"`
	Origin            string  `json:"origin"`
	Original          string  `json:"original"`
}

type Make string

const (
	Su Make = "SU"
	Ty Make = "TY"
)

type MakeName string

const (
	Subaru MakeName = "Subaru"
	Toyota MakeName = "Toyota"
)

type PartNameEng string

const (
	ValveAssyWasherHose PartNameEng = "VALVE ASSY, WASHER HOSE"
)

type PartNameRus string

const (
	КлапанОмывателяСтекла PartNameRus = "КЛАПАН ОМЫВАТЕЛЯ СТЕКЛА"
)

type EmexRq struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text         string `xml:",chardata"`
		SearchPartEx struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
			DetailNum   string `xml:"DetailNum"`
			ShowSubsets string `xml:"ShowSubsts"`
		} `xml:"SearchPart"`
	} `xml:"soap:Body"`
}

type EmexRuSearch struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Xsi     string   `xml:"xmlns:xsi,attr"`
	Xsd     string   `xml:"xmlns:xsd,attr"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text               string `xml:",chardata"`
		FindDetailAdv4Test struct {
			Text                          string `xml:",chardata"`
			Xmlns                         string `xml:"xmlns,attr"`
			Login                         string `xml:"login"`
			Password                      string `xml:"password"`
			MakeLogo                      string `xml:"makeLogo"`
			DetailNum                     string `xml:"detailNum"`
			SubstLevel                    string `xml:"substLevel"`
			SubstFilter                   string `xml:"substFilter"`
			DeliveryRegionType            string `xml:"deliveryRegionType"`
			MinDeliveryPercentString      string `xml:"minDeliveryPercentString"`
			MaxADDaysString               string `xml:"maxADDaysString"`
			MinQuantityString             string `xml:"minQuantityString"`
			MaxResultPriceString          string `xml:"maxResultPriceString"`
			MaxOneDetailOffersCountString string `xml:"maxOneDetailOffersCountString"`
			DetailNumsToLoadString        string `xml:"detailNumsToLoadString"`
		} `xml:"FindDetailAdv4_Test"`
	} `xml:"soap:Body"`
}

type EmexRs struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Xsi     string   `xml:"xmlns:xsi,attr"`
	Xsd     string   `xml:"xmlns:xsd,attr"`
	Body    struct {
		Text                 string `xml:",chardata"`
		SearchPartExResponse struct {
			Text               string `xml:",chardata"`
			Xmlns              string `xml:"xmlns,attr"`
			SearchPartExResult struct {
				Text         string `xml:",chardata"`
				FindByNumber []struct {
					Text              string `xml:",chardata"`
					Available         string `xml:"Available"`
					BitOldNum         string `xml:"bitOldNum"`
					PercentSupped     string `xml:"PercentSupped"`
					PriceId           string `xml:"PriceId"`
					Region            string `xml:"Region"`
					Delivery          string `xml:"Delivery"`
					Make              string `xml:"Make"`
					DetailNum         string `xml:"DetailNum"`
					PriceLogo         string `xml:"PriceLogo"`
					Price             string `xml:"Price"`
					PartNameRus       string `xml:"PartNameRus"`
					PartNameEng       string `xml:"PartNameEng"`
					WeightGr          string `xml:"WeightGr"`
					MakeName          string `xml:"MakeName"`
					Packing           string `xml:"Packing"`
					BitECO            string `xml:"bitECO"`
					BitWeightMeasured string `xml:"bitWeightMeasured"`
					VolumeAdd         string `xml:"VolumeAdd"`
					GuaranteedDay     string `xml:"GuaranteedDay"`
				} `xml:"FindByNumber"`
			} `xml:"SearchPartExResult"`
		} `xml:"SearchPartExResponse"`
	} `xml:"soap:Body"`
}

type EmexCartRq struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text         string `xml:",chardata"`
		SearchPartEx struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
			Array struct {
				Partstobasket []Partstobasket `xml:"partstobasket"`
			}
			DetailNum string `xml:"DetailNum"`
		} `xml:"InsertPartToBasket"`
	} `xml:"soap:Body"`
}
type Partstobasket struct {
	XMLName             xml.Name `xml:"partstobasket"`
	Text                string   `xml:",chardata"`
	CoeffMaxAgree       string   `xml:"CoeffMaxAgree" json:"coeffMaxAgree"`
	UploadedPrice       string   `xml:"UploadedPrice" json:"uploadedPrice"`
	BitAgree            string   `xml:"bitAgree" json:"bitAgree"`
	OnlyThisBrand       string   `xml:"OnlyThisBrand" json:"onlyThisBrand"`
	Confirm             string   `xml:"Confirm" json:"confirm"`
	Delete              string   `xml:"Delete" json:"delete"`
	BasketId            string   `xml:"BasketId" json:"basketId"`
	MakeLogo            string   `xml:"MakeLogo" json:"makeLogo"`
	DetailNum           string   `xml:"DetailNum" json:"detailNum"`
	DestinationLogo     string   `xml:"DestinationLogo" json:"destinationLogo"`
	PriceLogo           string   `xml:"PriceLogo" json:"priceLogo"`
	Quantity            string   `xml:"Quantity" json:"quantity"`
	BitOnly             string   `xml:"bitOnly" json:"bitOnly"`
	BitQuantity         string   `xml:"bitQuantity" json:"bitQuantity"`
	BitWait             string   `xml:"bitWait" json:"bitWait"`
	Reference           string   `xml:"Reference" json:"reference"`
	CustomerSubId       string   `xml:"CustomerSubId" json:"customerSubId"`
	TransportPack       string   `xml:"TransportPack" json:"transportPack"`
	DetailWeight        string   `xml:"DetailWeight" json:"detailWeight"`
	EmExWeight          string   `xml:"EmExWeight" json:"emExWeight"`
	CustomerStickerData string   `xml:"CustomerStickerData" json:"customerStickerData"`
}

type PartstobasketRu struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Xsi     string   `xml:"xmlns:xsi,attr"`
	Xsd     string   `xml:"xmlns:xsd,attr"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text            string `xml:",chardata"`
		InsertToBasket3 struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Login    string `xml:"login"`
			Password string `xml:"password"`
			EPrices  struct {
				Text   string   `xml:",chardata"`
				EPrice []PartRu `xml:"EPrice"`
			} `xml:"ePrices"`
		} `xml:"InsertToBasket3"`
	} `xml:"soap:Body"`
}

type PartRu struct {
	Text               string `xml:",chardata"`
	Num                string `xml:"Num"`
	DLogo              string `xml:"DLogo"`
	MName              string `xml:"MName"`
	MLogo              string `xml:"MLogo"`
	DNum               string `xml:"DNum"`
	Ref                string `xml:"Ref"`
	Name               string `xml:"Name"`
	Com                string `xml:"Com"`
	Quan               string `xml:"Quan"`
	Price              string `xml:"Price"`
	PLogo              string `xml:"PLogo"`
	Notc               string `xml:"Notc"`
	Error              string `xml:"Error"`
	DeliveryRegionType string `xml:"DeliveryRegionType"`
}

type SoapSearchResult struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                       string `xml:",chardata"`
		FindDetailAdv4TestResponse struct {
			Text                     string `xml:",chardata"`
			Xmlns                    string `xml:"xmlns,attr"`
			FindDetailAdv4TestResult struct {
				Text    string `xml:",chardata"`
				Details struct {
					Text           string `xml:",chardata"`
					SoapDetailItem []struct {
						Text                  string `xml:",chardata"`
						GroupId               string `xml:"GroupId"`
						PriceGroup            string `xml:"PriceGroup"`
						MakeLogo              string `xml:"MakeLogo"`
						MakeName              string `xml:"MakeName"`
						DetailNum             string `xml:"DetailNum"`
						DetailNameRus         string `xml:"DetailNameRus"`
						DestinationLogo       string `xml:"DestinationLogo"`
						PriceCountry          string `xml:"PriceCountry"`
						LotQuantity           string `xml:"LotQuantity"`
						Quantity              string `xml:"Quantity"`
						DDPercent             string `xml:"DDPercent"`
						ADDays                string `xml:"ADDays"`
						DeliverTimeGuaranteed string `xml:"DeliverTimeGuaranteed"`
						ResultPrice           string `xml:"ResultPrice"`
						PriceLogo             string `xml:"PriceLogo"`
						NewDetailNum          string `xml:"NewDetailNum"`
					} `xml:"SoapDetailItem"`
				} `xml:"Details"`
				IsSuccess    string `xml:"IsSuccess"`
				BlockDateEnd struct {
					Text string `xml:",chardata"`
					Nil  string `xml:"nil,attr"`
				} `xml:"BlockDateEnd"`
			} `xml:"FindDetailAdv4_TestResult"`
		} `xml:"FindDetailAdv4_TestResponse"`
	} `xml:"Body"`
}

type SoapDetailItem struct {
	Text                  string `xml:",chardata"`
	GroupId               string `xml:"GroupId"`
	PriceGroup            string `xml:"PriceGroup"`
	MakeLogo              string `xml:"MakeLogo"`
	MakeName              string `xml:"MakeName"`
	DetailNum             string `xml:"DetailNum"`
	DetailNameRus         string `xml:"DetailNameRus"`
	DestinationLogo       string `xml:"DestinationLogo"`
	PriceCountry          string `xml:"PriceCountry"`
	LotQuantity           string `xml:"LotQuantity"`
	Quantity              string `xml:"Quantity"`
	DDPercent             string `xml:"DDPercent"`
	ADDays                string `xml:"ADDays"`
	DeliverTimeGuaranteed string `xml:"DeliverTimeGuaranteed"`
	ResultPrice           string `xml:"ResultPrice"`
	PriceLogo             string `xml:"PriceLogo"`
	NewDetailNum          string `xml:"NewDetailNum"`
}

type MovementInWork struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text           string `xml:",chardata"`
		MovementInWork struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementInWork"`
	} `xml:"soap:Body"`
}
type MovementPurchased struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text              string `xml:",chardata"`
		MovementPurchased struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementPurchased"`
	} `xml:"soap:Body"`
}
type MovementNotAvailable struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text                 string `xml:",chardata"`
		MovementNotAvailable struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementNotAvailable"`
	} `xml:"soap:Body"`
}
type MovementReadyToSend struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text                string `xml:",chardata"`
		MovementReadyToSend struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementReadyToSend"`
	} `xml:"soap:Body"`
}
type MovementReceivedOnStock struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text                    string `xml:",chardata"`
		MovementReceivedOnStock struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementReceivedOnStock"`
	} `xml:"soap:Body"`
}
type MovementSent struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text         string `xml:",chardata"`
		MovementSent struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementSent"`
	} `xml:"soap:Body"`
}
type MovementAGREE struct {
	XMLName xml.Name `xml:"soap:Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"xmlns:soap,attr"`
	Body    struct {
		Text          string `xml:",chardata"`
		MovementAGREE struct {
			Text     string `xml:",chardata"`
			Xmlns    string `xml:"xmlns,attr"`
			Customer struct {
				Text          string `xml:",chardata"`
				UserName      string `xml:"UserName"`
				Password      string `xml:"Password"`
				SubCustomerId string `xml:"SubCustomerId"`
				CustomerId    string `xml:"CustomerId"`
			} `xml:"Customer"`
		} `xml:"MovementAGREE"`
	} `xml:"soap:Body"`
}

type MovementInWorkRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                   string `xml:",chardata"`
		MovementInWorkResponse struct {
			Text                 string `xml:",chardata"`
			Xmlns                string `xml:"xmlns,attr"`
			MovementInWorkResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementInWorkResult"`
		} `xml:"MovementInWorkResponse"`
	} `xml:"Body"`
}
type MovementPurchasedRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                      string `xml:",chardata"`
		MovementPurchasedResponse struct {
			Text                    string `xml:",chardata"`
			Xmlns                   string `xml:"xmlns,attr"`
			MovementPurchasedResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementPurchasedResult"`
		} `xml:"MovementPurchasedResponse"`
	} `xml:"Body"`
}

type MovementNotAvailableRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                         string `xml:",chardata"`
		MovementNotAvailableResponse struct {
			Text                       string `xml:",chardata"`
			Xmlns                      string `xml:"xmlns,attr"`
			MovementNotAvailableResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementNotAvailableResult"`
		} `xml:"MovementNotAvailableResponse"`
	} `xml:"Body"`
}

type MovementReadyToSendRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                        string `xml:",chardata"`
		MovementReadyToSendResponse struct {
			Text                      string `xml:",chardata"`
			Xmlns                     string `xml:"xmlns,attr"`
			MovementReadyToSendResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementReadyToSendResult"`
		} `xml:"MovementReadyToSendResponse"`
	} `xml:"Body"`
}

type MovementReceivedOnStockRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                            string `xml:",chardata"`
		MovementReceivedOnStockResponse struct {
			Text                          string `xml:",chardata"`
			Xmlns                         string `xml:"xmlns,attr"`
			MovementReceivedOnStockResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementReceivedOnStockResult"`
		} `xml:"MovementReceivedOnStockResponse"`
	} `xml:"Body"`
}

type MovementSentRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                 string `xml:",chardata"`
		MovementSentResponse struct {
			Text               string `xml:",chardata"`
			Xmlns              string `xml:"xmlns,attr"`
			MovementSentResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementSentResult"`
		} `xml:"MovementSentResponse"`
	} `xml:"Body"`
}

type MovementAGREERs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	Soap    string   `xml:"soap,attr"`
	Xsi     string   `xml:"xsi,attr"`
	Xsd     string   `xml:"xsd,attr"`
	Body    struct {
		Text                  string `xml:",chardata"`
		MovementAGREEResponse struct {
			Text                string `xml:",chardata"`
			Xmlns               string `xml:"xmlns,attr"`
			MovementAGREEResult struct {
				Text     string `xml:",chardata"`
				Movement []struct {
					Text             string `xml:",chardata"`
					OrderNumber      string `xml:"OrderNumber"`
					DocumentDate     string `xml:"DocumentDate"`
					OrderDetailSubId string `xml:"OrderDetailSubId"`
					Comment          string `xml:"Comment"`
					PriceOrdered     string `xml:"PriceOrdered"`
					PriceSale        string `xml:"PriceSale"`
					Make             string `xml:"Make"`
					DetailNum        string `xml:"DetailNum"`
					Quantity         string `xml:"Quantity"`
					Condition        string `xml:"Condition"`
					Reference        string `xml:"Reference"`
					DetailNameRus    string `xml:"DetailNameRus"`
					DetailNameEng    string `xml:"DetailNameEng"`
					CustomerSubId    string `xml:"CustomerSubId"`
					SubstMake        string `xml:"SubstMake"`
					SubstNum         string `xml:"SubstNum"`
					DestinationLogo  string `xml:"DestinationLogo"`
					PriceLogo        string `xml:"PriceLogo"`
					StatusId         string `xml:"StatusId"`
					StateText        string `xml:"StateText"`
				} `xml:"Movement"`
			} `xml:"MovementAGREEResult"`
		} `xml:"MovementAGREEResponse"`
	} `xml:"Body"`
}
