package model

type RqAlArkana []RqAlArkanaElement

type RqAlArkanaElement struct {
	PartNumber string `json:"partNumber"`
	Delivery   string `json:"delivery"`
}

type RsAlArkana []RsAlArkanaElement

type RsAlArkanaElement struct {
	QuotationID   string      `json:"quotationId"`
	ID            string      `json:"id"`
	PartNumber    string      `json:"partNumber"`
	Brand         string      `json:"brand"`
	WeightKg      float64     `json:"weightKg"`
	VolumeKg      int64       `json:"volumeKg"`
	Description   string      `json:"description"`
	Booking       string      `json:"booking"`
	Available     int64       `json:"available"`
	Quantity      interface{} `json:"quantity"`
	Price         float64     `json:"price"`
	Currency      string      `json:"currency"`
	Rating        int64       `json:"rating"`
	Days          int64       `json:"days"`
	DeliveryCost  int64       `json:"deliveryCost"`
	Delivery      string      `json:"delivery"`
	Destination   string      `json:"destination"`
	Transport     string      `json:"transport"`
	Tariff        string      `json:"tariff"`
	YourReference interface{} `json:"yourReference"`
	YourRowID     interface{} `json:"yourRowId"`
	YourOrderID   interface{} `json:"yourOrderId"`
	InpID         string      `json:"inpId"`
	InpPartNumber string      `json:"inpPartNumber"`
	InpBrand      interface{} `json:"inpBrand"`
	InpPrice      interface{} `json:"inpPrice"`
	InpQuantity   interface{} `json:"inpQuantity"`
	InpDays       int64       `json:"inpDays"`
	Excess        interface{} `json:"excess"`
	Confirmed     bool        `json:"confirmed"`
	Catalog       string      `json:"catalog"`
	Err           int64       `json:"err"`
	ErrDesc       string      `json:"errDesc"`
	Stock         *int64      `json:"stock"`
	Packing       string      `json:"packing"`
}
