package model

import "time"

type Invoice struct {
	Id         int       `json:"id" db:"id"`
	InvoiceNum string    `json:"invoiceNum" db:"invoice_num"`
	Status     string    `json:"status" db:"status"`
	Date       time.Time `json:"date" db:"date"`
	Shipper    string    `json:"shipper" db:"shipper"`
	Recipient  string    `json:"recipient" db:"recipient"`
	Total      float64   `json:"total" db:"total"`
	Manager    string    `json:"manager" db:"manager"`
}
type InvoiceUser struct {
	Id         int       `json:"id" db:"id"`
	InvoiceNum string    `json:"invoiceNum" db:"invoice_num"`
	Date       time.Time `json:"date" db:"date"`
}

type InvoiceMail struct {
	Headers struct {
		Received             string `json:"received"`
		Date                 string `json:"date"`
		From                 string `json:"from"`
		To                   string `json:"to"`
		MessageId            string `json:"message_id"`
		Subject              string `json:"subject"`
		MimeVersion          string `json:"mime_version"`
		ContentType          string `json:"content_type"`
		DkimSignature        string `json:"dkim_signature"`
		XGoogleDkimSignature string `json:"x_google_dkim_signature"`
		XGmMessageState      string `json:"x_gm_message_state"`
		XGoogleSmtpSource    string `json:"x_google_smtp_source"`
		XReceived            string `json:"x_received"`
	} `json:"headers"`
	Envelope struct {
		To         string   `json:"to"`
		Recipients []string `json:"recipients"`
		From       string   `json:"from"`
		HeloDomain string   `json:"helo_domain"`
		RemoteIp   string   `json:"remote_ip"`
		Tls        bool     `json:"tls"`
		TlsCipher  string   `json:"tls_cipher"`
		Spf        struct {
			Result string `json:"result"`
			Domain string `json:"domain"`
		} `json:"spf"`
	} `json:"envelope"`
	Plain       string      `json:"plain"`
	Html        string      `json:"html"`
	ReplyPlain  interface{} `json:"reply_plain"`
	Attachments []struct {
		Content     string `json:"content"`
		FileName    string `json:"file_name"`
		ContentType string `json:"content_type"`
		Size        string `json:"size"`
		Disposition string `json:"disposition"`
		ContentId   string `json:"content_id"`
	} `json:"attachments"`
}

type InvoiceItem struct {
	Id               int     `json:"id" db:"id"`
	InvoiceId        int     `json:"invoiceId" db:"invoice_id"`
	Box              string  `json:"box" db:"box"`
	Brand            string  `json:"brand"db:"brand"`
	PartNumber       string  `json:"partNumber" db:"part_number"`
	Reference        string  `json:"reference" db:"reference"`
	PartName         string  `json:"partName" db:"part_name"`
	PartTotal        string  `json:"partTotal" db:"part_total"`
	Quantity         int     `json:"quantity" db:"quantity"`
	Price            float64 `json:"price" db:"price"`
	Amount           float64 `json:"amount" db:"amount"`
	Weight           float64 `json:"weight" db:"weight"`
	PartNameEn       string  `json:"partNameEn" db:"part_name_en"`
	OriginCountry    string  `json:"originCountry" db:"origin_country"`
	BrandName        string  `json:"brandName" db:"brand_name"`
	SubId            int     `json:"subId" db:"sub_id"`
	Portion          int     `json:"portion" db:"portion"`
	Barcode          string  `json:"barcode" db:"barcode"`
	SubCustomerPrice float64 `json:"subCustomerPrice" db:"sub_customer_price"`
	HsCode           string  `json:"hsCode" db:"hs_code"`
	CrossBrand       string  `json:"crossBrand" db:"cross_brand"`
	CrossNumber      string  `json:"crossNumber" db:"cross_number"`
	Approved         bool    `json:"approved" db:"approved"`
	ReferenceNum     string  `json:"referenceNum" db:"reference_num"`
	Login            string  `json:"login" db:"login"`
	RejectedItems    int     `json:"rejectedItems" db:"rejected_items_num"`
}

type InvoiceItemUser struct {
	InvoiceId     int     `json:"invoiceId" db:"invoice_id"`
	Box           string  `json:"box" db:"box"`
	Brand         string  `json:"brand"db:"brand"`
	PartNumber    string  `json:"partNumber" db:"part_number"`
	PartName      string  `json:"partName" db:"part_name"`
	Quantity      int     `json:"quantity" db:"quantity"`
	Price         float64 `json:"price" db:"price"`
	OriginCountry string  `json:"originCountry" db:"origin_country"`
	BrandName     string  `json:"brandName" db:"brand_name"`
	Barcode       string  `json:"barcode" db:"barcode"`
	HsCode        string  `json:"hsCode" db:"hs_code"`
	CrossBrand    string  `json:"crossBrand" db:"cross_brand"`
	CrossNumber   string  `json:"crossNumber" db:"cross_number"`
	ReferenceNum  string  `json:"referenceNum" db:"reference_num"`
	Login         string  `json:"login" db:"login"`
}
