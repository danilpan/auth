package model

import (
	"database/sql"
	"encoding/json"
)

type MyNullString struct {
	sql.NullString
}

type Error struct {
	Message string `json:"msg"`
}

func (s MyNullString) MarshalJSON() ([]byte, error) {
	if s.Valid {
		return json.Marshal(s.String)
	}
	return []byte(`null`), nil
}

type MyNullInt struct {
	sql.NullInt64
}

type ResponseMessage struct {
	Message string `json:"message"`
	Success bool   `json:"success"`
}

type RequestGUID struct {
	GUID string `json:"guid"`
}
