package model

import (
	"github.com/shopspring/decimal"
	"time"
)

type OrderItem struct {
	ID               string          `json:"id" db:"id"`
	PartNumber       string          `json:"partNumber" db:"part_number"`
	Brand            string          `json:"brand" db:"brand"`
	WeightKg         float64         `json:"weightKg" db:"weight_kg"`
	VolumeKg         string          `json:"volumeKg" db:"volume_kg"`
	Description      string          `json:"description" db:"description"`
	Booking          string          `json:"booking" db:"booking"`
	Available        string          `json:"available" db:"available"`
	Quantity         interface{}     `json:"quantity" db:"quantity"`
	Price            string          `json:"price" db:"price"`
	Currency         string          `json:"currency" db:"currency"`
	Days             int             `json:"days" db:"days"`
	DeliveryCost     int             `json:"deliveryCost" db:"delivery_cost"`
	Delivery         string          `json:"delivery" db:"delivery"`
	Destination      string          `json:"destination" db:"destination"`
	Transport        string          `json:"transport" db:"transport"`
	Tariff           string          `json:"tariff" db:"tariff"`
	Reference        interface{}     `json:"yourReference" db:"reference"`
	Excess           interface{}     `json:"excess" db:"status"`
	Catalog          string          `json:"catalog" db:"catalog"`
	Stock            int             `json:"stock" db:"stock"`
	OrderID          int             `json:"order_id" db:"order_id"`
	Invoice          *string         `json:"invoice" db:"invoice"`
	Comment          *string         `json:"comment" db:"comment"`
	RejectionDate    *string         `json:"rejection_date" db:"rejection_date"`
	StockName        *string         `json:"stock_name" db:"stock_name"`
	RejectedItemsNum int             `json:"rejected_items_num" db:"rejected_items_num"`
	MakeName         string          `json:"makeName" db:"make_name"`
	PartName         string          `json:"partName" db:"part_name"`
	Rate             decimal.Decimal `json:"rate" db:"rate"`
	OriginalPrice    decimal.Decimal `json:"originalPrice" db:"original_price"`
	History          []History       `json:"history"`
}

type Order struct {
	ID         int             `json:"id,omitempty" db:"id"`
	UserID     int             `json:"userID" db:"user_id"`
	StatusID   int             `json:"status_id" db:"status_id"`
	Status     *Status         `json:"status"`
	IsPaid     bool            `json:"is_paid" db:"is_paid"`
	ManagerID  int             `json:"manager_id" db:"manager_id"`
	Amount     decimal.Decimal `json:"amount" db:"amount"`
	CreatedAt  *time.Time      `json:"created_at,omitempty" db:"created_at"`
	OrderItems *[]OrderItem    `json:"order_items,omitempty" db:"order_items"`
}

type OrderItems struct {
	Id         int       `json:"id"`
	OrderId    int       `json:"orderId"`
	Reference  string    `json:"reference"`
	PartNumber string    `json:"partNumber"`
	Brand      string    `json:"brand"`
	Quantity   string    `json:"quantity"`
	Price      float64   `json:"price"`
	ManagerId  int       `json:"managerId"`
	UserId     int       `json:"userId"`
	Status     string    `json:"status"`
	History    []History `json:"history"`
}

type OrderItemsV2 struct {
	Status        string          `json:"status" db:"status"`
	Id            int             `json:"id" db:"id"`
	UserName      string          `json:"userName" db:"username"`
	Reference     string          `json:"reference" db:"reference"`
	PartNumber    string          `json:"partNumber" db:"part_number"`
	PartName      string          `json:"partName" db:"part_name"`
	MakeName      string          `json:"makeName" db:"make_name"`
	Comment       string          `json:"comment" db:"comment"`
	Price         decimal.Decimal `json:"price" db:"price"`
	Quantity      string          `json:"quantity" db:"quantity"`
	Amount        decimal.Decimal `json:"amount" db:"amount"`
	OriginalPrice decimal.Decimal `json:"originalPrice" db:"original_price"`
	Manager       string          `json:"manager" db:"manager"`
	History       []History       `json:"history"`
	Link          string          `json:"link"`
}

type OrderItemsTable struct {
	Id         int     `json:"id"`
	OrderId    int     `json:"orderId"`
	Reference  string  `json:"reference"`
	PartNumber string  `json:"partNumber"`
	Brand      string  `json:"brand"`
	Price      float64 `json:"price"`
	Quantity   string  `json:"quantity"`
	Rejected   int64   `json:"rejected"`
	Amount     float64 `json:"amount"`
	Status     string  `json:"status"`
	Comment    string  `json:"comment"`
	Stock      string  `json:"stock"`
	Invoice    string  `json:"invoice"`
}

type OrderItemReturn struct {
	Reference        string `json:"reference"`
	RejectedItemsNum int64  `json:"rejected_items_num" db:"rejected_items_num"`
}

type OrderItemsStatus struct {
	Status string        `json:"status"`
	Id     []OrderItemSt `json:"items"`
}
type OrderItemSt struct {
	Id int64 `json:"id"`
}

type History struct {
	Date    string `json:"date"`
	Status  string `json:"status"`
	Comment string `json:"comment"`
}
