package model

type ArmtekRs struct {
	Status   int64         `json:"STATUS"`
	Messages []interface{} `json:"MESSAGES"`
	Resp     []Resp        `json:"RESP"`
}

type Resp struct {
	Artid   string `json:"ARTID"`
	Parnr   string `json:"PARNR"`
	Keyzak  string `json:"KEYZAK"`
	Rvalue  string `json:"RVALUE"`
	Rdprf   string `json:"RDPRF"`
	Minbm   string `json:"MINBM"`
	Retdays string `json:"RETDAYS"`
	Vensl   string `json:"VENSL"`
	Price   string `json:"PRICE"`
	Waers   string `json:"WAERS"`
	Dlvdt   string `json:"DLVDT"`
	Wrntdt  string `json:"WRNTDT"`
	Analog  string `json:"ANALOG"`
	Pin     string `json:"PIN"`
	Brand   string `json:"BRAND"`
	Name    string `json:"NAME"`
}

type ArmtekRq struct {
	VKORG     string `json:"VKORG"`
	PIN       string `json:"PIN"`
	KUNNRRG   string `json:"KUNNR_RG"`
	INCOTERMS string `json:"INCOTERMS"`
	KUNNRZA   string `json:"KUNNR_ZA"`
}

type ArmtekOrderRq struct {
	VKORG     string       `json:"VKORG"`
	KUNRG     string       `json:"KUNRG"`
	INCOTERMS string       `json:"INCOTERMS"`
	DBTYP     string       `json:"DBTYP"`
	KUNNRZA   string       `json:"KUNNR_ZA"`
	ITEMS     []ArmtekItem `json:"ITEMS"`
}

type ArmtekItem struct {
	PIN      string `json:"PIN"`
	BRAND    string `json:"BRAND"`
	KWMENG   string `json:"KWMENG"`
	KEYZAK   string `json:"KEYZAK"`
	COMPLDLV string `json:"COMPL_DLV"`
}

type ArmtekOrder struct {
	STATUS   int           `json:"STATUS"`
	MESSAGES []interface{} `json:"MESSAGES"`
	RESP     struct {
		ITEMS []struct {
			PIN      string `json:"PIN"`
			BRAND    string `json:"BRAND"`
			KEYZAK   string `json:"KEYZAK"`
			KWMENG   string `json:"KWMENG"`
			PRICEMAX string `json:"PRICEMAX"`
			DATEMAX  string `json:"DATEMAX"`
			COMMENT  string `json:"COMMENT"`
			COMPLDLV string `json:"COMPL_DLV"`
			ARTID    string `json:"ARTID"`
			RESULT   []struct {
				POSID  string `json:"POSID"`
				POSNR  string `json:"POSNR"`
				KEYZAK string `json:"KEYZAK"`
				NUMZAK string `json:"NUM_ZAK"`
				KWMENG int    `json:"KWMENG"`
				RVALUE int    `json:"RVALUE"`
				PRICE  string `json:"PRICE"`
				WAERS  string `json:"WAERS"`
				DLVDT  string `json:"DLVDT"`
				VBELN  string `json:"VBELN"`
				BLOCK  string `json:"BLOCK"`
				ERROR  string `json:"ERROR"`
			} `json:"RESULT"`
			REMAIN       int    `json:"REMAIN"`
			ERROR        int    `json:"ERROR"`
			ERRORMESSAGE string `json:"ERROR_MESSAGE"`
		} `json:"ITEMS"`
	} `json:"RESP"`
}

type ArmtekGetOrderRs struct {
	STATUS   int                  `json:"STATUS"`
	MESSAGES []interface{}        `json:"MESSAGES"`
	RESP     ArmtekGetOrderRsResp `json:"RESP"`
}

type ArmtekGetOrderRsResp struct {
	HEADER ArmtekGetOrderRsHeader  `json:"HEADER"`
	ITEMS  []ArmtekGetOrderRsItems `json:"ITEMS"`
}

type ArmtekGetOrderRsHeader struct {
	ORDER        string `json:"ORDER"`
	ORDERTYPE    string `json:"ORDER_TYPE"`
	ORDERSTATUS  string `json:"ORDER_STATUS"`
	ORDERDATE    string `json:"ORDER_DATE"`
	ORDERSUM     string `json:"ORDER_SUM"`
	CURRENCY     string `json:"CURRENCY"`
	KUNRG        string `json:"KUNRG"`
	NAMERG       string `json:"NAMERG"`
	KUNWE        string `json:"KUNWE"`
	NAMEWE       string `json:"NAMEWE"`
	KUNZA        string `json:"KUNZA"`
	NAMEZA       string `json:"NAMEZA"`
	ADDRZA       string `json:"ADDRZA"`
	PARNRAP      string `json:"PARNRAP"`
	NAMEAP       string `json:"NAMEAP"`
	PARNRZP      string `json:"PARNRZP"`
	NAMEZP       string `json:"NAMEZP"`
	ETDAT        string `json:"ETDAT"`
	VDATU        string `json:"VDATU"`
	DOCNUM       string `json:"DOC_NUM"`
	DOCDATE      string `json:"DOC_DATE"`
	NUMDOG       string `json:"NUMDOG"`
	COMPLDLV     string `json:"COMPL_DLV"`
	COMMENT      string `json:"COMMENT"`
	COMMENTEXP   string `json:"COMMENT_EXP"`
	INCOTERMSTXT string `json:"INCOTERMS_TXT"`
	VSTELT       string `json:"VSTELT"`
}

type ArmtekGetOrderRsItems struct {
	POSNR       int    `json:"POSNR"`
	BRAND       string `json:"BRAND"`
	PIN         string `json:"PIN"`
	NAME        string `json:"NAME"`
	KWMENG      string `json:"KWMENG"`
	KWMENGREQ   string `json:"KWMENG_REQ"`
	KWMENGPROC  string `json:"KWMENG_PROC"`
	KWMENGL     int    `json:"KWMENG_L"`
	KWMENGREJP  string `json:"KWMENG_REJ_P"`
	KWMENGWAYIN int    `json:"KWMENG_WAYIN"`
	KWMENGP     int    `json:"KWMENG_P"`
	KWMENGR     string `json:"KWMENG_R"`
	KWMENGREJ   string `json:"KWMENG_REJ"`
	PRICE       string `json:"PRICE"`
	SUMMA       string `json:"SUMMA"`
	CURRENCY    string `json:"CURRENCY"`
	STATUS      string `json:"STATUS"`
	NOTE        string `json:"NOTE"`
	DLVRD       string `json:"DLVRD"`
	WRNTD       string `json:"WRNTD"`
	ABGRUTXT    string `json:"ABGRU_TXT"`
	MATNR       string `json:"MATNR"`
	COMPLDLV    int    `json:"COMPL_DLV"`
	POSEX       string `json:"POSEX"`
	POSROOT     string `json:"POSROOT"`
	CHARG       string `json:"CHARG"`
	CHARGBLK    string `json:"CHARG_BLK"`
}
