package model

type City struct {
	ID     int    `json:"id" db:"id"`
	NameRu string `json:"name_ru" db:"name_ru"`
	NameKk string `json:"name_kk" db:"name_kk"`
	NameEn string `json:"name_en" db:"name_en"`
}
