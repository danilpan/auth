package model

import (
	"github.com/shopspring/decimal"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type User struct {
	ID          int       `json:"id" db:"id"`
	Username    string    `json:"username,omitempty" db:"username"`
	FirstName   string    `json:"first_name,omitempty" db:"first_name"`
	LastName    string    `json:"last_name,omitempty" db:"last_name"`
	Password    string    `json:"password,omitempty" db:"password"`
	PhoneNumber string    `json:"phone_number,omitempty" db:"phone_number"`
	RoleID      int       `json:"role_id,omitempty" db:"role_id"`
	Email       string    `json:"email,omitempty" db:"email"`
	PartnerID   *int      `json:"partner_id,omitempty" db:"partner_id"`
	Balance     *float64  `json:"balance,omitempty" db:"balance"`
	CityID      int       `json:"city_id,omitempty" db:"city_id"`
	ManagerID   *int      `json:"manager_id,omitempty" db:"manager_id"`
	GroupID     *int      `json:"group_id,omitempty" db:"group_id"`
	Margin      *int      `json:"margin,omitempty" db:"margin"`
	IsActive    *bool     `json:"is_active,omitempty" db:"is_active"`
	CreatedAt   time.Time `json:"created_at,omitempty" db:"created_at"`
	Documents   bool      `json:"documents" db:"documents"`
}

type UserBalanceRequest struct {
	UserID     int     `json:"user_id"`
	Balance    float64 `json:"balance"`
	IsRecieved bool    `json:"is_recieved"`
}

func (user *User) HashPassword(password string) error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return err
	}
	user.Password = string(bytes)
	return nil
}
func (user *User) CheckPassword(providedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providedPassword))
	if err != nil {
		return err
	}
	return nil
}

type UserProfile struct {
	ID          int     `json:"id" db:"id"`
	Username    string  `json:"username" db:"username"`
	Balance     float64 `json:"balance" db:"balance"`
	CityID      int     `json:"city_id" db:"city_id"`
	FirstName   string  `json:"first_name" db:"first_name"`
	LastName    string  `json:"last_name" db:"last_name"`
	PhoneNumber string  `json:"phone_number" db:"phone_number"`
	RoleID      int     `json:"role_id" db:"role_id"`
	RoleName    string  `json:"role_name" db:"role_name"`
	Margin      int     `json:"margin" db:"margin"`
	Email       string  `json:"email" db:"email"`
	CreatedAt   string  `json:"created_at" db:"created_at"`
}

type UpdateUserProfileRequest struct {
	Username    string `json:"username" db:"username"`
	CityID      int    `json:"city_id" db:"city_id"`
	FirstName   string `json:"first_name" db:"first_name"`
	LastName    string `json:"last_name" db:"last_name"`
	PhoneNumber string `json:"phone_number" db:"phone_number"`
	Email       string `json:"email" db:"email"`
}

type UpdatePasswordRequest struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

type UserInfo struct {
	ID          int    `json:"id" db:"id"`
	Username    string `json:"username" db:"username"`
	FirstName   string `json:"first_name" db:"first_name"`
	LastName    string `json:"last_name" db:"last_name"`
	PhoneNumber string `json:"phone_number" db:"phone_number"`
}

type Transaction struct {
	ID            int             `json:"id" db:"id"`
	Amount        decimal.Decimal `json:"amount" db:"amount"`
	UserID        int             `json:"user_id" db:"user_id"`
	CreatedAt     string          `json:"created_at" db:"created_at"`
	WriteOff      bool            `json:"write_off" db:"write_off"`
	Replenishment bool            `json:"replenishment" db:"replenishment"`
	ManagerID     int             `json:"manager_id" db:"manager_id"`
	Manager       *UserInfo       `json:"manager"`
	User          *UserInfo       `json:"user"`
}

type TransactionRs struct {
	ID              int             `json:"id" db:"id"`
	Amount          decimal.Decimal `json:"amount" db:"amount"`
	CreatedAt       string          `json:"date" db:"created_at"`
	TransactionType string          `json:"operationType"`
	ManagerNumber   string          `json:"managerNumber" db:"manager_id"`
	Manager         string          `json:"manager"`
}

type Role struct {
	ID     int    `json:"id,omitempty" db:"id"`
	Title  string `json:"title,omitempty" db:"title"`
	Margin int    `json:"margin" db:"margin"`
}

type UpdateUserRequest struct {
	FirstName   string          `json:"first_name,omitempty"`
	LastName    string          `json:"last_name,omitempty"`
	PhoneNumber string          `json:"phone_number,omitempty"`
	Email       string          `json:"email,omitempty"`
	CityID      int             `json:"city_id,omitempty"`
	ManagerID   int             `json:"manager_id,omitempty"`
	GroupID     int             `json:"group_id,omitempty"`
	Margin      decimal.Decimal `json:"margin,omitempty"`
	RoleID      int             `json:"role_id,omitempty"`
	Document    bool            `json:"documents,omitempty"`
}

type Margin struct {
	UserMargin  decimal.Decimal `json:"userMargin" db:"user_margin"`
	GroupMargin decimal.Decimal `json:"groupMargin" db:"group_margin"`
	Emex        decimal.Decimal `json:"emex" db:"emex"`
	EmexRu      decimal.Decimal `json:"emexru" db:"emexru"`
	Local       decimal.Decimal `json:"local" db:"local"`
}

type Debtor struct {
	Name  string  `json:"name"`
	Id    int     `json:"id" db:"user_id"`
	Total float64 `json:"total" db:"total"`
}
