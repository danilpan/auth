package model

import "github.com/shopspring/decimal"

type MarginTable struct {
	Id     int             `json:"id" db:"id"`
	Title  string          `json:"title" db:"title"`
	Margin decimal.Decimal `json:"margin" db:"margin"`
	Emex   decimal.Decimal `json:"emex" db:"emex"`
	EmexRu decimal.Decimal `json:"emexru" db:"emexru"`
	Local  decimal.Decimal `json:"local" db:"local"`
}

type Volume struct {
	Id     string          `json:"id" db:"id"`
	Min    decimal.Decimal `json:"min" db:"min"`
	Max    decimal.Decimal `json:"max" db:"max"`
	Margin decimal.Decimal `json:"margin" db:"margin"`
}

type KeyWord struct {
	Id     string          `json:"id" db:"id"`
	Word   string          `json:"word" db:"keyword"`
	Margin decimal.Decimal `json:"margin" db:"margin"`
}

type Currency struct {
	Id     int             `json:"id" db:"id"`
	Title  string          `json:"title" db:"title"`
	Value  decimal.Decimal `json:"value" db:"value"`
	Date   string          `json:"date" db:"date"`
	IsAuto bool            `json:"isAuto" db:"is_automatic"`
}
