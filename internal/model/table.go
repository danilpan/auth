package model

type Table struct {
	Headers []Header `json:"headers"`
	Data    any      `json:"data"`
}

type Header struct {
	Id             string `json:"id"`
	Label          string `json:"label"`
	Numeric        bool   `json:"numeric"`
	DisablePadding bool   `json:"disablePadding"`
}
