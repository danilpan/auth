package model

type Status struct {
	ID   int    `json:"id" db:"id"`
	Name string `json:"name" db:"name"`
}

type ReferenceUpdate struct {
	ReferenceEmex string
	Reference     string `db:"reference"`
	StatusEmex    string
	Status        string
	OldStatus     string
	IsChanged     bool
}
