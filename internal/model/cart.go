package model

type CartItemQuantity struct {
	ID       int `json:"id" db:"id"`
	Quantity int `json:"quantity" db:"quantity"`
}

type AddCartItem struct {
	ID           int     `json:"id"`
	PartNumber   string  `json:"part_number"`
	Brand        string  `json:"brand"`
	Booking      string  `json:"booking"`
	Delivery     string  `json:"delivery"`
	Destination  string  `json:"destination"`
	Transport    string  `json:"transport"`
	Quantity     int     `json:"quantity"`
	Price        string  `json:"price"`
	Currency     string  `json:"currency"`
	Reference    string  `json:"reference"`
	OrderID      int     `json:"order_id"`
	Confirmed    bool    `json:"confirmed"`
	UserID       int     `json:"user_id"`
	MakeLogo     string  `json:"makeLogo"`  //
	PriceLogo    string  `json:"priceLogo"` //
	Weight       float64 `json:"weight"`    //
	Origin       string  `json:"origin"`    //
	PartID       string  `json:"part_id"`
	Availability string  `json:"availability"`  //
	MakeName     string  `json:"make_name"`     //
	PartNameRus  string  `json:"part_name_rus"` //
	Comment      string  `json:"comment"`
	VolumeAdd    string  `json:"volumeAdd" db:"volume_add"`
	Original     string  `json:"original"`
}

type CartItem struct {
	ID           int     `json:"id" db:"id"`
	PartNumber   string  `json:"part_number" db:"part_number"`
	Brand        string  `json:"brand" db:"brand"`
	Booking      string  `json:"booking" db:"booking"`
	Delivery     string  `json:"delivery" db:"delivery"`
	Destination  string  `json:"destination" db:"destination"`
	Transport    string  `json:"transport" db:"transport"`
	Quantity     int     `json:"quantity" db:"quantity"`
	Price        string  `json:"price" db:"price"`
	Currency     string  `json:"currency" db:"currency"`
	Reference    string  `json:"reference" db:"reference"`
	OrderID      int     `json:"order_id" db:"order_id"`
	Confirmed    bool    `json:"confirmed" db:"confirmed"`
	UserID       int     `json:"user_id" db:"user_id"`
	MakeLogo     string  `json:"makeLogo" db:"make_logo"`
	PriceLogo    string  `json:"priceLogo" db:"price_logo"`
	Weight       float64 `json:"weight" db:"weight"`
	Origin       string  `json:"origin" db:"origin"`
	PartID       string  `json:"part_id" db:"part_id"`
	Availability string  `json:"availability" db:"availability"`
	MakeName     string  `json:"make_name" db:"make_name"`
	PartNameRus  string  `json:"part_name_rus" db:"part_name_rus"`
	CreatedAt    string  `json:"created_at" db:"created_at"`
	Comment      *string `json:"comment" db:"comment"`
	PartName     string  `json:"partName" db:"part_name"`
	VolumeAdd    string  `json:"volumeAdd" db:"volume_add"`
	Original     string  `json:"original" db:"original"`
}

type ConfirmCartRequest struct {
	ID        int  `json:"id"`
	Confirmed bool `json:"confirmed"`
}

type DeleteCartRequest struct {
	ID string `json:"id"`
}

type ConfirmOrderRequest struct {
	ItemID   int     `json:"item_id"`
	Lines    int     `json:"lines"`
	Amount   float64 `json:"amount"`
	Currency string  `json:"currency"`
	Comment  string  `json:"comment"`
}

type EmexCart struct {
	Price        float64 `json:"price"`
	MakeLogo     string  `json:"makeLogo"`
	DetailNum    string  `json:"detailNum"`
	PriceLogo    string  `json:"priceLogo"`
	Quantity     int     `json:"quantity"`
	Reference    string  `json:"reference"`
	DetailWeight float64 `json:"detailWeight"`
}

type UserCart struct {
	CartItems []CartItem `json:"cart_items,omitempty"`
	User      User       `json:"user"`
	ManagerID int        `json:"manager_id"`
	UpdatedAt string     `json:"updated_at"`
	TotalSum  string     `json:"total_sum"`
	UserID    int        `json:"user_id,omitempty"`
	UserEmail string     `json:"user_email,omitempty"`
}

var CartItemsTable = []Header{{
	Id:             "id",
	Label:          "Номер",
	Numeric:        true,
	DisablePadding: false,
}, {
	Id:             "part_number",
	Label:          "Номер детали",
	Numeric:        false,
	DisablePadding: false,
}, {
	Id:             "brand",
	Label:          "Изготовитель",
	Numeric:        false,
	DisablePadding: false,
}, {
	Id:    "booking",
	Label: "Резерв",
}, {
	Id:    "delivery",
	Label: "Доставка",
}, {
	Id:    "destination",
	Label: "Пункт назначения",
}, {
	Id:    "transport",
	Label: "Транспорт",
}, {
	Id:             "quantity",
	Label:          "Количество",
	Numeric:        true,
	DisablePadding: false,
}, {
	Id:             "price",
	Label:          "Цена",
	Numeric:        true,
	DisablePadding: false,
}, {
	Id:    "currency",
	Label: "Валюта",
}, {
	Id:             "reference",
	Label:          "Референс",
	Numeric:        false,
	DisablePadding: false,
}, {
	Id:      "order_id",
	Label:   "Номер заказа",
	Numeric: true,
}, {
	Id:    "confirmed",
	Label: "Подтвержден",
}, {
	Id:             "user_id",
	Label:          "Номер клиента",
	Numeric:        true,
	DisablePadding: false,
}, {
	Id:    "makeLogo",
	Label: "Make Logo",
}, {
	Id:    "priceLogo",
	Label: "Склад",
}, {
	Id:      "weight",
	Label:   "Масса, кг",
	Numeric: true,
}, {
	Id:    "origin",
	Label: "Поставщик",
}, {
	Id:    "part_id",
	Label: "Код запчасти",
}, {
	Id:    "availability",
	Label: "Доступное количество",
}, {
	Id:    "make_num",
	Label: "Наименование бренда",
}, {
	Id:    "part_name_rus",
	Label: "Наименование запчасти",
}, {
	Id:    "created_at",
	Label: "Время создания",
}, {
	Id:    "comment",
	Label: "Комментарий",
}}
