package model

import (
	"github.com/dgrijalva/jwt-go"
)

type Token struct {
	AccessToken      string `json:"access_token"`
	RefreshToken     string `json:"refresh_token"`
	ExpiresIn        int    `json:"expires_in"`
	RefreshExpiresIn int    `json:"refresh_expires_in"`
	TokenType        string `json:"token_type"`
}

type jwtCustomClaims struct {
	Login       string `json:"login" db:"login"`
	RoleID      int    `json:"role_id" db:"role_id"`
	PhoneNumber string `json:"phone_number" db:"phone_number"`
	Email       string `json:"email" db:"email"`
	jwt.StandardClaims
}

type TokenRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
