package model

import "time"

type Finance struct {
	Time            time.Time `json:"time" db:"time"`
	TransactionId   int64     `json:"transactionId" db:"transaction_id"`
	Income          float64   `json:"income" db:"income"`
	Outcome         float64   `json:"outcome" db:"outcome"`
	User            string    `json:"user" db:"user"`
	Comment         string    `json:"comment" db:"comment"`
	TransactionType string    `json:"transactionType" db:"transaction_type"`
	PaymentType     string    `json:"paymentType" db:"payment_type"`
	ManagerId       int64     `json:"managerId" db:"manager_id"`
}

type FinancialReport struct {
	Balance float64   `json:"balance"`
	Cashbox float64   `json:"cashbox"`
	Finance []Finance `json:"finance"`
}
type ApproveInvoiceRq struct {
	Id []Approve `json:"ids"`
}
type Approve struct {
	Id            int64 `json:"id"`
	RejectedItems int64 `json:"rejectedItems"`
}
