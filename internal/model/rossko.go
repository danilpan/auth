package model

import "encoding/xml"

type RosskoRq struct {
	XMLName xml.Name `xml:"soapenv:Envelope"`
	Text    string   `xml:",chardata"`
	Soapenv string   `xml:"xmlns:soapenv,attr"`
	Api     string   `xml:"xmlns:api,attr"`
	Header  string   `xml:"soapenv:Header"`
	Body    struct {
		Text      string `xml:",chardata"`
		GetSearch struct {
			Chardata   string `xml:",chardata"`
			KEY1       string `xml:"KEY1"`
			KEY2       string `xml:"KEY2"`
			Text       string `xml:"text"`
			DeliveryID string `xml:"delivery_id"`
		} `xml:"api:GetSearch"`
	} `xml:"soapenv:Body"`
}

type RosskoAddRq struct {
	XMLName xml.Name `xml:"soapenv:Envelope"`
	Text    string   `xml:",chardata"`
	Soapenv string   `xml:"xmlns:soapenv,attr"`
	Api     string   `xml:"xmlns:api,attr"`
	Header  string   `xml:"soapenv:Header"`
	Body    struct {
		Text        string `xml:",chardata"`
		GetCheckout struct {
			Text     string `xml:",chardata"`
			KEY1     string `xml:"api:KEY1"`
			KEY2     string `xml:"api:KEY2"`
			Delivery struct {
				Text       string `xml:",chardata"`
				DeliveryID string `xml:"api:delivery_id"`
			} `xml:"api:delivery"`
			Payment struct {
				Text      string `xml:",chardata"`
				PaymentID string `xml:"api:payment_id"`
			} `xml:"api:payment"`
			Contact struct {
				Text  string `xml:",chardata"`
				Name  string `xml:"api:name"`
				Phone string `xml:"api:phone"`
			} `xml:"api:contact"`
			DeliveryParts string `xml:"api:delivery_parts"`
			PARTS         struct {
				Text string       `xml:",chardata"`
				Part []RosskoPart `xml:"api:Part"`
			} `xml:"api:PARTS"`
		} `xml:"api:GetCheckout"`
	} `xml:"soapenv:Body"`
}

type RosskoPart struct {
	Text       string `xml:",chardata"`
	Partnumber string `xml:"partnumber"`
	Brand      string `xml:"brand"`
	Stock      string `xml:"stock"`
	Count      string `xml:"count"`
}

type RosskoRs struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	SOAPENV string   `xml:"SOAP-ENV,attr"`
	Ns1     string   `xml:"ns1,attr"`
	Body    struct {
		Text              string `xml:",chardata"`
		GetSearchResponse struct {
			Text         string `xml:",chardata"`
			SearchResult struct {
				Chardata  string `xml:",chardata"`
				Success   string `xml:"success"`
				Text      string `xml:"text"`
				PartsList struct {
					Text string `xml:",chardata"`
					Part []struct {
						Text       string `xml:",chardata"`
						Guid       string `xml:"guid"`
						Brand      string `xml:"brand"`
						Partnumber string `xml:"partnumber"`
						Name       string `xml:"name"`
						Crosses    struct {
							Text string `xml:",chardata"`
							Part []struct {
								Text       string `xml:",chardata"`
								Guid       string `xml:"guid"`
								Brand      string `xml:"brand"`
								Partnumber string `xml:"partnumber"`
								Name       string `xml:"name"`
								Stocks     struct {
									Text  string `xml:",chardata"`
									Stock []struct {
										Text          string `xml:",chardata"`
										ID            string `xml:"id"`
										Price         string `xml:"price"`
										Count         string `xml:"count"`
										Multiplicity  string `xml:"multiplicity"`
										Type          string `xml:"type"`
										Delivery      string `xml:"delivery"`
										Extra         string `xml:"extra"`
										Description   string `xml:"description"`
										DeliveryStart string `xml:"deliveryStart"`
										DeliveryEnd   string `xml:"deliveryEnd"`
									} `xml:"stock"`
								} `xml:"stocks"`
							} `xml:"Part"`
						} `xml:"crosses"`
						Stocks struct {
							Text  string `xml:",chardata"`
							Stock []struct {
								Text          string `xml:",chardata"`
								ID            string `xml:"id"`
								Price         string `xml:"price"`
								Count         string `xml:"count"`
								Multiplicity  string `xml:"multiplicity"`
								Type          string `xml:"type"`
								Delivery      string `xml:"delivery"`
								Extra         string `xml:"extra"`
								Description   string `xml:"description"`
								DeliveryStart string `xml:"deliveryStart"`
								DeliveryEnd   string `xml:"deliveryEnd"`
							} `xml:"stock"`
						} `xml:"stocks"`
					} `xml:"Part"`
				} `xml:"PartsList"`
			} `xml:"SearchResult"`
		} `xml:"GetSearchResponse"`
	} `xml:"Body"`
}
