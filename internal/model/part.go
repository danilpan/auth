package model

import (
	"github.com/shopspring/decimal"
	"time"
)

type ExcelPart struct {
	Id        int             `json:"id" db:"id"`
	StockId   int             `json:"stockId" db:"stock_id"`
	Stock     int             `json:"stock" db:"stock"`
	Price     decimal.Decimal `json:"price" db:"price"`
	Name      string          `json:"name" json:"name"`
	Brand     string          `json:"brand" db:"brand"`
	DetailNum string          `json:"detailNum" db:"detail_num"`
}

type LocalStock struct {
	Id   int       `json:"id" db:"id"`
	Name string    `json:"name" db:"name"`
	Date time.Time `json:"date" db:"updated"'`
}

type SearchRs struct {
	Makes []string `json:"makes"`
	Parts []Part   `json:"parts"`
}
