package repository

import (
	"context"
	"database/sql"
	"fmt"
	"processor/internal/model"
	"strconv"
	"time"
)

func (r Repository) GetOrders(userID, roleID int) (*[]model.Order, error) {
	data := []model.Order{}
	query := ""
	role, roleErr := r.GetRoleByID(roleID)
	if roleErr != nil {
		return nil, roleErr
	}
	if role != nil {
		if role.Title == "manager" {
			query = fmt.Sprintf("SELECT * FROM postgres.public.orders WHERE manager_id = %d", userID)
		} else if role.Title == "admin" {
			query = "SELECT * FROM postgres.public.orders order by created_at DESC"
		} else {
			query = fmt.Sprintf("SELECT * FROM postgres.public.orders WHERE manager_id = %d", userID)
		}
	}

	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		query)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		return nil, err
	}
	/*for k, v := range data {
		orderItems, err := r.GetOrderItems(v.ID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].OrderItems = orderItems
	}
	*/
	return &data, nil
}

func (r Repository) GetOrdersByUserID(userID int) (*[]model.Order, error) {
	data := []model.Order{}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		"SELECT * FROM postgres.public.orders WHERE user_id = $1 ORDER BY id DESC",
		userID)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		fmt.Println(err.Error())

		return nil, err
	}
	for _, v := range data {
		orderItems, err := r.GetOrderItems(v.ID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			fmt.Println(err.Error())
			return nil, err
		}
		v.OrderItems = orderItems
	}

	return &data, nil
}

func (r Repository) GetOrdersByManagerID(userID int) (*[]model.Order, error) {
	data := []model.Order{}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		"SELECT * FROM postgres.public.orders WHERE manager_id = $1 ORDER BY id DESC",
		userID)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		fmt.Println(err.Error())

		return nil, err
	}
	for _, v := range data {
		orderItems, err := r.GetOrderItems(v.ID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			fmt.Println(err.Error())
			return nil, err
		}
		v.OrderItems = orderItems
	}

	return &data, nil
}

func (r Repository) GetOrderItems(orderID int) (*[]model.OrderItem, error) {
	data := []model.OrderItem{}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		"SELECT * FROM postgres.public.order_items WHERE order_id = $1 ORDER BY id DESC",
		orderID)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetOrderByID(orderID, userID, roleID int) (*model.Order, error) {
	data := model.Order{}
	query := fmt.Sprintf("SELECT * FROM postgres.public.orders WHERE id = %d and user_id = %d", orderID, userID)
	role, roleErr := r.GetRoleByID(roleID)
	if roleErr != nil {
		return nil, roleErr
	}
	if role.Title == "admin" {
		query = fmt.Sprintf("SELECT * FROM postgres.public.orders WHERE id = %d", orderID)
	}
	err := r.cl.PrimaryPreferred().DBx().Get(
		&data,
		query)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetOrderItemByID(order int) (*model.OrderItem, error) {
	data := model.OrderItem{}
	query := fmt.Sprintf("SELECT * FROM postgres.public.order_items WHERE id = %d", order)
	err := r.cl.PrimaryPreferred().DBx().Get(
		&data,
		query)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetOrderItemsByID(orderID, userID int) ([]model.OrderItems, error) {
	data := []model.OrderItems{}
	query := fmt.Sprintf("SELECT order_id,part_number,brand,reference,quantity,price FROM postgres.public.order_items where order_id=$1")
	rows, err := r.cl.PrimaryPreferred().DBx().Query(
		query, orderID)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	for rows.Next() {
		var f model.OrderItems
		if err := rows.Scan(&f.OrderId, &f.PartNumber, &f.Brand, &f.Reference, &f.Quantity, &f.Price); err != nil {
			return data, fmt.Errorf("GetActivityData: Error scanning activity data -> %v", err)
		}
		h := model.History{
			Date:    time.Now().String(),
			Status:  "создан",
			Comment: "заказ создан",
		}
		f.History = append(f.History, h)
		data = append(data, f)
	}
	return data, nil
}

func (r Repository) GetOrderItemsAll(managerId int, ctx context.Context) ([]model.OrderItems, error) {
	data := []model.OrderItems{}
	query := `SELECT oi.id,
    oi.order_id,
       oi.reference,
       oi.part_number,
       oi.brand,
       oi.quantity,
       oi.price,
       oi.brand,
       oi.status,
       o.manager_id,
       o.user_id
from orders o LEFT OUTER JOIN order_items oi on o.id = oi.order_id WHERE order_id is not null ORDER BY o.id DESC;
`
	rows, err := r.cl.PrimaryPreferred().DBx().
		QueryContext(ctx, query)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var f model.OrderItems
		if err := rows.Scan(&f.Id, &f.OrderId, &f.Reference, &f.PartNumber, &f.Brand, &f.Quantity, &f.Price, &f.Brand, &f.Status, &f.ManagerId, &f.UserId); err != nil {
			return data, fmt.Errorf("GetActivityData: Error scanning activity data -> %v", err)
		}
		data = append(data, f)
	}
	return data, nil
}

func (r Repository) GetOrderItemsAllAdmin(managerId int, partNumber, reference, partName, brand, manager, client, supplier, status string, ctx context.Context) ([]model.OrderItemsV2, error) {
	data := []model.OrderItemsV2{}
	query := `select oi.status,
       oi.id,
       u.username as username,
       oi.reference,
       oi.part_number,
       oi.part_name,
       oi.make_name,
       oi.comment,
       oi.price,
       oi.quantity,
       CAST(oi.quantity as integer) *oi.price as amount,
       to_char(oi.original_price*oi.rate, 'FM99999999999.000') as original_price,
w.username as manager
from order_items oi
         join (select id, user_id, manager_id from orders) o on oi.order_id = o.id
         join (select id, username from users) u on u.id = o.user_id
join (select id, username from users) w on w.id = o.manager_id WHERE 1=1`
	if partNumber != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.part_number ILIKE concat('%', '`, partNumber, `'::varchar, '%')) `)
	}
	if reference != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.reference ILIKE concat('%', '`, reference, `'::varchar, '%')) `)
	}
	if partName != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.part_name ILIKE concat('%', '`, partName, `'::varchar, '%')) `)
	}
	if brand != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.make_name ILIKE concat('%', '`, brand, `'::varchar, '%')) `)
	}
	if manager != "" {
		query += fmt.Sprintf(` AND w.username='%v' `, manager)
	}
	if client != "" {
		query += fmt.Sprintf(` AND u.username='%v' `, client)
	}
	if supplier != "" {
		query += fmt.Sprintf(` AND oi.booking='%v' `, supplier)
	}
	if status != "" {
		query += fmt.Sprintf(` AND oi.status='%v' `, status)
	}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		query)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		return nil, err
	}
	return data, nil
}

func (r Repository) GetOrderItemsAllTable(userId int, ctx context.Context, partNumber, reference, partName, brand, status string) ([]model.OrderItemsTable, error) {
	data := []model.OrderItemsTable{}
	query := `SELECT oi.id,
    oi.order_id,
       oi.reference,
       oi.part_number,
       oi.brand,
       oi.price,
       oi.quantity,
       oi.rejected_items_num,
       oi.status,
       oi.comment,
       oi.stock_name
from orders o LEFT OUTER JOIN order_items oi on o.id = oi.order_id WHERE o.user_id=$1 
`
	if partNumber != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.part_number ILIKE concat('%', '`, partNumber, `'::varchar, '%')) `)
	}
	if reference != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.reference ILIKE concat('%', '`, reference, `'::varchar, '%')) `)
	}
	if partName != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.part_name ILIKE concat('%', '`, partName, `'::varchar, '%')) `)
	}
	if brand != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.make_name ILIKE concat('%', '`, brand, `'::varchar, '%')) `)
	}
	if status != "" {
		query += fmt.Sprintf(` AND oi.status='%v' `, status)
	}
	query += ` AND order_id is not null ORDER BY o.id DESC; `
	rows, err := r.cl.PrimaryPreferred().DBx().
		QueryContext(ctx, query, userId)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var f model.OrderItemsTable
		if err := rows.Scan(&f.Id, &f.OrderId, &f.Reference, &f.PartNumber, &f.Brand, &f.Price, &f.Quantity, &f.Rejected, &f.Status, &f.Comment, &f.Stock); err != nil {
			return data, fmt.Errorf("GetActivityData: Error scanning activity data -> %v", err)
		}
		quan, parseErr := strconv.Atoi(f.Quantity)
		if parseErr != nil {
			return data, fmt.Errorf("GetActivityData: strconv -> %v", parseErr)
		}
		f.Amount = float64(quan) * f.Price
		data = append(data, f)
	}
	return data, nil
}

func (r Repository) GetOrderItemsByManagerId(managerId int, partNumber, reference, partName, brand, manager, client, supplier, status string, ctx context.Context) ([]model.OrderItemsV2, error) {
	data := []model.OrderItemsV2{}
	query := `select oi.status,
       oi.id,
       u.username as username,
       oi.reference,
       oi.part_number,
       oi.part_name,
       oi.make_name,
       oi.comment,
       oi.price,
       oi.quantity,
       CAST(oi.quantity as integer) *oi.price as amount,
       to_char(oi.original_price*oi.rate, 'FM99999999999.000') as original_price,
w.username as manager
from order_items oi
         join (select id, user_id, manager_id from orders) o on oi.order_id = o.id
         join (select id, username from users) u on u.id = o.user_id
join (select id, username from users) w on w.id = o.manager_id where o.manager_id=$1
`
	if partNumber != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.part_number ILIKE concat('%', '`, partNumber, `'::varchar, '%')) `)
	}
	if reference != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.reference ILIKE concat('%', '`, reference, `'::varchar, '%')) `)
	}
	if partName != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.part_name ILIKE concat('%', '`, partName, `'::varchar, '%')) `)
	}
	if brand != "" {
		query += fmt.Sprintf("%v%v%v", ` AND (oi.make_name ILIKE concat('%', '`, brand, `'::varchar, '%')) `)
	}
	if manager != "" {
		query += fmt.Sprintf(` AND w.username='%v' `, manager)
	}
	if client != "" {
		query += fmt.Sprintf(` AND u.username='%v' `, client)
	}
	if supplier != "" {
		query += fmt.Sprintf(` AND oi.booking='%v' `, supplier)
	}
	if status != "" {
		query += fmt.Sprintf(` AND oi.status='%v' `, status)
	}
	query += ` ORDER BY oi.id DESC `
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		query, managerId)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	return data, nil
}

func (r Repository) UpdateOrderItemStatus(status string, orderID int) error {
	query := fmt.Sprintf("UPDATE postgres.public.order_items SET status=$1 where id=$2")
	row, err := r.cl.PrimaryPreferred().DBx().Exec(
		query, status, orderID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		fmt.Println(err.Error())
		return err
	}

	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return nil
}

func (r Repository) UpdateOrderItem(status string, orderID int) error {
	query := fmt.Sprintf("UPDATE postgres.public.order_items SET description=$1 where id=$2")
	row, err := r.cl.PrimaryPreferred().DBx().Exec(
		query, status, orderID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		fmt.Println(err.Error())
		return err
	}

	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return nil
}

func (r Repository) UpdateOrderItemsBatch(ctx context.Context, references []string, status string) error {

	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("create tx: -> %w", err)
	}
	query := "update order_items set status=tmp.status from (values "
	if len(references) > 0 {
		for i, v := range references {
			query += fmt.Sprintf("('%s', '%s')", v, status)
			if i != len(references)-1 {
				query += ","
			}
		}
	}
	query += ") as tmp (reference, status) where order_items.reference = tmp.reference;"
	res, errPC := tx.ExecContext(ctx, query)
	if errPC != nil {
		tx.Rollback()
		return fmt.Errorf("update order items: -> %w", errPC)
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	errC := tx.Commit()
	if errC != nil {
		tx.Rollback()
		return fmt.Errorf("commit tx -> %w", errC)
	}
	return nil

}

func (r Repository) UpdateOrderItemsBatchById(ctx context.Context, references []int64, status string) error {

	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("create tx: -> %w", err)
	}
	query := "update order_items set status=tmp.status from (values "
	if len(references) > 0 {
		for i, v := range references {
			query += fmt.Sprintf("(%v, '%s')", v, status)
			if i != len(references)-1 {
				query += ","
			}
		}
	}
	query += ") as tmp (id, status) where order_items.id = tmp.id;"
	res, errPC := tx.ExecContext(ctx, query)
	if errPC != nil {
		tx.Rollback()
		return fmt.Errorf("update order items: -> %w", errPC)
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	errC := tx.Commit()
	if errC != nil {
		tx.Rollback()
		return fmt.Errorf("commit tx -> %w", errC)
	}
	return nil

}

func (r Repository) UpdateOrderItemsBatchOrders(ctx context.Context, references []model.OrderItemReturn) error {

	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("create tx: -> %w", err)
	}
	query := "update order_items set rejected_items_num=tmp.rejected_items_num from (values "
	if len(references) > 0 {
		for i, v := range references {
			query += fmt.Sprintf("('%s', %d)", v.Reference, v.RejectedItemsNum)
			if i != len(references)-1 {
				query += ","
			}
		}
	}
	query += ") as tmp (reference, rejected_items_num) where order_items.reference = tmp.reference;"
	res, errPC := tx.ExecContext(ctx, query)
	if errPC != nil {
		tx.Rollback()
		return fmt.Errorf("update order items: -> %w", errPC)
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	errC := tx.Commit()
	if errC != nil {
		tx.Rollback()
		return fmt.Errorf("commit tx -> %w", errC)
	}
	return nil

}

func (r Repository) UpdateOrderItemsBatchStatus(ctx context.Context, references []model.ReferenceUpdate) error {

	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("create tx: -> %w", err)
	}
	query := "update order_items set catalog=tmp.catalog from (values "
	if len(references) > 0 {
		for i, v := range references {
			query += fmt.Sprintf("('%s', '%s')", v.Reference, v.StatusEmex)
			if i != len(references)-1 {
				query += ","
			}
		}
	}
	query += ") as tmp (reference, catalog) where order_items.reference = tmp.reference;"
	fmt.Println(query)
	res, errPC := tx.ExecContext(ctx, query)
	if errPC != nil {
		tx.Rollback()
		return fmt.Errorf("update order items: -> %w", errPC)
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	errC := tx.Commit()
	if errC != nil {
		tx.Rollback()
		return fmt.Errorf("commit tx -> %w", errC)
	}
	return nil

}

func (r Repository) GetOrderItemsEmex(ctx context.Context) ([]model.ReferenceUpdate, error) {
	data := []model.ReferenceUpdate{}
	query := `SELECT reference, catalog from order_items where booking='emex';
`
	rows, err := r.cl.PrimaryPreferred().DBx().
		QueryContext(ctx, query)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		fmt.Println(err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var f model.ReferenceUpdate
		if err := rows.Scan(&f.Reference, &f.OldStatus); err != nil {
			return data, fmt.Errorf("GetActivityData: Error scanning activity data -> %v", err)
		}
		data = append(data, f)
	}
	return data, nil
}

func (r Repository) GetOrderItemsArmtek(ctx context.Context) ([]model.ReferenceUpdate, error) {
	data := []model.ReferenceUpdate{}

	query := `SELECT description, catalog FROM order_items WHERE booking='arm';`
	rows, err := r.cl.PrimaryPreferred().DBx().QueryContext(ctx, query)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}

		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var f model.ReferenceUpdate
		if err := rows.Scan(&f.Reference, &f.OldStatus); err != nil {
			return data, fmt.Errorf("GetArmtekOrders: Error scanning armtek data -> %v", err)
		}
		data = append(data, f)
	}
	return data, nil
}

func (r Repository) UpdateArmtekOrderItemsBatchStatus(ctx context.Context, references []model.ReferenceUpdate) error {

	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("create tx: -> %w", err)
	}
	query := "UPDATE order_items SET catalog=tmp.catalog FROM (VALUES "
	if len(references) > 0 {
		for i, v := range references {
			query += fmt.Sprintf("('%s', '%s')", v.Reference, v.Status)
			if i != len(references)-1 {
				query += ","
			}
		}
	}
	query += ") AS tmp (description, catalog) WHERE order_items.reference = tmp.reference;"
	fmt.Println(query)
	res, errPC := tx.ExecContext(ctx, query)
	if errPC != nil {
		tx.Rollback()
		return fmt.Errorf("update armtek order items: -> %w", errPC)
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	errC := tx.Commit()
	if errC != nil {
		tx.Rollback()
		return fmt.Errorf("commit tx -> %w", errC)
	}
	return nil
}
