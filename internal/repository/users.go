package repository

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/shopspring/decimal"
	"processor/internal/model"
	"time"
)

func (r Repository) GetUser(username string) (*model.User, error) {
	data := model.User{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data,
		"SELECT id, username, password, first_name, last_name, city_id, phone_number, role_id, email, partner_id, group_id, is_active FROM users WHERE username = $1", username)
	if err != nil {
		fmt.Println(err.Error())
		return &data, err
	}
	return &data, nil
}

func (r Repository) GetUserInfoByID(userID int) (*model.UserInfo, error) {
	data := model.UserInfo{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data,
		"SELECT id, username, first_name, last_name, phone_number FROM users WHERE id = $1", userID)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

func (r Repository) CreateCustomer(u model.User) (*model.User, error) {
	role, roleErr := r.GetCustomerRole()
	if roleErr != nil {
		return nil, roleErr
	}
	res := r.cl.PrimaryPreferred().DBx().
		QueryRow("INSERT INTO users (username, password, first_name, last_name, phone_number, role_id, email, partner_id, city_id, group_id, documents) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning id, username, email, phone_number, first_name, last_name, created_at;",
			u.Username, u.Password, u.FirstName, u.LastName, u.PhoneNumber, role.ID, u.Email, u.PartnerID, u.CityID, 2, true)
	if res.Err() != nil {
		return nil, res.Err()
	}

	scanErr := res.Scan(&u.ID, &u.Username, &u.Email, &u.PhoneNumber, &u.FirstName, &u.LastName, &u.CreatedAt)
	if scanErr != nil {
		return nil, scanErr
	}

	return &u, nil
}

func (r Repository) CreateUser(u model.User) (*model.User, error) {
	res := r.cl.PrimaryPreferred().DBx().
		QueryRow("INSERT INTO users (username, password, first_name, last_name, phone_number, role_id, email, city_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) returning id",
			u.Username, u.Password, u.FirstName, u.LastName, u.PhoneNumber, u.RoleID, u.Email, u.CityID)
	if res.Err() != nil {
		return nil, res.Err()
	}

	scanErr := res.Scan(&u.ID)
	if scanErr != nil {
		return nil, scanErr
	}

	return &u, nil
}

func (r Repository) GetCustomersList(managerId int64) (*[]model.User, error) {
	users := []model.User{}
	role, roleErr := r.GetCustomerRole()
	if roleErr != nil {
		return nil, roleErr
	}

	err := r.cl.PrimaryPreferred().DBx().Select(&users, "SELECT id, username, first_name, last_name, city_id, phone_number, email, created_at, partner_id, balance, role_id FROM users WHERE role_id = $1 AND manager_id=$2", role.ID, managerId)
	if err != nil {
		return nil, err
	}
	return &users, nil
}

func (r Repository) GetUsersList() (*[]model.User, error) {
	users := []model.User{}

	err := r.cl.PrimaryPreferred().DBx().Select(&users, "SELECT id, username, first_name, last_name, city_id, phone_number, email, created_at, partner_id, balance, role_id FROM users order by id DESC ")
	if err != nil {
		return nil, err
	}
	return &users, nil
}

func (r Repository) GetManagerCustomersList(managerId int) (*[]model.User, error) {
	users := []model.User{}
	err := r.cl.PrimaryPreferred().DBx().Select(&users,
		"SELECT id, username, first_name, last_name, city_id, phone_number, email, created_at, partner_id, balance, role_id FROM users WHERE role_id = (select id from postgres.public.roles where title='customer') and manager_id = $1 order by id DESC",
		managerId)
	if err != nil {
		return nil, err
	}
	return &users, nil
}

func (r Repository) GetUserByID(ctx context.Context, id int) (*model.User, error) {
	data := model.User{}
	err := r.cl.PrimaryPreferred().DBx().GetContext(ctx, &data,
		"SELECT id, username, first_name, last_name, city_id, phone_number, role_id, email, partner_id, balance, created_at, group_id, margin, manager_id, documents FROM users WHERE id = $1", id)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

func (r Repository) UpdateUserPassword(ctx context.Context, id int, newPassword string) error {
	row, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		"UPDATE users SET password=$1 WHERE id = $2",
		newPassword, id)
	if err != nil {
		return err
	}
	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("Error: Rows affected 0 -> %w", err)
	}
	return nil
}

func (r Repository) GetProfileByID(ctx context.Context, id int) (*model.UserProfile, error) {
	data := model.UserProfile{}
	err := r.cl.PrimaryPreferred().DBx().GetContext(ctx, &data,
		"SELECT id, username, first_name, last_name, city_id, phone_number, email, balance, role_id, (select title from roles where id = users.role_id limit 1) as role_name, created_at FROM users WHERE id = $1", id)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

func (r Repository) BalanceReplenishment(ctx context.Context, userID int, sum decimal.Decimal, managerId int) (id int, err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		ExecContext(ctx, "UPDATE users SET balance = (balance + $1) WHERE id = $2", sum, userID)
	if err != nil {
		return 0, err
	}
	t := model.Transaction{
		Amount:        sum,
		UserID:        userID,
		Replenishment: true,
		ManagerID:     managerId,
	}

	return r.CreateTransaction(ctx, t)
}

func (r Repository) ManagerWriteOff(ctx context.Context, userID int, sum decimal.Decimal, managerId, transId int, comment, transType string) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		ExecContext(ctx, "INSERT INTO finance (transaction_id,income,outcome,user_id, comment,transaction_type,manager_id, time)"+
			"VALUES ($1, $2,$3,$4,$5,$6, $7, $8)", transId, 0, sum, userID, comment, transType, managerId, time.Now())

	if err != nil {
		return err
	}
	return nil
}

func (r Repository) ManagerReplenishment(ctx context.Context, userID int, sum decimal.Decimal, managerId, transId int, comment, transType string) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		ExecContext(ctx, "INSERT INTO finance (transaction_id,income,outcome,user_id, comment,transaction_type,manager_id, time)"+
			"VALUES ($1, $2,$3,$4,$5,$6, $7, $8)", transId, sum, 0, userID, comment, transType, managerId, time.Now())

	if err != nil {
		return err
	}
	return nil
}

func (r Repository) Report(ctx context.Context, managerId int, username, since, to string) (finance []model.Finance, err error) {
	query := `SELECT transaction_id,income,outcome,u.username, comment,transaction_type, time, manager_id FROM finance s join (select id,username from users )u on s.user_id=u.id where 1=1 `
	if username != "" {
		fmt.Sprintf("%v AND u.username='%v'", query, username)
	}
	if managerId != 0 {
		fmt.Sprintf("%v AND manager_id=%v ", query, managerId)
	}
	fmt.Sprintf("%v AND time between '%v' and '%v'", query, since, to)
	rows, err := r.cl.PrimaryPreferred().DBx().
		QueryContext(ctx, query)

	defer rows.Close()
	for rows.Next() {
		var f model.Finance
		if err := rows.Scan(&f.TransactionId, &f.Income, &f.Outcome, &f.User, &f.Comment, &f.TransactionType, &f.Time, &f.ManagerId); err != nil {
			return finance, fmt.Errorf("GetActivityData: Error scanning activity data -> %v", err)
		}
		finance = append(finance, f)
	}
	return finance, nil
}

func (r Repository) BalanceWriteOff(ctx context.Context, userID int, sum decimal.Decimal, managerId int) (id int, err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		ExecContext(ctx, "UPDATE users SET balance = (balance - $1) WHERE id = $2", sum, userID)
	if err != nil {
		return 0, err
	}
	t := model.Transaction{
		Amount:    sum,
		UserID:    userID,
		WriteOff:  true,
		ManagerID: managerId,
	}

	return r.CreateTransaction(ctx, t)
}

func (r Repository) UpdateUserInfoByID(ctx context.Context, id int, u model.UpdateUserProfileRequest) error {
	row, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		"UPDATE users SET first_name=$1, last_name=$2, phone_number=$3, email=$4 WHERE id = $5",
		u.FirstName, u.LastName, u.PhoneNumber, u.Email, id)
	if err != nil {
		return err
	}
	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("Error: Rows affected Delete docs -> %w", err)
	}
	return nil
}

func (r Repository) UpdateUserByID(ctx context.Context, id int, u model.UpdateUserRequest) error {
	row, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		"UPDATE users SET first_name=$1, last_name=$2, phone_number=$3, email=$4, group_id=$5, margin=$6, city_id=$7, role_id=$8, manager_id=$9, documents=$10 WHERE id = $11",
		u.FirstName, u.LastName, u.PhoneNumber, u.Email, u.GroupID, u.Margin, u.CityID, u.RoleID, u.ManagerID, u.Document, id)
	if err != nil {
		return err
	}
	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("Error: Rows affected 0: %w", err)
	}
	return nil
}

func (r Repository) CreateTransaction(ctx context.Context, t model.Transaction) (int, error) {
	var id int
	err := r.cl.PrimaryPreferred().DBx().QueryRowContext(ctx,
		"INSERT INTO transactions(amount, user_id, write_off, replenishment, manager_id) VALUES ($1, $2, $3, $4, $5) returning id",
		t.Amount, t.UserID, t.WriteOff, t.Replenishment, t.ManagerID).Scan(&id)
	if err != nil {
		return id, err
	}
	return id, nil
}

func (r Repository) GetRolesList() (*[]model.Role, error) {
	data := []model.Role{}
	err := r.cl.PrimaryPreferred().DBx().Select(&data, "SELECT id, title FROM roles")
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetRoleByID(roleID int) (*model.Role, error) {
	data := model.Role{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data, "SELECT id, title FROM roles where id = $1", roleID)
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetAdminRole() (*model.Role, error) {
	data := model.Role{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data, "SELECT id, title FROM roles where title = 'admin'")
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetCustomerRole() (*model.Role, error) {
	data := model.Role{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data, "SELECT id, title FROM roles where title = 'customer'")
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetManagerRole() (*model.Role, error) {
	data := model.Role{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data, "SELECT id, title FROM roles where title = 'manager'")
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetKassaRole() (*model.Role, error) {
	data := model.Role{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data, "SELECT id, title FROM roles where title = 'kassa'")
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetManagersList() (*[]model.User, error) {
	data := []model.User{}
	err := r.cl.PrimaryPreferred().DBx().Select(&data, "SELECT id, first_name, last_name, username FROM users WHERE role_id = (SELECT id from roles where title = 'manager' limit 1)")
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetUsersWithCarts() (*[]model.User, error) {
	data := []model.User{}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		"SELECT id, username, phone_number, first_name, last_name, email, manager_id FROM users WHERE id in (SELECT distinct user_id from postgres.public.cart_items where confirmed = false)")
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetUserWithCarts(userID int) (*model.User, error) {
	data := model.User{}
	err := r.cl.PrimaryPreferred().DBx().Get(
		&data,
		"SELECT id, username, phone_number, first_name, last_name, email FROM users WHERE id = $1", userID)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetUserMargins(userID int) (model.Margin, error) {
	margin := model.Margin{}
	query := `SELECT s.margin as user_margin, g.margin as group_margin, g.emex as emex, g.emexru as emexru, g.local as local
from users s
         JOIN (select id, margin, emex, emexru, local from groups) g ON g.id = s.group_id
where s.id = $1`
	err := r.cl.PrimaryPreferred().DBx().Get(
		&margin,
		query, userID)
	if err != nil && err != sql.ErrNoRows {
		if err == sql.ErrNoRows {
			return margin, nil
		}
		return margin, err
	}
	return margin, nil
}

func (r Repository) IdByUser(user string) (int, error) {
	var id int
	row := r.cl.PrimaryPreferred().DBx().QueryRow(
		"SELECT id from users WHERE username=$1",
		user)
	row.Scan(&id)
	return id, nil
}

func (r Repository) SetVerificationCodeByUserID(ctx context.Context, userId int, verificationCode string) error {
	row, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		"UPDATE users SET verification_code=$1 WHERE id = $2",
		verificationCode, userId)
	if err != nil {
		return err
	}
	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("Error: Rows affected 0 -> %w", err)
	}
	return nil
}

func (r Repository) GetUserVerifyActivationCode(ctx context.Context, userId int) (string, error) {
	var code string
	row := r.cl.PrimaryPreferred().DBx().QueryRowContext(
		ctx,
		"SELECT verification_code from users WHERE id=$1",
		userId)
	if scanErr := row.Scan(&code); scanErr != nil {
		return "", scanErr
	}

	return code, nil
}

func (r Repository) ActivateUser(ctx context.Context, userId int, isActive bool) error {
	row, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		"UPDATE users SET is_active=$1 WHERE id = $2",
		isActive, userId)
	if err != nil {
		return err
	}
	a, _ := row.RowsAffected()
	if a == 0 {
		return fmt.Errorf("Error: Rows affected 0 -> %w", err)
	}
	return nil
}
