package repository

import (
	"context"
	"processor/internal/model"
)

func (r Repository) GetCityList(ctx context.Context) (data []model.City, err error) {
	err = r.cl.PrimaryPreferred().DBx().SelectContext(ctx, &data, "SELECT id, name_ru, name_en, name_kk from city")
	if err != nil {
		return data, err
	}
	return data, nil
}
