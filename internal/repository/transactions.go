package repository

import (
	"database/sql"
	"github.com/lib/pq"
	"processor/internal/model"
	"time"
)

func (r Repository) GetUserTransactions(userID int) (*[]model.Transaction, error) {
	data := []model.Transaction{}
	err := r.cl.PrimaryPreferred().DBx().Select(&data, "SELECT id, amount, user_id, created_at, write_off, replenishment, manager_id from postgres.public.transactions where user_id = $1", userID)
	if err != nil {
		return &data, err
	}
	for k, v := range data {
		manager, err := r.GetUserInfoByID(v.ManagerID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].Manager = manager
	}
	return &data, nil
}
func (r Repository) GetUserTransactionsExcel(userID int, from, to string) (*[]model.Transaction, error) {
	data := []model.Transaction{}
	dateFrom, _ := time.Parse("2006-01-02", from)
	dateTo, _ := time.Parse("2006-01-02", to)
	filter := " "
	query := "SELECT id, amount, user_id, created_at, write_off, replenishment, manager_id from postgres.public.transactions where user_id = $1"
	if from != "" {
		filter += " AND created_at>='" + string(pq.FormatTimestamp(dateFrom)) + "'"
	}
	if to != "" {
		filter += " AND created_at<='" + string(pq.FormatTimestamp(dateTo)) + "'"
	}
	err := r.cl.PrimaryPreferred().DBx().Select(&data, query+filter, userID)
	if err != nil {
		return &data, err
	}
	for k, v := range data {
		manager, err := r.GetUserInfoByID(v.ManagerID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		user, err := r.GetUserInfoByID(v.UserID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].Manager = manager
		data[k].User = user
	}
	return &data, nil
}
func (r Repository) GetManagerTransactions(userID int) (*[]model.Transaction, error) {
	data := []model.Transaction{}
	err := r.cl.PrimaryPreferred().DBx().Select(&data, "SELECT id, amount, user_id, created_at, write_off, replenishment, manager_id from postgres.public.transactions where manager_id = $1", userID)
	if err != nil {
		return &data, err
	}
	for k, v := range data {
		user, err := r.GetUserInfoByID(v.UserID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].User = user
	}
	return &data, nil
}

func (r Repository) GetAllTransactions() (*[]model.Transaction, error) {
	data := []model.Transaction{}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		"SELECT id, amount, user_id, created_at, write_off, replenishment, manager_id from postgres.public.transactions order by id DESC")
	if err != nil {
		return &data, err
	}
	for k, v := range data {
		user, err := r.GetUserInfoByID(v.UserID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].User = user
		manager, err := r.GetUserInfoByID(v.ManagerID)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].Manager = manager
	}
	return &data, nil
}

func (r Repository) GetDebtors(managerId int) (*[]model.Debtor, error) {
	data := []model.Debtor{}
	err := r.cl.PrimaryPreferred().DBx().Select(
		&data,
		"select user_id,sum(income)-sum(outcome) as total from finance where manager_id = $1 group by user_id ;", managerId)
	if err != nil {
		return &data, err
	}
	for k, v := range data {
		user, err := r.GetUserInfoByID(v.Id)
		if err != nil {
			if err == sql.ErrNoRows {
				continue
			}
			return nil, err
		}
		data[k].Name = user.Username
	}
	return &data, nil
}
