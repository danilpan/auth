package repository

import (
	"fmt"
	"github.com/lib/pq"
	"golang.org/x/net/context"
	"log"
	"processor/internal/model"
)

func (r Repository) CreateInvoice(invoice model.Invoice, position []model.InvoiceItem, ctx context.Context) (err error) {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	row := tx.QueryRowContext(ctx, "INSERT INTO invoice (invoice_num, status, date, shipper, recipient, total, manager) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id",
		invoice.InvoiceNum, invoice.Status, invoice.Date, invoice.Shipper, invoice.Recipient, invoice.Total, invoice.Manager)
	err = row.Scan(&invoice.Id)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("CreateInvoice: failed to create invoice -> %w", err)
	}
	for i, _ := range position {
		position[i].InvoiceId = invoice.Id
	}
	log.Println(fmt.Sprintf("invoice created id %d", invoice.Id))
	//var wg sync.WaitGroup
	//var mutex sync.Mutex
	//sem := semaphore.NewWeighted(50)
	//for _, part := range position {
	//	wg.Add(1)
	//	go func(part model.InvoiceItem, ctx context.Context) {
	//		sem.Acquire(ctx, 1)
	//		defer sem.Release(1)
	//		defer wg.Done()
	//		_, errEC := tx.ExecContext(ctx, "INSERT INTO public.invoice_positions ( invoice_id, box, brand, part_number, reference, part_name, part_total, quantity, price, amount, weight, part_name_en, origin_country, brand_name, sub_id, portion, barcode, sub_customer_price, hs_code, cross_brand, cross_number, login, reference_num, rejected_items_num) VALUES ( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24)",
	//			invoice.Id, part.Box, part.Brand, part.PartNumber, part.Reference, part.PartName, part.PartTotal, part.Quantity, part.Price, part.Amount, part.Weight, part.PartNameEn, part.OriginCountry, part.BrandName, part.SubId, part.Portion, part.Barcode, part.SubCustomerPrice, part.HsCode, part.CrossBrand, part.CrossNumber, part.Login, part.ReferenceNum, part.RejectedItems)
	//		if errEC != nil {
	//			tx.Rollback()
	//			log.Println(fmt.Sprintf("CreateInvoice: failed to create invoice position -> %w", errEC))
	//		}
	//		mutex.Lock()
	//
	//		mutex.Unlock()
	//	}(part, ctx)
	//}
	res, errN := tx.NamedExecContext(ctx, "INSERT INTO public.invoice_positions ( invoice_id, box, brand, part_number, reference, part_name, part_total, quantity, price, amount, weight, part_name_en, origin_country, brand_name, sub_id, portion, barcode, sub_customer_price, hs_code, cross_brand, cross_number, login, reference_num, rejected_items_num) VALUES (:invoice_id, :box, :brand, :part_number, :reference, :part_name, :part_total, :quantity, :price, :amount, :weight, :part_name_en, :origin_country, :brand_name, :sub_id, :portion, :barcode, :sub_customer_price, :hs_code, :cross_brand, :cross_number, :login, :reference_num, :rejected_items_num)", position)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("CreateInvoice: failed to create invoice positions-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("CreateInvoice: failed to create invoice positions-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return nil
}

func (r Repository) GetUserInvoices(ctx context.Context, login string) ([]model.InvoiceUser, error) {
	rs := []model.InvoiceUser{}
	query := "select id,       s.invoice_num,       date from invoice s  JOIN (select invoice_id, login from invoice_positions) o ON s.id = o.invoice_id where o.login =$1"

	errQ := r.cl.PrimaryPreferred().DBx().SelectContext(ctx, &rs, query, login)
	if errQ != nil {
		log.Println(errQ)
		return rs, errQ
	}

	return rs, nil
}

func (r Repository) GetUserInvoice(ctx context.Context, login string, id int64) ([]model.InvoiceItemUser, error) {
	rs := []model.InvoiceItemUser{}
	query := `select invoice_id,
       box,
       s.brand,
       s.part_number,
       s.part_name,
       s.quantity,
       o.price,
       origin_country,
       brand_name,
       barcode,
       hs_code,
       cross_brand,
       cross_number,
       login,
       reference_num
from invoice_positions s
         left JOIN order_items as o ON s.reference_num = o.reference
where s.login = $1
  AND s.invoice_id = $2`

	errQ := r.cl.PrimaryPreferred().DBx().SelectContext(ctx, &rs, query, login, id)
	if errQ != nil {
		log.Println(errQ)
		return rs, errQ
	}

	return rs, nil
}

func (r Repository) GetInvoices(shipper, recipient, id, status string, ctx context.Context) (*[]model.Invoice, error) {
	rs := []model.Invoice{}
	query := "SELECT * from invoice"
	filter := ""

	if shipper != "" || recipient != "" || id != "" || status != "" {
		filter += " WHERE id!=0"
	}
	if shipper != "" {
		filter += " AND shipper='" + shipper + "'"
	}
	if recipient != "" {
		filter += " AND recipient='" + recipient + "'"
	}
	if id != "" {

		filter += " AND id=" + id
	}
	if status != "" {
		filter += " AND status='" + status + "'"
	}
	errQ := r.cl.PrimaryPreferred().DBx().SelectContext(ctx, &rs, query+filter)
	if errQ != nil {
		log.Println(errQ)
		return &rs, errQ
	}
	return &rs, nil
}

func (r Repository) GetInvoice(id int, ctx context.Context) (*[]model.InvoiceItem, error) {
	rs := []model.InvoiceItem{}
	query := "SELECT * from invoice_positions WHERE invoice_id=$1"

	errQ := r.cl.PrimaryPreferred().DBx().SelectContext(ctx, &rs, query, id)
	if errQ != nil {
		log.Println(errQ)
		return &rs, errQ
	}
	return &rs, nil
}

func (r Repository) ApproveInvoice(ctx context.Context, ids model.ApproveInvoiceRq) error {

	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("create tx: -> %w", err)
	}
	query := "update invoice_positions set approved=tmp.approved,rejected_items_num=tmp.rejected_items_num from (values "
	if len(ids.Id) > 0 {
		for i, v := range ids.Id {
			query += fmt.Sprintf("(%d, true, %d)", v.Id, v.RejectedItems)
			if i != len(ids.Id)-1 {
				query += ","
			}
		}
	}
	query += ") as tmp (id, approved, rejected_items_num) where invoice_positions.id = tmp.id;"
	res, errPC := tx.ExecContext(ctx, query)
	if errPC != nil {
		tx.Rollback()
		return fmt.Errorf("update accrual: -> %w", errPC)
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	errC := tx.Commit()
	if errC != nil {
		tx.Rollback()
		return fmt.Errorf("commit tx -> %w", errC)
	}
	return nil

}

func (r Repository) GetInvoiceReferencesByID(ctx context.Context, id []int64) ([]model.InvoiceItem, error) {
	rs := []model.InvoiceItem{}
	query := "select id,reference_num from invoice_positions where id = ANY ($1);"

	errQ := r.cl.PrimaryPreferred().DBx().SelectContext(ctx, &rs, query, pq.Array(id))
	if errQ != nil {
		log.Println(errQ)
		return rs, errQ
	}
	return rs, nil
}
