package repository

import (
	"github.com/jmoiron/sqlx"
	hasql "golang.yandex/hasql/sqlx"
)

type Repository struct {
	cl *hasql.Cluster
}

func NewRepository(cl *hasql.Cluster) *Repository {
	return &Repository{cl: cl}
}

func InitDB(name, url string) (*sqlx.DB, error) {
	database, err := sqlx.Connect(name, url)
	if err != nil {
		return nil, err
	}
	if pgErr := database.Ping(); pgErr != nil {
		return nil, pgErr
	}
	return database, nil
}
