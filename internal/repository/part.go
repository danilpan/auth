package repository

import (
	"context"
	"fmt"
	"github.com/labstack/gommon/log"
	"golang.org/x/sync/semaphore"
	"processor/internal/model"
	"sync"
	"time"
)

func (r Repository) CreatePart(parts []model.ExcelPart, ctx context.Context) (err error) {
	var wg sync.WaitGroup
	var mutex sync.Mutex
	sem := semaphore.NewWeighted(50)
	for _, part := range parts {
		fmt.Println(part)
		wg.Add(1)
		go func(part model.ExcelPart) {
			sem.Acquire(ctx, 1)
			defer sem.Release(1)
			defer wg.Done()
			_, err = r.cl.PrimaryPreferred().DBx().
				Exec("INSERT INTO parts ( brand, name, detail_num, stock, price, stock_id) VALUES ($1, $2, $3, $4, $5, $6)",
					part.Brand, part.Name, part.DetailNum, part.Stock, part.Price, part.StockId)
			if err != nil {
				log.Fatal(err)
			}
			mutex.Lock()

			mutex.Unlock()
		}(part)
	}
	wg.Wait()

	return nil
}

func (r Repository) CreateStock(name string, ctx context.Context) (id int, err error) {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return id, err
	}
	row := tx.QueryRowContext(ctx, "INSERT INTO local_stock (name, updated) VALUES ($1, $2) RETURNING id",
		name, time.Now().UTC())
	err = row.Scan(&id)
	if err != nil {
		tx.Rollback()
		return id, fmt.Errorf("CreateInvoice: failed to create invoice -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return id, fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return id, nil
}

func (r Repository) GetStock(ctx context.Context) (rs []model.LocalStock, err error) {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return rs, err
	}
	errS := tx.SelectContext(ctx, &rs, "select id, name , updated from local_stock")
	if errS != nil {
		tx.Rollback()
		return rs, fmt.Errorf("CreateInvoice: failed to create invoice -> %w", errS)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return rs, fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return rs, nil
}

func (r Repository) GetStockById(ctx context.Context, id string) (rs []model.ExcelPart, err error) {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return rs, err
	}
	errS := tx.SelectContext(ctx, &rs, "select id, name , brand,detail_num,price, stock from parts where stock_id=$1", id)
	if errS != nil {
		tx.Rollback()
		return rs, fmt.Errorf("CreateInvoice: failed to create invoice -> %w", errS)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return rs, fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return rs, nil
}

func (r Repository) GetParts(part string, ctx context.Context) (parts []model.ExcelPart, err error) {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return parts, err
	}
	errS := tx.SelectContext(ctx, &parts, "select id, name , brand,detail_num,price, stock from parts where part=$1", part)
	if errS != nil {
		tx.Rollback()
		return parts, fmt.Errorf("CreateInvoice: failed to create invoice -> %w", errS)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return parts, fmt.Errorf("GetPointsData: failed to get points data -> %w", err)
	}
	return parts, nil
}
