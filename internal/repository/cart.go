package repository

import (
	"database/sql"
	"github.com/shopspring/decimal"
	"processor/internal/model"
)

func (r Repository) GetCartItems(userID int) ([]model.CartItem, error) {
	data := []model.CartItem{}
	err := r.cl.PrimaryPreferred().DBx().Select(&data, "SELECT * FROM cart_items WHERE user_id = $1 AND confirmed = FALSE order by id DESC", userID)
	if err != nil {
		if err == sql.ErrNoRows {
			return data, nil
		}
		return data, err
	}
	return data, nil
}

func (r Repository) AddCartItem(i model.AddCartItem) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		Exec(
			"INSERT INTO cart_items (part_number, brand, booking, delivery, destination, transport, quantity, price, currency, reference, order_id, confirmed, user_id, make_logo,price_logo, weight, origin, part_id, availability, make_name, part_name_rus, comment, volume_add, original) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,$15,$16,$17, $18, $19, $20, $21, $22, $23, $24)",
			i.PartNumber, i.Brand, i.Booking, i.Delivery, i.Destination, i.Transport, i.Quantity, i.Price, i.Currency, i.Reference, i.OrderID, i.Confirmed, i.UserID, i.MakeLogo, i.PriceLogo, i.Weight, i.Origin, i.PartID, i.Availability, i.MakeName, i.PartNameRus, i.Comment, i.VolumeAdd, i.Original)
	if err != nil {
		return err
	}

	return nil
}

func (r Repository) CartItemUpdateQuantity(data model.CartItemQuantity) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		Exec(
			"UPDATE cart_items SET quantity = $1 WHERE id = $2",
			data.Quantity, data.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r Repository) ConfirmCartItem(userID int, cartItemID int) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		Exec("UPDATE cart_items SET confirmed = true WHERE id = $1 and user_id = $2", cartItemID, userID)
	if err != nil {
		return err
	}
	return nil
}

func (r Repository) DeleteCartItem(userID int, itemId string) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		Exec("DELETE FROM cart_items WHERE id = $1 AND user_id = $2", itemId, userID)
	if err != nil {
		return err
	}
	return nil
}

func (r Repository) CreateOrder(i model.Order) (id int, err error) {
	row := r.cl.PrimaryPreferred().DBx().
		QueryRow("INSERT INTO orders (user_id, status_id, is_paid, manager_id, amount) VALUES ($1, $2, $3, $4, $5) RETURNING id",
			i.UserID, i.StatusID, i.IsPaid, i.ManagerID, i.Amount)
	if row.Err() != nil {
		return 0, row.Err()
	}

	err = row.Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r Repository) CreateOrderItem(i *model.OrderItem) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		Exec("INSERT INTO order_items (part_number, brand, weight_kg, volume_kg, description, booking, available, quantity, price, currency, days, delivery_cost, delivery, destination, transport, tariff, reference, status, catalog, stock, order_id, comment, stock_name, rejected_items_num, rate, original_price, part_name, make_name) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25,$26, $27, $28)",
			i.PartNumber, i.Brand, i.WeightKg, i.VolumeKg, i.Description, i.Booking, i.Available, i.Quantity, i.Price, i.Currency, i.Days, i.DeliveryCost, i.Delivery, i.Destination, i.Transport, i.Tariff, i.Reference, i.Excess, i.Catalog, i.Stock, i.OrderID, i.Comment, i.StockName, i.RejectedItemsNum, i.Rate, i.OriginalPrice, i.PartName, i.MakeName)
	if err != nil {
		return err
	}

	return nil
}

func (r Repository) UpdateOrder(o model.Order) (err error) {
	_, err = r.cl.PrimaryPreferred().DBx().
		Exec("UPDATE orders SET is_paid = $1, status_id = $2 ,true WHERE id = $3", o.IsPaid, o.StatusID, o.ID)
	if err != nil {
		return err
	}
	return nil
}

func (r Repository) GetCartItem(id int) (*model.CartItem, error) {
	data := model.CartItem{}
	err := r.cl.PrimaryPreferred().DBx().Get(&data, "SELECT * FROM cart_items WHERE id = $1", id)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		return nil, err
	}
	return &data, nil
}

func (r Repository) GetUsersCarts(users *[]model.User) (*[]model.UserCart, error) {
	data := []model.UserCart{}
	for _, v := range *users {
		cart, cartErr := r.GetCartItems(v.ID)
		if cartErr != nil {
			return &data, cartErr
		}
		var totalSum decimal.Decimal
		for _, v := range cart {
			vSum, _ := decimal.NewFromString(v.Price)
			totalSum.Add(vSum)
		}
		temp := model.UserCart{}
		temp.User = v
		if managerID := 0; v.ManagerID == nil {
			temp.ManagerID = managerID
		}
		temp.CartItems = cart
		temp.UpdatedAt = cart[0].CreatedAt
		temp.TotalSum = totalSum.String()
		data = append(data, temp)
	}
	return &data, nil
}

func (r Repository) GetUsersCartsWithoutItems(users *[]model.User) (*[]model.UserCart, error) {
	data := []model.UserCart{}
	for _, v := range *users {
		cart, cartErr := r.GetCartItems(v.ID)
		if cartErr != nil {
			return &data, cartErr
		}
		var totalSum decimal.Decimal
		for _, v := range cart {
			vSum, _ := decimal.NewFromString(v.Price)
			totalSum.Add(vSum)
		}
		temp := model.UserCart{}
		temp.User = v

		if managerID := 0; v.ManagerID == nil {
			temp.ManagerID = managerID
		} else {
			temp.ManagerID = *v.ManagerID
		}
		temp.UpdatedAt = cart[0].CreatedAt
		temp.TotalSum = totalSum.String()
		temp.UserID = v.ID
		temp.UserEmail = v.Email
		data = append(data, temp)
	}
	return &data, nil
}

func (r Repository) GetUserCarts(user *model.User) (*model.UserCart, error) {
	data := model.UserCart{}
	cart, cartErr := r.GetCartItems(user.ID)
	if cartErr != nil {
		return &data, cartErr
	}
	var totalSum decimal.Decimal
	for _, v := range cart {
		vSum, _ := decimal.NewFromString(v.Price)
		totalSum.Add(vSum)
	}
	data.User = *user
	if managerID := 0; user.ManagerID == nil {
		data.ManagerID = managerID
	}
	data.CartItems = cart
	data.UpdatedAt = cart[0].CreatedAt
	data.TotalSum = totalSum.String()

	return &data, nil
}

func (r Repository) GetUserCartItemsTable(user *model.User) (*[]model.CartItem, error) {
	cart, cartErr := r.GetCartItems(user.ID)
	if cartErr != nil {
		return nil, cartErr
	}

	return &cart, nil
}
