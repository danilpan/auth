package repository

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/shopspring/decimal"
	"log"
	"processor/internal/model"
	"time"
)

func (r Repository) GetVolumeMargins(ctx context.Context) (*[]model.Volume, error) {
	data := []model.Volume{}
	query := `select id,min,max,margin from volume_margin`
	err := r.cl.PrimaryPreferred().DBx().SelectContext(ctx,
		&data,
		query)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

func (r Repository) PatchVolumeMargin(ctx context.Context, rq model.Volume) error {
	query := `update volume_margin set min=$1, max=$2, margin=$3 where id=$4`
	result, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		query, rq.Min, rq.Max, rq.Margin, rq.Id)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
		return err
	}

	return nil
}

func (r Repository) AddVolumeMargins(ctx context.Context, rq []model.Volume) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "INSERT INTO volume_margin (id,min,max,margin) VALUES (:id, :min, :max, :margin)", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

func (r Repository) DeleteVolumeMargins(ctx context.Context, rq model.Volume) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "DELETE from volume_margin where id=:id", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

//KeyWord

func (r Repository) GetKeyWordMargins(ctx context.Context) (*[]model.KeyWord, error) {
	data := []model.KeyWord{}
	query := `select id,keyword,margin from keyword_margin`
	err := r.cl.PrimaryPreferred().DBx().SelectContext(ctx,
		&data,
		query)
	if err != nil {
		if err == sql.ErrNoRows {
			return &data, nil
		}
		return &data, err
	}
	return &data, nil
}

func (r Repository) PatchKeyWordMargin(ctx context.Context, rq model.KeyWord) error {
	query := `update keyword_margin set keyword=$1, margin=$2 where id=$3`
	result, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		query, rq.Word, rq.Margin, rq.Id)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
		return err
	}

	return nil
}

func (r Repository) AddKeyWordMargins(ctx context.Context, rq []model.KeyWord) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "INSERT INTO keyword_margin (id,keyword,margin) VALUES (:id, :keyword, :margin)", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

func (r Repository) DeleteKeyWordMargins(ctx context.Context, rq model.KeyWord) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "DELETE from keyword_margin where id=:id", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

//group

func (r Repository) GetGroupMargins(ctx context.Context) (*[]model.MarginTable, error) {
	data := []model.MarginTable{}
	query := `select id,title,margin, emex, emexru, local from groups`
	err := r.cl.PrimaryPreferred().DBx().SelectContext(ctx,
		&data,
		query)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

func (r Repository) PatchGroupMargin(ctx context.Context, rq model.MarginTable) error {
	query := `update groups set title=$1, margin=$2,emex=$3, emexru=$4, local=$5 where id=$6`
	result, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		query, rq.Title, rq.Margin, rq.Emex, rq.EmexRu, rq.Local, rq.Id)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
		return err
	}

	return nil
}

func (r Repository) AddGroupMargins(ctx context.Context, rq []model.MarginTable) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "INSERT INTO groups (title,margin, emex, emexru, local) VALUES (:title,:margin, :emex, :emexru, :local)", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

func (r Repository) DeleteGroupMargins(ctx context.Context, rq model.MarginTable) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "DELETE from groups where id=:id", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

func (r Repository) GetVolumeMarginByVolume(ctx context.Context, volume decimal.Decimal) (decimal.Decimal, error) {
	data := decimal.Decimal{}
	query := `select margin from volume_margin where $1 between min and max`
	err := r.cl.PrimaryPreferred().DBx().GetContext(ctx,
		&data,
		query, volume)
	if err != nil {
		if err == sql.ErrNoRows {
			return decimal.Zero, nil
		}
		return data, err
	}
	return data, nil
}

func (r Repository) GetCurrencies(ctx context.Context) (*[]model.Currency, error) {
	data := []model.Currency{}
	query := `select id,title,value, date, is_automatic from currency`
	err := r.cl.PrimaryPreferred().DBx().SelectContext(ctx,
		&data,
		query)
	if err != nil {
		return &data, err
	}
	return &data, nil
}

func (r Repository) PatchCurrency(ctx context.Context, rq model.Currency) error {
	query := `update currency set title=$1, value=$2,date=$3, is_automatic=$4 where id=$5`
	result, err := r.cl.PrimaryPreferred().DBx().ExecContext(ctx,
		query, rq.Title, rq.Value, time.Now().Format("2006-02-01 15:04:05"), rq.IsAuto, rq.Id)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
		return err
	}

	return nil
}

func (r Repository) AddCurrency(ctx context.Context, rq []model.Currency) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	for i, _ := range rq {
		rq[i].Date = time.Now().Format("2006-01-02 15:04:05")
	}
	res, errN := tx.NamedExecContext(ctx, "INSERT INTO currency (title,value, date, is_automatic) VALUES (:title,:value, :date, :is_automatic)", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}

func (r Repository) DeleteCurrency(ctx context.Context, rq model.Currency) error {
	tx, err := r.cl.PrimaryPreferred().DBx().Beginx()
	if err != nil {
		return err
	}
	res, errN := tx.NamedExecContext(ctx, "DELETE from currency where id=:id", rq)
	if errN != nil {
		tx.Rollback()
		log.Println(fmt.Sprintf("create volume margin-> %v", errN))
		return fmt.Errorf(fmt.Sprintf("create volume margin-> %v", errN))
	}
	a, _ := res.RowsAffected()
	if a == 0 {
		tx.Rollback()
		return fmt.Errorf("update row affected 0 -> %w", err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("commit failed -> %w", err)
	}

	return nil
}
