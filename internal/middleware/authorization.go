package middleware

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gitlab.com/danilpan/logger"
	"net/http"
	"processor/internal/config"
	"processor/internal/model"
	"processor/internal/repository"
	"processor/internal/service"
	"strings"
)

const secret = "secret"

type authMw struct {
	logger       *logger.Logger
	cfg          *config.Config
	repo         *repository.Repository
	excludedUrls map[string]interface{}
}

func NewAuthMiddleware(cLogger *logger.Logger, cfg *config.Config, repo *repository.Repository, excludeUrls map[string]interface{}) *authMw {
	return &authMw{
		logger:       cLogger,
		cfg:          cfg,
		repo:         repo,
		excludedUrls: excludeUrls,
	}
}

func (amw authMw) ValidateToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		req := c.Request()
		if req.Method == http.MethodOptions {
			return next(c)
		}
		ctx := req.Context()

		if _, ok := amw.excludedUrls[req.RequestURI]; ok {
			return next(c)
		}
		authHeader := req.Header.Get("Authorization")

		if authHeader == "" {
			msg := "token is empty"
			amw.logger.WarnCtx(ctx, msg)
			return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: msg})
		}

		token := strings.TrimPrefix(authHeader, "Bearer ")

		claims, err := service.ValidateToken(token)
		if err != nil {
			amw.logger.WarnCtx(ctx, fmt.Sprintf("validation error: %s", err.Error()))
			return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: err.Error()})
		}

		c.Set("claims", claims)
		amw.logger.InfoCtx(ctx, fmt.Sprintf("token validation success: %s", claims.Username))

		return next(c)
	}
}

func (amw authMw) AdminRole(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		claims := c.Get("claims").(*service.JWTClaim)
		admRole, admErr := amw.repo.GetAdminRole()
		if admErr != nil {
			return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: "admin role not found"})
		}
		if claims.RoleID == admRole.ID {
			return next(c)
		}

		return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: "no access"})
	}
}

func (amw authMw) ManagerRole(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		claims := c.Get("claims").(*service.JWTClaim)
		admRole, admErr := amw.repo.GetManagerRole()
		if admErr != nil {
			return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: "admin role not found"})
		}
		if claims.RoleID == admRole.ID {
			return next(c)
		}

		return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: "no access"})
	}
}

func (amw authMw) KassaRole(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		claims := c.Get("claims").(*service.JWTClaim)
		admRole, admErr := amw.repo.GetKassaRole()
		if admErr != nil {
			return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: "admin role not found"})
		}
		if claims.RoleID == admRole.ID {
			return next(c)
		}

		return c.JSON(http.StatusUnauthorized, model.ResponseMessage{Message: "no access"})
	}
}
