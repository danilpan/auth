create table orders
(
    id         bigserial
        constraint orders_pk
            primary key,
    user_id    integer                 not null,
    status_id  integer                 not null,
    is_paid    boolean   default false not null,
    manager_id integer,
    amount     integer,
    created_at timestamp default now() not null
);

alter table orders
    owner to postgres;

create table order_items
(
    id            bigserial
        constraint order_items_pk
            primary key,
    part_number   varchar not null,
    brand         varchar not null,
    volume_kg     integer,
    description   varchar,
    booking       varchar,
    available     integer,
    quantity      varchar,
    currency      varchar,
    days          integer,
    delivery_cost integer,
    delivery      varchar,
    destination   varchar,
    transport     varchar,
    tariff        varchar,
    reference     varchar,
    excess        varchar,
    catalog       varchar,
    stock         integer,
    order_id      integer,
    weight_kg     real,
    price         double precision
);

alter table order_items
    owner to postgres;

