create table cart_items
(
    id          bigint  default nextval('cart_item_id_seq'::regclass) not null
        constraint cart_item_pk
            primary key,
    part_number varchar,
    brand       varchar,
    booking     varchar,
    delivery    varchar,
    destination varchar,
    transport   varchar,
    quantity    integer,
    price       real,
    currency    varchar,
    reference   varchar,
    order_id    integer,
    confirmed   boolean default false                                 not null,
    user_id     integer                                               not null
);

alter table cart_items
    owner to postgres;

