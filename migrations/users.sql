create table users
(
    id           serial
        constraint users_pk
            primary key,
    username     varchar       not null,
    password     varchar(1024) not null,
    full_name    varchar,
    phone_number varchar,
    role_id      integer,
    email        varchar,
    created_at   timestamp default now(),
    partner_id   integer
);

alter table users
    owner to postgres;

create unique index users_username_uindex
    on users (username);

